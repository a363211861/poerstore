//
//  AppDelegate.h
//  PoerStore
//
//  Created by YanW on 15/3/14.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

