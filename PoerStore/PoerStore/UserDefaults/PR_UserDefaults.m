//
//  PR_UserDefaults.m
//  PR_UserDefaults
//
//  Created by YanWei on 16/10/11.
//  Copyright © 2016年 YanWei. All rights reserved.
//

#import "PR_UserDefaults.h"

#import "PR_UserDefaults+Database.h"

@implementation PR_UserDefaults

+ (instancetype)standardUserDefaults
{
    static PR_UserDefaults *_standardUserDefaults = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _standardUserDefaults = [[PR_UserDefaults alloc] init];
    });
    return _standardUserDefaults;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createTableIfNotExist];
    }
    return self;
}

- (nullable id)objectForKey:(NSString *)defaultName
{
    return [self valueObjectForKey:defaultName];
}

- (void)setObject:(nullable id)value forKey:(NSString *)defaultName
{
    if (value && ![value isKindOfClass:[NSNull class]]) {
        [self replaceIntoValueObject:value forKey:defaultName];
    } else {
        [self removeObjectForKey:defaultName];
    }
}

- (void)removeObjectForKey:(NSString *)defaultName
{
    [self deleteValueObjectForKey:defaultName];
}

- (nullable NSString *)stringForKey:(NSString *)defaultName
{
    id object = [self objectForKey:defaultName];
    
    if ([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]]) {
        NSString *targetString = [NSString stringWithFormat:@"%@", object];
        return targetString;
    }
    return nil;
}

- (NSInteger)integerForKey:(NSString *)defaultName
{
    id object = [self objectForKey:defaultName];
    NSInteger integer = [object integerValue];
    return integer;
}

- (float)floatForKey:(NSString *)defaultName
{
    id object = [self objectForKey:defaultName];
    float floatValue = [object floatValue];
    return floatValue;
}

- (double)doubleForKey:(NSString *)defaultName
{
    id object = [self objectForKey:defaultName];
    float doubleValue = [object doubleValue];
    return doubleValue;
}

- (BOOL)boolForKey:(NSString *)defaultName
{
    id object = [self objectForKey:defaultName];
    float boolValue = [object boolValue];
    return boolValue;
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)defaultName
{
    [self setObject:@(value) forKey:defaultName];
}

- (void)setFloat:(float)value forKey:(NSString *)defaultName
{
    [self setObject:@(value) forKey:defaultName];
}

- (void)setDouble:(double)value forKey:(NSString *)defaultName
{
    [self setObject:@(value) forKey:defaultName];
}

- (void)setBool:(BOOL)value forKey:(NSString *)defaultName
{
    [self setObject:@(value) forKey:defaultName];
}

@end
