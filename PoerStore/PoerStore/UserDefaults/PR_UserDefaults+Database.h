//
//  PR_UserDefaults+Database.h
//  PR_UserDefaults
//
//  Created by YanWei on 16/10/11.
//  Copyright © 2016年 YanWei. All rights reserved.
//

#import "PR_UserDefaults.h"

NS_ASSUME_NONNULL_BEGIN
@interface PR_UserDefaults (Database)

- (void)createTableIfNotExist;

- (void)replaceIntoValueObject:(id)object forKey:(NSString *)defaultName;

- (void)deleteValueObjectForKey:(NSString *)defaultName;

- (nullable id)valueObjectForKey:(NSString *)defaultName;

@end
NS_ASSUME_NONNULL_END
