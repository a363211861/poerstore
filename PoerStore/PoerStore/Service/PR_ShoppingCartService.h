//
//  PR_ShoppingCartService.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_DataStruct.h"

@class PR_ShoppingCartModel, PR_MerchandiseModel;

@interface PR_ShoppingCartService : NSObject

@property (strong, nonatomic, readonly) PR_ShoppingCartModel *shoppingCartModel;

+ (instancetype)service;

- (void)requestShoppingCartMerchandisesWithSuccess:(dispatch_block_t)success failure:(PR_ErrorCallback)failure;
- (void)addMerchandise:(PR_MerchandiseModel *)merchandise success:(dispatch_block_t)success failure:(PR_ErrorCallback)failure;
- (void)deleteMerchandise:(PR_MerchandiseModel *)merchandise success:(dispatch_block_t)success failure:(PR_ErrorCallback)failure;
- (void)changeMerchandise:(PR_MerchandiseModel *)merchandise count:(NSUInteger)count success:(dispatch_block_t)success failure:(PR_ErrorCallback)failure;



//清除购物车
- (void)clearShoppingCart;

@end
