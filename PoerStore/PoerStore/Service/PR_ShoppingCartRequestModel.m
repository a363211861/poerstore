//
//  PR_ShoppingCartRequestModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/10/15.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ShoppingCartRequestModel.h"

@interface PR_ShoppingCartRequestModel ()

@property (strong, nonatomic, readwrite) NSMutableArray<dispatch_block_t> *success;
@property (strong, nonatomic, readwrite) NSMutableArray<PR_ErrorCallback> *failure;

@end

@implementation PR_ShoppingCartRequestModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.success = [NSMutableArray array];
        self.failure = [NSMutableArray array];
    }
    return self;
}

@end
