//
//  PR_ShoppingCartService.m
//  PoerStore
//
//  Created by 伟 晏 on 16/10/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ShoppingCartService.h"

#import <libextobjc/EXTScope.h>

#import "PR_ShoppingCartRequest.h"
#import "PR_ShoppingCartRequestModel.h"

#import "PR_ShoppingCartModel.h"
#import "PR_ShoppingCartMerchandiseModel.h"
#import "PR_MerchandiseModel.h"


@interface PR_ShoppingCartService ()

@property (strong, nonatomic, readwrite) PR_ShoppingCartModel *shoppingCartModel;

@property (strong, atomic) PR_ShoppingCartRequestModel *shoppingCartMerchandisesRequestModel;
@property (strong, nonatomic) NSMutableDictionary<NSString *, PR_ShoppingCartRequestModel *> *addMerchandiseRequests;
@property (strong, nonatomic) NSMutableDictionary<NSString *, PR_ShoppingCartRequestModel *> *deleteMerchandiseRequests;
@property (strong, nonatomic) NSMutableDictionary<NSString *, PR_ShoppingCartRequestModel *> *changeMerchandiseCountRequests;

@end

@implementation PR_ShoppingCartService

+ (instancetype)service
{
    static PR_ShoppingCartService *_shoppingCartService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shoppingCartService = [[PR_ShoppingCartService alloc] init];
    });
    return _shoppingCartService;
}

- (void)clearShoppingCart
{
    [self.shoppingCartMerchandisesRequestModel.request cancel];
    self.shoppingCartMerchandisesRequestModel = nil;
    
    for (PR_ShoppingCartRequestModel *requestModel in self.addMerchandiseRequests.allValues) {
        [requestModel.request cancel];
    }
    self.addMerchandiseRequests = nil;
    
    for (PR_ShoppingCartRequestModel *requestModel in self.deleteMerchandiseRequests.allValues) {
        [requestModel.request cancel];
    }
    self.deleteMerchandiseRequests = nil;
    
    for (PR_ShoppingCartRequestModel *requestModel in self.changeMerchandiseCountRequests.allValues) {
        [requestModel.request cancel];
    }
    self.changeMerchandiseCountRequests = nil;
    
    self.shoppingCartModel = nil;
}

- (void)requestShoppingCartMerchandisesWithSuccess:(dispatch_block_t)success failure:(PR_ErrorCallback)failure
{
    if (!self.shoppingCartMerchandisesRequestModel) {
        self.shoppingCartMerchandisesRequestModel = [[PR_ShoppingCartRequestModel alloc] init];
    }
    
    if (self.shoppingCartMerchandisesRequestModel.request) {
        if (success) {
            [self.shoppingCartMerchandisesRequestModel.success addObject:success];
        }
        if (failure) {
            [self.shoppingCartMerchandisesRequestModel.failure addObject:failure];
        }
        return;
    }
    
    @weakify(self);
    PR_HTTPRequest *request = [PR_ShoppingCartRequest requestShoppingCartMerchandisesWithSuccess:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        @strongify(self);
        
        self.shoppingCartModel = responseObject;
        
        for (dispatch_block_t success in self.shoppingCartMerchandisesRequestModel.success) {
            success();
        }
        self.shoppingCartMerchandisesRequestModel = nil;
        
    } progress:nil failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        @strongify(self);
        
        for (PR_ErrorCallback failure in self.shoppingCartMerchandisesRequestModel.failure) {
            failure(error);
        }
        self.shoppingCartMerchandisesRequestModel = nil;
    }];
    
    self.shoppingCartMerchandisesRequestModel.request = request;
    if (success) {
        [self.shoppingCartMerchandisesRequestModel.success addObject:success];
    }
    if (failure) {
        [self.shoppingCartMerchandisesRequestModel.failure addObject:failure];
    }
}

- (void)addMerchandise:(PR_MerchandiseModel *)merchandise success:(dispatch_block_t)success failure:(PR_ErrorCallback)failure
{
    if (!self.addMerchandiseRequests) {
        self.addMerchandiseRequests = [NSMutableDictionary dictionary];
    }
    
    PR_ShoppingCartRequestModel *requestModel = self.addMerchandiseRequests[merchandise.merchandiseID];
    if (!requestModel) {
        requestModel = [[PR_ShoppingCartRequestModel alloc] init];
        self.addMerchandiseRequests[merchandise.merchandiseID] = requestModel;
    }
    
    if (requestModel.request) {
        if (success) {
            [requestModel.success addObject:success];
        }
        if (failure) {
            [requestModel.failure addObject:failure];
        }
        return;
    }
    
    @weakify(self);
    PR_HTTPRequest *request = [PR_ShoppingCartRequest addMerchandise:merchandise.merchandiseID success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        @strongify(self);
        
        for (dispatch_block_t success in requestModel.success) {
            success();
        }
        [self.addMerchandiseRequests removeObjectForKey:merchandise.merchandiseID];
        
    } progress:nil failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        @strongify(self);
        
        for (PR_ErrorCallback failure in requestModel.failure) {
            failure(error);
        }
        [self.addMerchandiseRequests removeObjectForKey:merchandise.merchandiseID];
    }];
    
    requestModel.request = request;
    if (success) {
        [requestModel.success addObject:success];
    }
    if (failure) {
        [requestModel.failure addObject:failure];
    }
}

- (void)deleteMerchandise:(PR_MerchandiseModel *)merchandise success:(dispatch_block_t)success failure:(PR_ErrorCallback)failure
{
    if (!self.deleteMerchandiseRequests) {
        self.deleteMerchandiseRequests = [NSMutableDictionary dictionary];
    }
    
    PR_ShoppingCartRequestModel *requestModel = self.deleteMerchandiseRequests[merchandise.merchandiseID];
    if (!requestModel) {
        requestModel = [[PR_ShoppingCartRequestModel alloc] init];
        self.deleteMerchandiseRequests[merchandise.merchandiseID] = requestModel;
    }
    
    if (requestModel.request) {
        if (success) {
            [requestModel.success addObject:success];
        }
        if (failure) {
            [requestModel.failure addObject:failure];
        }
        return;
    }
    
    @weakify(self);
    PR_HTTPRequest *request = [PR_ShoppingCartRequest deleteMerchandise:merchandise.merchandiseID success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        @strongify(self);
        
        for (dispatch_block_t success in requestModel.success) {
            success();
        }
        [self.deleteMerchandiseRequests removeObjectForKey:merchandise.merchandiseID];
        
    } progress:nil failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        @strongify(self);
        
        for (PR_ErrorCallback failure in requestModel.failure) {
            failure(error);
        }
        [self.deleteMerchandiseRequests removeObjectForKey:merchandise.merchandiseID];
    }];
    
    requestModel.request = request;
    if (success) {
        [requestModel.success addObject:success];
    }
    if (failure) {
        [requestModel.failure addObject:failure];
    }
}

- (void)changeMerchandise:(PR_MerchandiseModel *)merchandise count:(NSUInteger)count success:(dispatch_block_t)success failure:(PR_ErrorCallback)failure
{
    if (!self.changeMerchandiseCountRequests) {
        self.changeMerchandiseCountRequests = [NSMutableDictionary dictionary];
    }
    
    PR_ShoppingCartRequestModel *requestModel = self.changeMerchandiseCountRequests[merchandise.merchandiseID];
    if (!requestModel) {
        requestModel = [[PR_ShoppingCartRequestModel alloc] init];
        self.changeMerchandiseCountRequests[merchandise.merchandiseID] = requestModel;
    }
    
    if (requestModel.request) {
        if (success) {
            [requestModel.success addObject:success];
        }
        if (failure) {
            [requestModel.failure addObject:failure];
        }
        return;
    }
    
    @weakify(self);
    PR_HTTPRequest *request = [PR_ShoppingCartRequest changeMerchandise:merchandise.merchandiseID count:count success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        @strongify(self);
        
        for (dispatch_block_t success in requestModel.success) {
            success();
        }
        [self.changeMerchandiseCountRequests removeObjectForKey:merchandise.merchandiseID];
        
    } progress:nil failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        @strongify(self);
        
        for (PR_ErrorCallback failure in requestModel.failure) {
            failure(error);
        }
        [self.changeMerchandiseCountRequests removeObjectForKey:merchandise.merchandiseID];
    }];
    
    requestModel.request = request;
    if (success) {
        [requestModel.success addObject:success];
    }
    if (failure) {
        [requestModel.failure addObject:failure];
    }
}

@end
