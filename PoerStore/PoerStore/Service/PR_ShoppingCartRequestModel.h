//
//  PR_ShoppingCartRequestModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/15.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_DataStruct.h"

@class PR_HTTPRequest;

@interface PR_ShoppingCartRequestModel : NSObject

@property (strong, nonatomic) PR_HTTPRequest *request;
@property (strong, nonatomic, readonly) NSMutableArray<dispatch_block_t> *success;
@property (strong, nonatomic, readonly) NSMutableArray<PR_ErrorCallback> *failure;

@end
