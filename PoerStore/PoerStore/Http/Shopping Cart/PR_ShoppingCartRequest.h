//
//  PR_ShoppingCartRequest.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_HTTPRequest.h"

@interface PR_ShoppingCartRequest : NSObject

/**
 *  请求购物车信息
 *
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestShoppingCartMerchandisesWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  购物车添加商品
 *
 *  @param merchandiseID 商品ID
 *  @param success       成功回调
 *  @param progress      进度回调
 *  @param failure       失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)addMerchandise:(NSString *)merchandiseID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  购物车删除商品
 *
 *  @param merchandiseID 商品ID
 *  @param success       成功回调
 *  @param progress      进度回调
 *  @param failure       失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)deleteMerchandise:(NSString *)merchandiseID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  购物车修改商品数量
 *
 *  @param merchandiseID 商品ID
 *  @param count         购买数量
 *  @param success       成功回调
 *  @param progress      进度回调
 *  @param failure       失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)changeMerchandise:(NSString *)merchandiseID count:(NSInteger)count success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

@end
