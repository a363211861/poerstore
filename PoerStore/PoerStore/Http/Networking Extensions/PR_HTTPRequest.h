//
//  PR_HTTPRequest.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PR_HTTPRequest;

typedef void(^PR_HTTPRequestSuccess)(PR_HTTPRequest * _Nonnull request, id _Nullable responseObject);
typedef void(^PR_HTTPRequestProgress)(NSProgress * _Nonnull progress);
typedef void(^PR_HTTPRequestFailure)(PR_HTTPRequest *_Nonnull request, NSError * _Nonnull error);


@interface PR_HTTPRequest : NSObject

@property (nonatomic) _Nonnull Class responseClass;

- (void)cancel;

+ (_Nonnull instancetype)GET:(nullable NSString *)URLString
                  parameters:(nullable id)parameters
                     success:(nullable PR_HTTPRequestSuccess)success
                    progress:(nullable PR_HTTPRequestProgress)progress
                     failure:(nullable PR_HTTPRequestFailure)failure;

+ (_Nonnull instancetype)POST:(nullable NSString *)URLString
                   parameters:(nullable id)parameters
                      success:(nullable PR_HTTPRequestSuccess)success
                     progress:(nullable PR_HTTPRequestProgress)progress
                      failure:(nullable PR_HTTPRequestFailure)failure;

@end
