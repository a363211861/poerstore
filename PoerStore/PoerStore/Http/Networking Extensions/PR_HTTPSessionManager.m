//
//  PR_HTTPSessionManager.m
//  PoerStore
//
//  Created by YanW on 15/3/14.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_HTTPSessionManager.h"

#import "PR_HTTPRequestSerializer.h"
#import "PR_HTTPResponseSerializer.h"

static NSString * const PR_AppDotNetAPIBaseURLString = @"http://localhost/PoreStore";

@implementation PR_HTTPSessionManager

+ (instancetype)defaultManager
{
    static PR_HTTPSessionManager *_defaultManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _defaultManager = [[PR_HTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:PR_AppDotNetAPIBaseURLString]];
        _defaultManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _defaultManager.requestSerializer = [PR_HTTPRequestSerializer serializer];
        _defaultManager.responseSerializer = [PR_JSONResponseSerializer serializer];
    });
    return _defaultManager;
}

@end
