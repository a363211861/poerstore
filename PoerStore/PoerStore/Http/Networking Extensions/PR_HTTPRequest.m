//
//  PR_HTTPRequest.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_HTTPRequest.h"

#import "PR_HTTPRequest.h"

#import <JSONModel/JSONModel.h>
#import <libextobjc/EXTScope.h>

#import "NSError+PR_Category.h"

#import "PR_HTTPSessionManager.h"


typedef void(^PR_AFRequestSuccess)(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject);
typedef void(^PR_AFRequestProgress)(NSProgress * _Nonnull progress);
typedef void(^PR_AFRequestFailure)(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error);


@interface PR_HTTPRequestQueue : NSObject

@property (strong, nonatomic) NSMutableArray *queue;

@end

@implementation PR_HTTPRequestQueue

+ (instancetype)shareQueue
{
    static PR_HTTPRequestQueue *_shareQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareQueue = [[PR_HTTPRequestQueue alloc] init];
    });
    return _shareQueue;
}

@end



@interface PR_HTTPRequest ()

@property (strong, nonatomic) NSURLSessionDataTask *dataTask;

@property (copy, nonatomic) PR_HTTPRequestSuccess success;
@property (copy, nonatomic) PR_HTTPRequestProgress progress;
@property (copy, nonatomic) PR_HTTPRequestFailure failure;

@end


@implementation PR_HTTPRequest

- (void)cancel
{
    self.success = nil;
    self.failure = nil;
    [self.dataTask cancel];
    
    [[PR_HTTPRequestQueue shareQueue].queue removeObject:self];
}

- (PR_AFRequestSuccess)af_success
{
    @weakify(self);
    
    PR_AFRequestSuccess af_success = ^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        @strongify(self);
        
        do {
            
            id object = nil;
            
            if (self.responseClass) {
                
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    
                    NSError *error = nil;
                    
                    object = [[self.responseClass alloc] initWithDictionary:responseObject error:&error];
                    
                    if (error) {
#if DEBUG
                        NSLog(@"%@", [error localizedDescription]);
#endif
                        break;
                    }
                    
                } else if ([responseObject isKindOfClass:[NSArray class]]) {
                    
                    NSMutableArray *objectArray = [NSMutableArray array];
                    for (NSDictionary *objectDict in responseObject) {
                        
                        NSError *error = nil;
                        
                        id oneObject = [[self.responseClass alloc] initWithDictionary:objectDict error:&error];
                        
                        if (error) {
#if DEBUG
                            NSLog(@"%@", [error localizedDescription]);
#endif
                            break;
                        }
                        
                        [objectArray addObject:oneObject];
                    }
                    
                    object = objectArray;
                }
            }
            
            if (self.success) {
                self.success(self, object);
            }
            
            [[PR_HTTPRequestQueue shareQueue].queue removeObject:self];
            
            return;
            
        } while (0);
        
        NSError *error = [NSError localizedParserError];
        if (self.failure) {
            self.failure(self, error);
        }
        
        [[PR_HTTPRequestQueue shareQueue].queue removeObject:self];
    };
    
    return af_success;
}

- (PR_AFRequestProgress)af_progress
{
    @weakify(self);
    
    PR_AFRequestProgress af_progress = ^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        
        if (self.progress) {
            self.progress(progress);
        }
    };
    
    return af_progress;
}

- (PR_AFRequestFailure)af_failure
{
    @weakify(self);
    
    PR_AFRequestFailure af_failure = ^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        
        @strongify(self);
        
        if (self.failure) {
            self.failure(self, error);
        }
        
        [[PR_HTTPRequestQueue shareQueue].queue removeObject:self];
    };
    
    return af_failure;
}


+ (_Nonnull instancetype)GET:(nullable NSString *)URLString
                  parameters:(nullable id)parameters
                     success:(nullable PR_HTTPRequestSuccess)success
                    progress:(nullable PR_HTTPRequestProgress)progress
                     failure:(nullable PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [[PR_HTTPRequest alloc] init];
    request.success = success;
    request.progress = progress;
    request.failure = failure;
    
    request.dataTask = [[PR_HTTPSessionManager defaultManager] GET:URLString parameters:parameters progress:[request af_progress] success:[request af_success] failure:[request af_failure]];
    
    [[PR_HTTPRequestQueue shareQueue].queue addObject:self];
    
    return request;
}

+ (_Nonnull instancetype)POST:(nullable NSString *)URLString
                   parameters:(nullable id)parameters
                      success:(nullable PR_HTTPRequestSuccess)success
                     progress:(nullable PR_HTTPRequestProgress)progress
                      failure:(nullable PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [[PR_HTTPRequest alloc] init];
    request.success = success;
    request.progress = progress;
    request.failure = failure;
    
    request.dataTask = [[PR_HTTPSessionManager defaultManager] POST:URLString parameters:parameters progress:[request af_progress] success:[request af_success] failure:[request af_failure]];
    
    [[PR_HTTPRequestQueue shareQueue].queue addObject:self];
    
    return request;
}

@end
