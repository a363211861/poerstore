//
//  PR_HTTPSessionManager.h
//  PoerStore
//
//  Created by YanW on 15/3/14.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <AFNetworking/AFHTTPSessionManager.h>

@interface PR_HTTPSessionManager : AFHTTPSessionManager

+ (instancetype)defaultManager;

@end
