//
//  PR_HTTPResponseSerializer.m
//  PoerStore
//
//  Created by 伟 晏 on 15/8/13.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_HTTPResponseSerializer.h"

#import "NSError+PR_Category.h"



#if DEBUG

#import "PR_Utils.h"

#endif



@implementation PR_HTTPResponseSerializer

@end


@implementation PR_JSONResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error
{
    id responseDictionary = [super responseObjectForResponse:response data:data error:error];
    
    do {
        
        if (*error) {
            *error = [NSError localizedAPIError];
            break;
        }
        
        if (![responseDictionary isKindOfClass:[NSDictionary class]]) {
            break;
        }
        
#if DEBUG
        [PR_Utils printJsonObject:responseDictionary];
#endif
        
        id codeObject = responseDictionary[@"code"];
        NSInteger code = [codeObject integerValue];
        if (code != 1) {
            
            NSString *message = responseDictionary[@"message"];
            if (message.length == 0) {
                message = PR_HTTPAPIErrorDescription;
            }
            *error = [NSError errorWithDomain:PR_HTTPErrorDomain code:code userInfo:@{NSLocalizedDescriptionKey: message}];
            
            break;
        }
        
        id responseObject = responseDictionary[@"data"];
        
        return responseObject;
        
    } while (0);
    
    
    return nil;
}

@end