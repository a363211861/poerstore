//
//  PR_HTTPResponseSerializer.h
//  PoerStore
//
//  Created by 伟 晏 on 15/8/13.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "AFURLResponseSerialization.h"

@interface PR_HTTPResponseSerializer : AFHTTPResponseSerializer

@end


@interface PR_JSONResponseSerializer : AFJSONResponseSerializer

@end