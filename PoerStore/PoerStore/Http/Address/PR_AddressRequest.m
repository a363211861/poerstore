//
//  PR_AddressRequest.m
//  PoerStore
//
//  Created by 伟 晏 on 16/2/2.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressRequest.h"

#import "PR_AddressConfigModel.h"
#import "PR_UserAddressModel.h"

@implementation PR_AddressRequest

+ (PR_HTTPRequest *)requestAddressTreeVersionWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"address/addressTreeVersion.json" parameters:nil success:success progress:progress failure:failure];
    request.responseClass = [PR_AddressVersionModel class];
    return request;
}

+ (PR_HTTPRequest *)requestAddressTreeWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"address/addressTree.json" parameters:nil success:success progress:progress failure:failure];
    request.responseClass = [PR_ProvinceModel class];
    return request;
}

+ (PR_HTTPRequest *)requestAddressesWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"address/addressQuery.json" parameters:nil success:success progress:progress failure:failure];
    request.responseClass = [PR_UserAddressModel class];
    return request;
}

+ (PR_HTTPRequest *)saveAddress:(PR_UserAddressModel *)address success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"address"] = [address toJSONString];
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"address/addressSave.json" parameters:nil success:success progress:progress failure:failure];
    return request;
}

+ (PR_HTTPRequest *)deleteAddress:(NSString *)addressID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"addressID"] = addressID;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"address/addressDelete.json" parameters:nil success:success progress:progress failure:failure];
    return request;
}

@end
