//
//  PR_AddressRequest.h
//  PoerStore
//
//  Created by 伟 晏 on 16/2/2.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_HTTPRequest.h"

@class PR_UserAddressModel;

@interface PR_AddressRequest : NSObject

/**
 *  请求全国城市数据最新版本号
 *
 *  @param success  成功回调，数据对应：PR_AddressVersionModel
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestAddressTreeVersionWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  请求全国城市地址数据
 *
 *  @param merchandiseID 商品ID
 *  @param success       成功回调，数据对应：PR_ProvinceModel 数组
 *  @param progress      进度回调
 *  @param failure       失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestAddressTreeWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress  failure:(PR_HTTPRequestFailure)failure;

/**
 *  请求地址列表
 *
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestAddressesWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  保存地址
 *
 *  @param address  带保存的地址信息 addressID为空则是新建，非空为保存
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)saveAddress:(PR_UserAddressModel *)address success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  删除地址
 *
 *  @param addressID 待删除的地址ID
 *  @param success   成功回调
 *  @param progress  进度回调
 *  @param failure   失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)deleteAddress:(NSString *)addressID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

@end
