//
//  PR_OrderRequest.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_DataStruct.h"
#import "PR_HTTPRequest.h"

@interface PR_OrderRequest : NSObject

/**
 *  创建订单
 *
 *  @param addressID 地址ID
 *  @param success   成功回调
 *  @param progress  进度回调
 *  @param failure   失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)createOrderWithAddressID:(NSString *)addressID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  取消订单
 *
 *  @param orderID  订单ID
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)cancelOrder:(NSString *)orderID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  删除订单
 *
 *  @param orderID  订单ID
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)deleteOrder:(NSString *)orderID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  编辑订单（只能修改收货信息）
 *
 *  @param orderID   订单ID
 *  @param addressID 地址ID
 *  @param success   成功回调
 *  @param progress  进度回调
 *  @param failure   失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)modifyOrder:(NSString *)orderID addressID:(NSString *)addressID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  请求用户订单信息
 *
 *  @param status     订单状态
 *  @param startIndex 纪录开始位置
 *  @param count      请求纪录条数
 *  @param success    成功回调
 *  @param progress   进度回调
 *  @param failure    失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestOrdersForStatus:(PR_UserOrderStatus)status startIndex:(NSUInteger)startIndex count:(NSUInteger)count success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

@end
