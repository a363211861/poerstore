//
//  PR_UserRequest.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/7.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_UserRequest.h"

#import "PR_UserModel.h"
#import "PR_UserInfoModel.h"

@implementation PR_UserRequest

+ (PR_HTTPRequest *)registerWithUsername:(NSString *)username password:(NSString *)password success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = username;
    parameters[@"password"] = password;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"user/userRegister.json" parameters:parameters success:success progress:progress failure:failure];
    return request;
}

+ (PR_HTTPRequest *)loginWithUsername:(NSString *)username password:(NSString *)password success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = username;
    parameters[@"password"] = password;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"user/userLogin.json" parameters:parameters success:success progress:progress failure:failure];
    request.responseClass = [PR_UserModel class];
    return request;
}

+ (PR_HTTPRequest *)resetPassword:(NSString *)password smsCode:(NSString *)smsCode success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"password"] = password;
    parameters[@"smsCode"] = smsCode;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"user/userResetPassword.json" parameters:parameters success:success progress:progress failure:failure];
    request.responseClass = [PR_UserModel class];
    return request;
}

+ (PR_HTTPRequest *)requestUserInfoWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"user/userInfo.json" parameters:nil success:success progress:progress failure:failure];
    request.responseClass = [PR_UserInfoModel class];
    return request;
}

+ (PR_HTTPRequest *)feedback:(NSString *)feedback success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"feedback"] = feedback;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"user/userFeedback.json" parameters:nil success:success progress:progress failure:failure];
    request.responseClass = [PR_UserInfoModel class];
    return request;
}

@end
