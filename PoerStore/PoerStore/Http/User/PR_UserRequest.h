//
//  PR_UserRequest.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/7.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_HTTPRequest.h"

@interface PR_UserRequest : NSObject

/**
 *  用户注册
 *
 *  @param username 用户名
 *  @param password 密码
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)registerWithUsername:(NSString *)username password:(NSString *)password success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  用户登录
 *
 *  @param username 用户名
 *  @param password 密码
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)loginWithUsername:(NSString *)username password:(NSString *)password success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  重置密码
 *
 *  @param password 新密码
 *  @param smsCode  短信验证码
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)resetPassword:(NSString *)password smsCode:(NSString *)smsCode success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  请求用户信息
 *
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestUserInfoWithSuccess:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  用户反馈
 *
 *  @param feedback 反馈内容
 *  @param success  成功回调
 *  @param progress 进度回调
 *  @param failure  失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)feedback:(NSString *)feedback success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

@end
