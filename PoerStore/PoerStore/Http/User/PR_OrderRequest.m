//
//  PR_OrderRequest.m
//  PoerStore
//
//  Created by 伟 晏 on 16/10/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_OrderRequest.h"

@implementation PR_OrderRequest

+ (PR_HTTPRequest *)createOrderWithAddressID:(NSString *)addressID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"addressID"] = addressID;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"order/orderCreate.json" parameters:parameters success:success progress:progress failure:failure];
    return request;
}

+ (PR_HTTPRequest *)cancelOrder:(NSString *)orderID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"orderID"] = orderID;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"order/orderCancel.json" parameters:parameters success:success progress:progress failure:failure];
    return request;
}

+ (PR_HTTPRequest *)deleteOrder:(NSString *)orderID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"orderID"] = orderID;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"order/orderDelete.json" parameters:parameters success:success progress:progress failure:failure];
    return request;
}

+ (PR_HTTPRequest *)modifyOrder:(NSString *)orderID addressID:(NSString *)addressID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"orderID"] = orderID;
    parameters[@"addressID"] = addressID;
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"order/orderModify.json" parameters:parameters success:success progress:progress failure:failure];
    return request;
}

+ (PR_HTTPRequest *)requestOrdersForStatus:(PR_UserOrderStatus)status startIndex:(NSUInteger)startIndex count:(NSUInteger)count success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"status"] = @(status);
    parameters[@"startIndex"] = @(startIndex);
    parameters[@"count"] = @(count);
    
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"order/orderQuery.json" parameters:parameters success:success progress:progress failure:failure];
    return request;
}

@end
