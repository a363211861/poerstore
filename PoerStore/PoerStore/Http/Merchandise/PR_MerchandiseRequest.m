//
//  PR_MerchandiseRequest.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/7.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_MerchandiseRequest.h"

#import "PR_MerchandiseModel.h"
#import "PR_MerchandiseDetailModel.h"

@implementation PR_MerchandiseRequest

+ (PR_HTTPRequest *)requestMerchandisesForStyle:(PR_MerchandisesStyle)style success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    NSString *stringForStyle = [NSString stringWithFormat:@"%@", @(style)];
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"merchandise/merchandises.json" parameters:@{@"style": stringForStyle} success:success progress:progress failure:failure];
    request.responseClass = [PR_MerchandiseModel class];
    return request;
}


+ (PR_HTTPRequest *)requestMerchandiseDetail:(NSString *)merchandiseID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure
{
    PR_HTTPRequest *request = [PR_HTTPRequest GET:@"merchandise/merchandiseDetail.json" parameters:nil success:success progress:progress failure:failure];
    request.responseClass = [PR_MerchandiseDetailModel class];
    return request;
}

@end
