//
//  PR_MerchandiseRequest.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/7.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_DataStruct.h"

#import "PR_HTTPRequest.h"

@interface PR_MerchandiseRequest : NSObject

/**
 *  请求商品列表
 *
 *  @param style 商品所属类型(全部／热门／推荐)
 *  @param success 成功回调，数据对应：PR_MerchandiseModel
 *  @param progress 进度回调
 *  @param failure 失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestMerchandisesForStyle:(PR_MerchandisesStyle)style success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

/**
 *  请求商品详情
 *
 *  @param merchandiseID 商品ID
 *  @param success       成功回调，数据对应：PR_MerchandiseDetailModel
 *  @param progress      进度回调
 *  @param failure       失败回调
 *
 *  @return 请求DataTask
 */
+ (PR_HTTPRequest *)requestMerchandiseDetail:(NSString *)merchandiseID success:(PR_HTTPRequestSuccess)success progress:(PR_HTTPRequestProgress)progress failure:(PR_HTTPRequestFailure)failure;

@end
