//
//  UIStoryboard+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/4/24.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (PR_Category)

+ (instancetype)mainStoryboard;

+ (instancetype)userStoryboard;

+ (instancetype)authorizeStoryboard;

+ (instancetype)merchandiseStoryboard;

+ (instancetype)shoppingCartStoryboard;

+ (instancetype)orderStoryboard;

+ (instancetype)addressStoryboard;

@end
