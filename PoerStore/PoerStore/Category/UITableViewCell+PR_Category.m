//
//  UITableViewCell+PR_Category.m
//  PoerStore
//
//  Created by 伟 晏 on 15/5/23.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"


#import <objc/runtime.h>

static NSString * const kPR_TableViewCellKey_Initialized = @"PR_TableViewCellKey_Initialized";

@interface UITableViewCell (PR_Category_Private)

@property (assign, atomic) BOOL initialized;

@end


@implementation UITableViewCell (PR_Category_Private)

@dynamic initialized;

- (void)setInitialized:(BOOL)initialized
{
    objc_setAssociatedObject(self, (__bridge const void *)(kPR_TableViewCellKey_Initialized), @(initialized), OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)initialized
{
    id object = objc_getAssociatedObject(self, (__bridge const void *)(kPR_TableViewCellKey_Initialized));
    
    return [object boolValue];
}

@end


@implementation UITableViewCell (PR_Category)

+ (void)registerNIBForTableView:(UITableView *)tableView
{
    NSString *cellClassName = NSStringFromClass([self class]);
    [tableView registerNib:[UINib nibWithNibName:cellClassName bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellClassName];
}

+ (void)load
{
    Method Method_AwakeFromNib = class_getInstanceMethod([self class], @selector(awakeFromNib));
    Method Method_PR_AwakeFromNib = class_getInstanceMethod([self class], @selector(PR_AwakeFromNib));
    method_exchangeImplementations(Method_AwakeFromNib, Method_PR_AwakeFromNib);
    
    Method Method_InitWithStyle = class_getInstanceMethod([self class], @selector(initWithStyle:reuseIdentifier:));
    Method Method_PR_InitWithStyle = class_getInstanceMethod([self class], @selector(initPRCellWithStyle:reuseIdentifier:));
    method_exchangeImplementations(Method_InitWithStyle, Method_PR_InitWithStyle);
}

- (void)PR_AwakeFromNib
{
    [self PR_AwakeFromNib];
    [self customInit];
}

- (void)awakeFromNib
{
    
}

- (id)initPRCellWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    UITableViewCell *cell = [self initPRCellWithStyle:style reuseIdentifier:reuseIdentifier];
    
    [cell customInit];
    
    return cell;
}

- (void)customInit
{
    if (self.initialized) {
        
        return;
    }
    self.initialized = YES;
    
    [self commonInit];
}

- (void)commonInit
{
    
}

@end
