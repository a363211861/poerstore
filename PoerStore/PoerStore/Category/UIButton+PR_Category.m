//
//  UIButton+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "UIButton+PR_Category.h"

#import "UIImage+PR_Category.h"

static NSInteger const UIButtonBackgroundCornerRadius = 2;

@implementation UIButton (Background)

- (void)backgroundForState:(UIControlState)state1 withColor:(UIColor *)color withState:(UIControlState)state2
{
    CGFloat edgeInset = 1/[UIScreen mainScreen].scale;
    UIImage *image = [UIImage buttonImageWithColor:color cornerRadius:UIButtonBackgroundCornerRadius shadowColor:[self titleColorForState:state2]  shadowInsets:UIEdgeInsetsMake(edgeInset, edgeInset, edgeInset, edgeInset)];
    UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(UIButtonBackgroundCornerRadius, UIButtonBackgroundCornerRadius, UIButtonBackgroundCornerRadius, UIButtonBackgroundCornerRadius) resizingMode:UIImageResizingModeStretch];
    [self setBackgroundImage:resizableImage forState:state1];
}

- (void)backgroundWithClearColorAndSystemColor
{
    [self backgroundForState:UIControlStateNormal withColor:[UIColor clearColor] withState:UIControlStateNormal];
    [self backgroundForState:UIControlStateHighlighted withColor:[UIColor clearColor] withState:UIControlStateHighlighted];
    [self backgroundForState:UIControlStateSelected withColor:[UIColor clearColor] withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateDisabled withColor:[UIColor clearColor] withState:UIControlStateDisabled];
}

- (void)backgroundWithSystemHighlightColor
{
    [self backgroundForState:UIControlStateNormal withColor:[UIColor whiteColor] withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateHighlighted withColor:[UIColor whiteColor] withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateSelected withColor:[UIColor whiteColor] withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateDisabled withColor:[UIColor whiteColor] withState:UIControlStateSelected];
}

- (void)backgroundWithSystemColor
{
    [self backgroundForState:UIControlStateNormal withColor:[UIColor whiteColor] withState:UIControlStateNormal];
    [self backgroundForState:UIControlStateHighlighted withColor:[UIColor whiteColor] withState:UIControlStateHighlighted];
    [self backgroundForState:UIControlStateSelected withColor:[UIColor whiteColor] withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateDisabled withColor:[UIColor whiteColor] withState:UIControlStateDisabled];
}



@end




@implementation UIButton (TitleAndImage)

- (void)setAllStateTitleColor:(UIColor *)color
{
    [self setTitleColor:color forState:UIControlStateNormal];
    [self setTitleColor:color forState:UIControlStateHighlighted];
    [self setTitleColor:color forState:UIControlStateSelected];
    [self setTitleColor:color forState:UIControlStateDisabled];
}

- (void)setAllStateTitle:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateHighlighted];
    [self setTitle:title forState:UIControlStateSelected];
    [self setTitle:title forState:UIControlStateDisabled];
}

- (void)setAllStateBackgroundImage:(UIImage *)image
{
    [self setBackgroundImage:image forState:UIControlStateNormal];
    [self setBackgroundImage:image forState:UIControlStateHighlighted];
    [self setBackgroundImage:image forState:UIControlStateSelected];
    [self setBackgroundImage:image forState:UIControlStateDisabled];
}

- (void)setAllStateImage:(UIImage *)image
{
    [self setImage:image forState:UIControlStateNormal];
    [self setImage:image forState:UIControlStateHighlighted];
    [self setImage:image forState:UIControlStateSelected];
    [self setImage:image forState:UIControlStateDisabled];
}

- (void)setNormalTitleColor:(UIColor *)color
{
    [self setTitleColor:color forState:UIControlStateNormal];
}

- (void)setNormalTitle:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
}

- (void)setNormalImage:(UIImage *)image
{
    [self setImage:image forState:UIControlStateNormal];
}

- (void)setNormalBackgroundImage:(UIImage *)image
{
    [self setBackgroundImage:image forState:UIControlStateNormal];
}

@end
