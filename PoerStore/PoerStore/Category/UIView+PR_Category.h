//
//  UIView+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (PR_Category)

/**
 *  让view摇动
 */
- (void)shake;

/**
 *  将view裁减为image的形状
 *
 *  @param image 提供形状的image
 */
- (void)clipWithImage:(UIImage *)image;

@end
