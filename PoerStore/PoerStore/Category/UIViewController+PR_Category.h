//
//  UIViewController+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/3/18.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "UIStoryboard+PR_Category.h"

#pragma mark - HUD Control

#import <MBProgressHUD/MBProgressHUD.h>
#import <libextobjc/EXTScope.h>

@interface UIViewController (PR_HUD)

- (void)showHUD;

- (void)showHUDForText:(NSString *)text;

- (void)hideHUD;

@end


extern NSString * const UIViewControllerViewWillAppear;
extern NSString * const UIViewControllerViewDidAppear;

@interface UIViewController (Appearance)

- (void)PR_ViewWillAppear;
- (void)PR_ViewDidAppear;

@end


#pragma mark - childView Control


@interface UIViewController (PR_Child)

- (void)displayChildController:(UIViewController *)viewController;

- (void)displayChildController:(UIViewController *)viewController inFrame:(CGRect)frame;

- (void)hideChildControllers;

+ (void)displayChildController:(UIViewController *)childViewController inParentViewController:(UIViewController *)parentViewController inFrame:(CGRect)frame;

+ (void)hideChildController:(UIViewController *)childViewController;

@end



#import "PR_AlertCenter.h"

@interface UIViewController (PR_Alert)

/**
 *  系统的弹框提示alertView
 *
 *  @param message 提示内容
 */
- (void)alertMessage:(NSString *)message;


/**
 *  系统的弹框提示
 *
 *  @param message 提示内容
 *  @param key     提示的key，避免重复弹框提示
 */
- (void)alertMessage:(NSString *)message forKey:(id<NSCopying>)key;


/**
 *  HUD 弹出提示，2秒自动消失
 *
 *  @param message 提示内容
 */
- (void)showMessage:(NSString *)message;


/**
 *  HUD 弹出提示，最短显示minShowTime秒
 *
 *  @param message     提示内容
 *  @param minShowTime 最短显示时间
 */
- (void)showMessage:(NSString *)message minShowTime:(NSTimeInterval)minShowTime;


/**
 *  HUD 弹出提示，最短显示minShowTime秒
 *
 *  @param message     提示内容
 *  @param key         最短的Key，避免同一时间重复提示
 *  @param minShowTime 最短显示时间
 */
- (void)showMessage:(NSString *)message forKey:(id<NSCopying>)key minShowTime:(NSTimeInterval)minShowTime;

@end
