//
//  NSDictionary+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/3/16.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PR_Category)

- (id)objectOrNilForKey:(id)aKey;

@end
