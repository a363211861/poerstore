//
//  NSError+PR_Category.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "NSError+PR_Category.h"

NSString * const PR_HTTPErrorDomain = @"com.poer.http.error";
NSString * const PR_ShoppingCartErrorDomain = @"com.poer.shoppingCart.error";

NSString * const PR_HTTPNetErrorDescription = @"网络不给力，请检查网络";
NSString * const PR_HTTPAPIErrorDescription = @"购买的小伙伴太多，请稍后重试";
NSString * const PR_HTTPParserErrorDescription = @"数据出错啦，请稍后重试";

NSInteger const PR_HTTPNetErrorCode = 8880;
NSInteger const PR_HTTPAPIErrorCode = 8881;
NSInteger const PR_HTTPParserErrorCode = 8882;

NSInteger const PR_ShoppingCartIncreaseErrorCode = 9880;
NSInteger const PR_ShoppingCartMinusErrorCode = 9881;

@implementation NSError (PR_Category)

+ (NSError *)errorWithDomain:(NSString *)domain code:(NSInteger)code localizedDescription:(NSString *)localizedDescription
{
    NSError *error = [NSError errorWithDomain:domain code:code userInfo:@{NSLocalizedDescriptionKey: localizedDescription}];
    return error;
}

+ (NSError *)localizedNetError
{
    NSError *error = [self errorWithDomain:PR_HTTPErrorDomain code:PR_HTTPNetErrorCode localizedDescription:PR_HTTPNetErrorDescription];
    return error;
}

+ (NSError *)localizedAPIError
{
    NSError *error = [self errorWithDomain:PR_HTTPErrorDomain code:PR_HTTPAPIErrorCode localizedDescription:PR_HTTPAPIErrorDescription];
    return error;
}

+ (NSError *)localizedParserError
{
    NSError *error = [self errorWithDomain:PR_HTTPErrorDomain code:PR_HTTPParserErrorCode localizedDescription:PR_HTTPParserErrorDescription];
    return error;
}

@end
