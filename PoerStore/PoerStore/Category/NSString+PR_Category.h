//
//  NSString+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/3/16.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PR_Category)

- (NSString *)md5;

- (BOOL)sameToString:(NSString *)aString;

- (BOOL)isValidateUsername;

- (BOOL)isValidateEmail;

- (BOOL)isValidatePassword;

@end
