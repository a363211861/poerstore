//
//  UIViewController+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/3/18.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "UIViewController+PR_Category.h"

#pragma mark - HUD Control

#import <objc/runtime.h>

static NSString * const kHUDKey_HUD = @"HUDKey_HUD";

@interface UIViewController (PR_HUD_Private)

@property (strong, nonatomic) MBProgressHUD *HUD;

@end

@implementation UIViewController (PR_HUD_Private)

- (void)setHUD:(MBProgressHUD *)HUD
{
    objc_setAssociatedObject(self, (__bridge const void *)(kHUDKey_HUD), HUD, OBJC_ASSOCIATION_RETAIN);
}

- (MBProgressHUD *)HUD
{
    MBProgressHUD *HUD = objc_getAssociatedObject(self, (__bridge const void *)(kHUDKey_HUD));
    
    return HUD;
}

@end

@implementation UIViewController (PR_HUD)

- (void)showHUD
{
    if (self.HUD) {
        
        self.HUD.label.text = nil;
    
    } else {
        
        self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

- (void)showHUDForText:(NSString *)text
{
    if (!self.HUD) {
        
        self.HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    self.HUD.label.text = text;
}

- (void)hideHUD
{
    if (!self.HUD) {
        
        return;
    }
    
    [self.HUD hideAnimated:YES];
    
    self.HUD = nil;
}

@end



NSString * const UIViewControllerViewWillAppear = @"UIViewControllerViewWillAppear";
NSString * const UIViewControllerViewDidAppear = @"UIViewControllerViewDidAppear";

@implementation UIViewController (Appearance)

+ (void)PR_Appearance_ExchangeImplementations
{
    Method Method_ViewWillAppear = class_getInstanceMethod([UIViewController class], @selector(viewWillAppear:));
    Method Method_PR_Private_ViewWillAppear = class_getInstanceMethod([UIViewController class], @selector(PR_Private_ViewWillAppear:));
    method_exchangeImplementations(Method_ViewWillAppear, Method_PR_Private_ViewWillAppear);
    
    Method Method_ViewDidAppear = class_getInstanceMethod([UIViewController class], @selector(viewDidAppear:));
    Method Method_PR_Private_ViewDidAppear = class_getInstanceMethod([UIViewController class], @selector(PR_Private_ViewDidAppear:));
    method_exchangeImplementations(Method_ViewDidAppear, Method_PR_Private_ViewDidAppear);
}

- (void)PR_Private_ViewWillAppear:(BOOL)animated
{
    [self PR_Private_ViewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UIViewControllerViewWillAppear object:self];
    
    [self PR_ViewWillAppear];
}

- (void)PR_ViewWillAppear
{
    
}


- (void)PR_Private_ViewDidAppear:(BOOL)animated
{
    [self PR_Private_ViewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UIViewControllerViewDidAppear object:self];
    
    [self PR_ViewDidAppear];
}

- (void)PR_ViewDidAppear
{
    
}

@end



#pragma mark - ChildView Control

@interface UIViewControllerAutomaticallyForwardAppearanceControl : NSObject

@property (atomic, assign) BOOL automaticallyForwardAppearance;

+ (instancetype)shareControl;

@end

@implementation UIViewControllerAutomaticallyForwardAppearanceControl

+ (instancetype)shareControl
{
    static UIViewControllerAutomaticallyForwardAppearanceControl *_shareControl = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareControl = [[UIViewControllerAutomaticallyForwardAppearanceControl alloc] init];
    });
    return _shareControl;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.automaticallyForwardAppearance = YES;
    }
    return self;
}

@end

@implementation UIViewController (PR_Child)

+ (void)PR_Child_ExchangeImplementations
{
    Method Method_WillMoveToParentViewController = class_getInstanceMethod([UIViewController class], @selector(willMoveToParentViewController:));
    Method Method_PR_WillMoveToParentViewController = class_getInstanceMethod([UIViewController class], @selector(PR_willMoveToParentViewController:));
    method_exchangeImplementations(Method_WillMoveToParentViewController, Method_PR_WillMoveToParentViewController);
    
    Method Method_DidMoveToParentViewController = class_getInstanceMethod([UIViewController class], @selector(didMoveToParentViewController:));
    Method Method_PR_DidMoveToParentViewController = class_getInstanceMethod([UIViewController class], @selector(PR_didMoveToParentViewController:));
    method_exchangeImplementations(Method_DidMoveToParentViewController, Method_PR_DidMoveToParentViewController);
}

- (void)displayChildController:(UIViewController *)viewController
{
    [UIViewController displayChildController:viewController inParentViewController:self];
}

+ (void)displayChildController:(UIViewController *)childViewController inParentViewController:(UIViewController *)parentViewController
{
    [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance = NO;
    
    [parentViewController addChildViewController:childViewController];
    childViewController.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:childViewController.view];
    [childViewController didMoveToParentViewController:parentViewController];
    
    [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance = YES;
}

- (void)displayChildController:(UIViewController *)viewController inFrame:(CGRect)frame
{
    [UIViewController displayChildController:viewController inParentViewController:self inFrame:frame];
}

+ (void)displayChildController:(UIViewController *)childViewController inParentViewController:(UIViewController *)parentViewController inFrame:(CGRect)frame
{
    [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance = NO;
    
    [parentViewController addChildViewController:childViewController];
    childViewController.view.frame = frame;
    [parentViewController.view addSubview:childViewController.view];
    [childViewController didMoveToParentViewController:parentViewController];
    
    [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance = YES;
}

- (void)hideChildControllers
{
    NSArray *childViewControllers = self.childViewControllers;
    for (UIViewController *viewController in childViewControllers) {
        
        [UIViewController hideChildController:viewController];
    }
}


+ (void)hideChildController:(UIViewController *)childViewController
{
    [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance = NO;
    
    [childViewController willMoveToParentViewController:nil];
    [childViewController.view removeFromSuperview];
    [childViewController removeFromParentViewController];
    
    [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance = YES;
}

//#pragma mark - AutomaticallyForwardAppearanceMethods

- (BOOL)shouldAutomaticallyForwardAppearanceMethods
{
    return [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance;
}

- (BOOL)shouldAutomaticallyForwardRotationMethods
{
    return [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance;
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers
{
    return [UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance;
}

- (void)PR_willMoveToParentViewController:(UIViewController *)parent
{
    [self PR_willMoveToParentViewController:parent];
    
    if (![UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance) {
        
        BOOL isAppearing = NO;
        
        if (parent) {
            
            isAppearing = YES;
            
        } else {
            
            isAppearing = NO;
        }
        
        [self beginAppearanceTransition:isAppearing animated:NO];
    }
}

- (void)PR_didMoveToParentViewController:(UIViewController *)parent
{
    [self PR_didMoveToParentViewController:parent];
    
    if (![UIViewControllerAutomaticallyForwardAppearanceControl shareControl].automaticallyForwardAppearance) {
        
        [self endAppearanceTransition];
    }
}

@end





#pragma mark - Alert Control

@implementation UIViewController (PR_Alert)

- (void)alertMessage:(NSString *)message
{
    [[PR_AlertCenter shareCenter] alertMessage:message];
}

- (void)alertMessage:(NSString *)message forKey:(id<NSCopying>)key
{
    [[PR_AlertCenter shareCenter] alertMessage:message forKey:key];
}

- (void)showMessage:(NSString *)message
{
    [[PR_AlertCenter shareCenter] showMessage:message];
}

- (void)showMessage:(NSString *)message minShowTime:(NSTimeInterval)minShowTime
{
    [[PR_AlertCenter shareCenter] showMessage:message minShowTime:minShowTime];
}

- (void)showMessage:(NSString *)message forKey:(id<NSCopying>)key minShowTime:(NSTimeInterval)minShowTime
{
    [[PR_AlertCenter shareCenter] showMessage:message forKey:key minShowTime:minShowTime];
}

@end





#pragma mark - Private Control

@interface UIViewController (PR_Private)

@end

@implementation UIViewController (PR_Private)

+ (void)load
{
    [self PR_Child_ExchangeImplementations];
    [self PR_Appearance_ExchangeImplementations];
}

@end
