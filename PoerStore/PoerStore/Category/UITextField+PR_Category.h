//
//  UITextField+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (PR_Category)

+ (instancetype)textField;

- (void)backgroundForNormal;
- (void)backgroundForEdit;
- (void)backgroundForError;

- (void)backgroundForColor:(UIColor *)color;

- (void)disableAllAutoFunction;

@end
