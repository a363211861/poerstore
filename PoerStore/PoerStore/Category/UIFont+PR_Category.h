//
//  UIFont+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (PR_Category)

+ (void)logAllFontName;

+ (UIFont *)customFontOne;
+ (UIFont *)customFontTwo;
+ (UIFont *)customFontThree;
+ (UIFont *)customFontFour;
+ (UIFont *)customFontFive;

@end
