//
//  UIFont+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "UIFont+PR_Category.h"

#import <objc/runtime.h>

@interface NSString (stringCompare)

- (NSComparisonResult)stringCompare:(NSString *)string;

@end

@implementation NSString (stringCompare)

- (NSComparisonResult)stringCompare:(NSString *)string
{
    return [self caseInsensitiveCompare:string];
}

@end



static NSString *kPR_FontName = @"Microsoft YaHei";

@implementation UIFont (PR_Category)

+ (void)logAllFontName
{
    NSArray *familyNames = [UIFont familyNames];    // 获取所有的familyName
    familyNames = [familyNames sortedArrayUsingSelector:@selector(stringCompare:)]; // 排序
    
    for (NSString *familyname in familyNames)
    {
        NSLog(@"family:'%@'",familyname);
        
        NSArray *fontNames = [UIFont fontNamesForFamilyName:familyname]; // 获取相应的familyName下的fontNames
        
        for (NSString *fontName in fontNames) {
        
            NSLog(@"\tfont:'%@'",fontName);
        }
    }
}

+ (void)load
{
    Method Method_SystemFontOfSize = class_getInstanceMethod([UIFont class], @selector(systemFontOfSize:));
    Method Method_customFontOfSize = class_getInstanceMethod([UIFont class], @selector(customFontOfSize:));
    method_exchangeImplementations(Method_SystemFontOfSize, Method_customFontOfSize);
}

+ (UIFont *)customFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:kPR_FontName size:fontSize];
}

+ (UIFont *)customFontOne
{
    return [UIFont systemFontOfSize:18];
}

+ (UIFont *)customFontTwo
{
    return [UIFont systemFontOfSize:16];
}

+ (UIFont *)customFontThree
{
    return [UIFont systemFontOfSize:15];
}

+ (UIFont *)customFontFour
{
    return [UIFont systemFontOfSize:14];
}

+ (UIFont *)customFontFive
{
    return [UIFont systemFontOfSize:12];
}

@end
