//
//  NSDictionary+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/3/16.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "NSDictionary+PR_Category.h"

#import "NSString+PR_Category.h"

@implementation NSDictionary (PR_Category)

- (id)objectOrNilForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    
    if ([object isKindOfClass:[NSNull class]]) {
        object = nil;
    }
    
    if ([object isKindOfClass:[NSString class]] && [object sameToString:@"null"]) {
        
        object = nil;
    }
    
    return object;
}

@end
