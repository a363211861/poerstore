//
//  UITableViewCell+PR_Category.h
//  PoerStore
//
//  Created by 伟 晏 on 15/5/23.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (PR_Category)

+ (void)registerNIBForTableView:(UITableView *)tableView;

- (void)commonInit;

@end
