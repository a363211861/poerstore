//
//  UITextField+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "UITextField+PR_Category.h"

#import "UIImage+PR_Category.h"
#import "UIColor+PR_Category.h"

static NSInteger const UITextFieldBackgroundCornerRadius = 2;

@implementation UITextField (PR_Category)

+ (instancetype)textField
{
    UITextField *textField = [[UITextField alloc] init];
    textField.backgroundColor = [UIColor clearColor];
    textField.borderStyle = UITextBorderStyleNone;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.enablesReturnKeyAutomatically = YES;
    textField.textColor = [UIColor silverColor];
    textField.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [textField backgroundForNormal];
    
    return textField;
}

- (void)backgroundForNormal
{
    [self backgroundForColor:[UIColor colorWithWhite:0.8 alpha:1]];
}

- (void)backgroundForEdit
{
    [self backgroundForColor:[UIColor colorWithR:0 G:122 B:255 A:1]];
}

- (void)backgroundForError
{
    [self backgroundForColor:[UIColor redColor]];
}

- (void)backgroundForColor:(UIColor *)color
{
    CGFloat edgeInset = 1/[UIScreen mainScreen].scale;
    UIImage *image = [UIImage buttonImageWithColor:[UIColor whiteColor] cornerRadius:UITextFieldBackgroundCornerRadius shadowColor:color  shadowInsets:UIEdgeInsetsMake(edgeInset, edgeInset, edgeInset, edgeInset)];
    UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(UITextFieldBackgroundCornerRadius, UITextFieldBackgroundCornerRadius, UITextFieldBackgroundCornerRadius, UITextFieldBackgroundCornerRadius) resizingMode:UIImageResizingModeStretch];
    self.background = resizableImage;
}

- (void)disableAllAutoFunction
{
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.spellCheckingType = UITextSpellCheckingTypeNo;
}

@end
