//
//  NSString+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/3/16.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "NSString+PR_Category.h"

#import <CommonCrypto/CommonDigest.h>

@implementation NSString (PR_Category)

- (NSString *)md5
{
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

- (BOOL)sameToString:(NSString *)aString
{
    BOOL result = [self caseInsensitiveCompare:aString] == NSOrderedSame;
    
    return result;
}

- (BOOL)isValidateUsername
{
    NSString *usernameRegex = @"^[a-zA-Z0-9]{6,1000}+$";
    NSPredicate *usernamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", usernameRegex];
    return [usernamePredicate evaluateWithObject:self];
}

- (BOOL)isValidateEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidatePassword
{
    NSString *passwordRegex = @"^[a-zA-Z0-9]{6,1000}+$";
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordPredicate evaluateWithObject:self];
}

@end
