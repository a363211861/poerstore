//
//  UIImage+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PR_Category)

+ (instancetype)imageWithColor:(UIColor *)color
                  cornerRadius:(CGFloat)cornerRadius;

+ (instancetype) buttonImageWithColor:(UIColor *)color
                         cornerRadius:(CGFloat)cornerRadius
                          shadowColor:(UIColor *)shadowColor
                         shadowInsets:(UIEdgeInsets)shadowInsets;

+ (instancetype) circularImageWithColor:(UIColor *)color
                                   size:(CGSize)size;

- (instancetype) imageWithMinimumSize:(CGSize)size;

+ (instancetype) stepperPlusImageWithColor:(UIColor *)color;
+ (instancetype) stepperMinusImageWithColor:(UIColor *)color;

+ (instancetype) backButtonImageWithColor:(UIColor *)color
                               barMetrics:(UIBarMetrics) metrics
                             cornerRadius:(CGFloat)cornerRadius;

+ (instancetype)imageWithView:(UIView *)view;

@end


@interface UIImage (Scale)

/**
 *  重设图片的Size
 *
 *  @param size 目标大小
 *
 *  @return 调整大小后的图片
 */
- (UIImage *)scaleToSize:(CGSize)size;

@end


@interface UIImage (Mask)

/**
 *  给image添加遮盖maskImage
 *
 *  @param maskImage 遮盖Image
 *
 *  @return 合成图片（添加遮盖层后的图片）
 */
- (UIImage *)imageWithMask:(UIImage *)maskImage;

@end


@interface UIImage (RoundRect)

/**
 *  生成圆角图片
 *
 *  @param cornerRadius 圆角半径
 *
 *  @return 圆角图片
 */
- (UIImage *)imageWithCornerRadius:(CGFloat)cornerRadius;

@end
