//
//  UIStoryboard+PR_Category.m
//  PoerStore
//
//  Created by YanW on 15/4/24.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "UIStoryboard+PR_Category.h"

@implementation UIStoryboard (PR_Category)

+ (instancetype)mainStoryboard
{
    static UIStoryboard *_mainStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    });
    return _mainStoryboard;
}

+ (instancetype)userStoryboard
{
    static UIStoryboard *_mainStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mainStoryboard = [UIStoryboard storyboardWithName:@"PR_User" bundle:[NSBundle mainBundle]];
    });
    return _mainStoryboard;
}

+ (instancetype)authorizeStoryboard
{
    static UIStoryboard *_authorizeStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _authorizeStoryboard = [UIStoryboard storyboardWithName:@"PR_Authorize" bundle:[NSBundle mainBundle]];
    });
    return _authorizeStoryboard;
}

+ (instancetype)merchandiseStoryboard
{
    static UIStoryboard *_merchandiseStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _merchandiseStoryboard = [UIStoryboard storyboardWithName:@"PR_Merchandise" bundle:[NSBundle mainBundle]];
    });
    return _merchandiseStoryboard;
}

+ (instancetype)shoppingCartStoryboard
{
    static UIStoryboard *_shoppingCartStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shoppingCartStoryboard = [UIStoryboard storyboardWithName:@"PR_ShoppingCart" bundle:[NSBundle mainBundle]];
    });
    return _shoppingCartStoryboard;
}

+ (instancetype)orderStoryboard
{
    static UIStoryboard *_orderStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _orderStoryboard = [UIStoryboard storyboardWithName:@"PR_Order" bundle:[NSBundle mainBundle]];
    });
    return _orderStoryboard;
}

+ (instancetype)addressStoryboard
{
    static UIStoryboard *_addressStoryboard = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _addressStoryboard = [UIStoryboard storyboardWithName:@"PR_Address" bundle:[NSBundle mainBundle]];
    });
    return _addressStoryboard;
}

@end
