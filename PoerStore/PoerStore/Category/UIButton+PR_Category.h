//
//  UIButton+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Background)

- (void)backgroundWithSystemHighlightColor;
- (void)backgroundWithSystemColor;
- (void)backgroundWithClearColorAndSystemColor;

@end


@interface UIButton (TitleAndImage)

// set all state
- (void)setAllStateTitleColor:(UIColor *)color;
- (void)setAllStateTitle:(NSString *)title;
- (void)setAllStateBackgroundImage:(UIImage *)image;
- (void)setAllStateImage:(UIImage *)image;

// set normal state
- (void)setNormalTitleColor:(UIColor *)color;
- (void)setNormalTitle:(NSString *)title;
- (void)setNormalImage:(UIImage *)image;
- (void)setNormalBackgroundImage:(UIImage *)image;

@end