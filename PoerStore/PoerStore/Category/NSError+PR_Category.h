//
//  NSError+PR_Category.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const PR_HTTPErrorDomain;
extern NSString * const PR_ShoppingCartErrorDomain;

extern NSString * const PR_HTTPNetErrorDescription;
extern NSString * const PR_HTTPAPIErrorDescription;
extern NSString * const PR_HTTPParserErrorDescription;

extern NSInteger const PR_ShoppingCartIncreaseErrorCode;
extern NSInteger const PR_ShoppingCartMinusErrorCode;

@interface NSError (PR_Category)

+ (NSError *)errorWithDomain:(NSString *)domain code:(NSInteger)code localizedDescription:(NSString *)localizedDescription;

+ (NSError *)localizedNetError;
+ (NSError *)localizedAPIError;
+ (NSError *)localizedParserError;

@end
