//
//  UIColor+PR_Category.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c)2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PR_Category)

+ (UIColor *)colorFromHexCode:(NSString *)hexString;
+ (UIColor *)turquoiseColor;
+ (UIColor *)greenSeaColor;
+ (UIColor *)emerlandColor;
+ (UIColor *)nephritisColor;
+ (UIColor *)peterRiverColor;
+ (UIColor *)belizeHoleColor;
+ (UIColor *)amethystColor;
+ (UIColor *)wisteriaColor;
+ (UIColor *)wetAsphaltColor;
+ (UIColor *)midnightBlueColor;
+ (UIColor *)sunflowerColor;
+ (UIColor *)tangerineColor;
+ (UIColor *)carrotColor;
+ (UIColor *)pumpkinColor;
+ (UIColor *)alizarinColor;
+ (UIColor *)pomegranateColor;
+ (UIColor *)cloudsColor;
+ (UIColor *)silverColor;
+ (UIColor *)concreteColor;
+ (UIColor *)asbestosColor;

+ (UIColor *)blendedColorWithForegroundColor:(UIColor *)foregroundColor
                              backgroundColor:(UIColor *)backgroundColor
                                 percentBlend:(CGFloat)percentBlend;

+ (UIColor *)colorWithR:(CGFloat)R G:(CGFloat)G B:(CGFloat)B A:(CGFloat)a;

@end
