//
//  PR_FileManager.h
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PR_FileManager : NSObject


/******************************* search directory *******************************/

+ (NSString *)searchPathForDirectoryInUserDomain:(NSSearchPathDirectory)searchPath;

+ (NSString *)cacheDirectory;

+ (NSString *)documentDirectory;



/******************************* file manager *******************************/

+ (BOOL)fileExistsAtPath:(NSString *)path;

+ (BOOL)createDirectoryAtPath:(NSString *)path;

+ (BOOL)createFileAtPath:(NSString *)path contents:(NSData *)contents;

+ (BOOL)deleteItemAtPath:(NSString *)path;

@end
