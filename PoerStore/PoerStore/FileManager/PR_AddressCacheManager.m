//
//  PR_AddressCacheManager.m
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressCacheManager.h"

#import <AutoCoding/AutoCoding.h>

#import "PR_FileManager.h"

static NSString * const PR_AddressCacheDirectory = @"address";
static NSString * const PR_AddressCacheFile = @"address.cache";


@implementation PR_AddressCacheModel


@end



@interface PR_AddressCacheManager ()

@property (nonatomic, strong) NSString *cacheFilePath;
@property (strong, nonatomic, readwrite) PR_AddressCacheModel *cache;

@end


@implementation PR_AddressCacheManager

+ (instancetype)cacheManager
{
    static PR_AddressCacheManager *_cacheManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _cacheManager = [[PR_AddressCacheManager alloc] init];
    });
    return _cacheManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.cache = [NSKeyedUnarchiver unarchiveObjectWithFile:self.cacheFilePath];
        if (!self.cache) {
            self.cache = [[PR_AddressCacheModel alloc] init];
        }
        
        [self.cache addObserver:self forKeyPath:NSStringFromSelector(@selector(provinces)) options:NSKeyValueObservingOptionNew context:nil];
    }
    
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (object == self.cache && [keyPath isEqualToString:NSStringFromSelector(@selector(provinces))]) {
        
        [self synchronize];
        
    } else {
        
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (NSString *)cacheFilePath
{
    if (!_cacheFilePath) {
        
        NSString *cacheDirectory = [PR_FileManager cacheDirectory];
        NSString *addressDirectory = [cacheDirectory stringByAppendingPathComponent:PR_AddressCacheDirectory];
        NSString *addressFile = [addressDirectory stringByAppendingPathComponent:PR_AddressCacheFile];
        if (![PR_FileManager fileExistsAtPath:addressFile]) {
            
            BOOL result = [PR_FileManager createFileAtPath:addressFile contents:nil];
            if (result) {
                _cacheFilePath = addressFile;
            }
        }
    }
    
    return _cacheFilePath;
}

- (BOOL)synchronize
{
    BOOL result = [NSKeyedArchiver archiveRootObject:self.cache toFile:self.cacheFilePath];
    
    return result;
}

@end
