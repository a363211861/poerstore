//
//  PR_AddressCacheManager.h
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PR_ProvinceModel;
@class PR_AddressVersionModel;


@interface PR_AddressCacheModel : NSObject

@property (strong, nonatomic) PR_AddressVersionModel *version;
@property (strong, nonatomic) NSArray<PR_ProvinceModel *> *provinces;

@end




@interface PR_AddressCacheManager : NSObject

+ (instancetype)cacheManager;

@property (strong, nonatomic, readonly) PR_AddressCacheModel *cache;

@end
