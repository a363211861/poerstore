//
//  PR_FileManager.m
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_FileManager.h"

@implementation PR_FileManager

+ (NSString *)searchPathForDirectoryInUserDomain:(NSSearchPathDirectory)searchPath
{
    NSArray *directories = NSSearchPathForDirectoriesInDomains(searchPath, NSUserDomainMask, YES);
    NSString *directory = [directories firstObject];
    
    return directory;
}

+ (NSString *)cacheDirectory
{
    NSString *directory = [self searchPathForDirectoryInUserDomain:NSCachesDirectory];
    
    return directory;
}

+ (NSString *)documentDirectory
{
    NSString *directory = [self searchPathForDirectoryInUserDomain:NSDocumentDirectory];
    
    return directory;
}

+ (BOOL)fileExistsAtPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL result = [fileManager fileExistsAtPath:path];
    
    return result;
}

+ (BOOL)createDirectoryAtPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL result = NO;
    
    if (![self fileExistsAtPath:path]) {
        
        result = [fileManager createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    return result;
}

+ (BOOL)createFileAtPath:(NSString *)path contents:(NSData *)contents
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = NO;
    
    if (![self fileExistsAtPath:path]) {
        
        result = [fileManager createFileAtPath:path contents:contents attributes:nil];
    }
    
    return result;
}

+ (BOOL)deleteItemAtPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL result = [fileManager removeItemAtPath:path error:nil];
    
    return result;
}

@end
