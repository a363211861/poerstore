//
//  PR_MerchandiseDescriptionCell.h
//  PoerStore
//
//  Created by 伟 晏 on 15/9/22.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"

@interface PR_MerchandiseDescriptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
