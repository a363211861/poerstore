//
//  PR_MerchandiseHeaderCell.h
//  PoerStore
//
//  Created by Creu on 15/5/27.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"

@class PR_PriceButton;

@interface PR_MerchandiseHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *customImageView;
@property (weak, nonatomic) IBOutlet UILabel *customTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *customDetailTextLabel;
@property (weak, nonatomic) IBOutlet PR_PriceButton *priceAndBuyButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceButtonWidthConstraint;

@end
