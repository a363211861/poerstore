//
//  PR_MerchandiseImageViewCell.m
//  PoerStore
//
//  Created by 伟 晏 on 15/5/28.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseImageViewCell.h"

#import "PR_MerchandiseImageCell.h"

#import "PR_ImageModel.h"
#import "PR_MerchandiseDetailModel.h"

#import <libextobjc/EXTScope.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <KVOController/KVOController.h>

#import "PR_Functions.h"

#import "PR_WebImageManager.h"

@implementation PR_MerchandiseImageViewCell

- (void)commonInit
{
    NSString *cellClassName = NSStringFromClass([PR_MerchandiseImageCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:cellClassName bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellClassName];
    
//    @weakify(self);
//    
//    [self.KVOControllerNonRetaining observe:self keyPath:NSStringFromSelector(@selector(images)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
//        
//        @strongify(self);
//        
//        [self.collectionView reloadData];
//    }];
}

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect bounds = self.contentView.bounds;
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    UIEdgeInsets edgeInsets = flowLayout.sectionInset;
    CGSize itemSize = flowLayout.itemSize;
    
    BOOL itemSizeHasChanged = NO;
    
    if (pr_fabs(CGRectGetWidth(bounds) - edgeInsets.left - edgeInsets.right - itemSize.width) > 5) {
        
        itemSize.width = CGRectGetWidth(bounds) - edgeInsets.left - edgeInsets.right;
        itemSizeHasChanged = YES;
    }
    
    if (pr_fabs(CGRectGetHeight(bounds) - edgeInsets.top - edgeInsets.bottom - itemSize.height) > 5) {
        
        itemSize.height = CGRectGetHeight(bounds) - edgeInsets.top - edgeInsets.bottom;
        itemSizeHasChanged = YES;
    }
    
    if (itemSizeHasChanged) {
        
        flowLayout.itemSize = itemSize;
    }
}

- (void)setImages:(NSArray<PR_ImageModel> *)images
{
    if (_images != images) {
        
        _images = images;
        [self.collectionView reloadData];
    }
}

#pragma mark - private

- (void)loadImagesForIndexPaths:(NSArray *)indexPaths
{
    for (NSIndexPath *indexPath in indexPaths) {
        
        PR_ImageModel *imageModel = self.images[indexPath.row];
        [self startDownloadImageForImageModel:imageModel];
    }
}

- (void)startDownloadImageForImageModel:(PR_ImageModel *)imageModel
{
    [PR_WebImageManager downloadImageWithURL:imageModel.url completed:^(UIImage *image) {
        
        imageModel.image = image;
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellReuseIdentifier = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellReuseIdentifier = NSStringFromClass([PR_MerchandiseImageCell class]);
    });
    PR_MerchandiseImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    
    [cell.KVOController unobserveAll];
    
    PR_ImageModel *imageModel = self.images[indexPath.row];
    
    @weakify(cell);
    
    [cell.KVOController observe:imageModel keyPath:NSStringFromSelector(@selector(image)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        
        @strongify(cell);
        
        if (imageModel.image) {
            cell.imageView.image = imageModel.image;
        } else {
            cell.imageView.image = nil;
        }
    }];
    
    if (imageModel.image) {
        cell.imageView.image = imageModel.image;
    } else {
        cell.imageView.image = nil;
        if (self.collectionView.dragging == NO && self.collectionView.decelerating == NO) {
            [self startDownloadImageForImageModel:imageModel];
        }
    }
    
    return cell;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.collectionViewWillBeginDragging) {
        self.collectionViewWillBeginDragging();
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self loadImagesForIndexPaths:[self.collectionView indexPathsForVisibleItems]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForIndexPaths:[self.collectionView indexPathsForVisibleItems]];
}

@end
