//
//  PR_MerchandiseDetailViewModel.h
//  PoerStore
//
//  Created by Creu on 15/5/27.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@class UIImage, UITableView, UIViewController;
@class PR_MerchandiseDetailModel;

@interface PR_MerchandiseDetailViewModel : PR_ViewModel

@property (weak, nonatomic) UIViewController *viewController;
@property (copy, nonatomic) dispatch_block_t updatedContent;

- (instancetype)initWithmerchandiseID:(NSString *)merchandiseID;
- (void)reloadData;

- (NSInteger)numberOfRowsInSection:(NSInteger)section;
- (NSInteger)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

- (PR_MerchandiseDetailModel *)merchandiseDetail;

- (void)downloadIconImage;

@end
