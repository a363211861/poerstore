//
//  PR_MerchandiseDetailViewController.m
//  PoerStore
//
//  Created by Creu on 15/5/27.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseDetailViewController.h"

#import "PR_ImageModel.h"
#import "PR_MerchandiseDetailModel.h"

#import "PR_MerchandiseHeaderCell.h"
#import "PR_PriceButton.h"
#import "PR_MerchandiseImageViewCell.h"
#import "PR_MerchandiseDescriptionCell.h"

#import "PR_MerchandiseDetailViewModel.h"

#import <KVOController/KVOController.h>

@interface PR_MerchandiseDetailViewController ()

@end

@implementation PR_MerchandiseDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewModel.viewController = self;
    
    [PR_MerchandiseHeaderCell registerNIBForTableView:self.tableView];
    [PR_MerchandiseImageViewCell registerNIBForTableView:self.tableView];
    [PR_MerchandiseDescriptionCell registerNIBForTableView:self.tableView];
    
    @weakify(self);
    self.viewModel.updatedContent = ^() {
        @strongify(self);
        [self.tableView reloadData];
    };
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.refresh = ^() {
        @strongify(self);
        [self refresh];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.viewModel.active = NO;
}

#pragma mark - refresh

- (void)refresh
{
    [self.viewModel reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    PR_MerchandiseDetailModel *model = [self.viewModel merchandiseDetail];
    
    if (row == 0) {
        
        static NSString *merchandiseHeaderCellIdentifier = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            merchandiseHeaderCellIdentifier = NSStringFromClass([PR_MerchandiseHeaderCell class]);
        });
        PR_MerchandiseHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:merchandiseHeaderCellIdentifier forIndexPath:indexPath];
        [cell.KVOController unobserveAll];
        
        @weakify(cell);
        
        /**
         *  设置name
         */
        {
            cell.customTextLabel.text = model.name;
            [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(name)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
                @strongify(cell);
                
                cell.customTextLabel.text = model.name;
            }];
        }
        
        /**
         *  设置描述
         */
        {
            cell.customDetailTextLabel.text = model.desc;
            [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(desc)) options:NSKeyValueObservingOptionInitial block:^(id observer, id object, NSDictionary *change) {
                @strongify(cell);
                
                cell.customDetailTextLabel.text = model.desc;
            }];
        }
        
        /**
         *  设置price
         */
        {
            if (model.price) {
                [cell.priceAndBuyButton setPrice:[NSString stringWithFormat:@"¥%@", model.price]];
            } else {
                [cell.priceAndBuyButton setPrice:@"购买"];
            }
            [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(price)) options:NSKeyValueObservingOptionInitial block:^(id observer, id object, NSDictionary *change) {
                @strongify(cell);
                
                if (model.price) {
                    [cell.priceAndBuyButton setPrice:[NSString stringWithFormat:@"¥%@", model.price]];
                } else {
                    [cell.priceAndBuyButton setPrice:@"购买"];
                }
            }];
        }
        
        /**
         *  设置图片
         */
        {
            [cell.KVOController observe:model.icon keyPath:NSStringFromSelector(@selector(image)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
                @strongify(cell);
                
                if (model.icon.image) {
                    cell.customImageView.image = model.icon.image;
                } else {
                    cell.customImageView.image = [UIImage imageNamed:@"Placeholder_100_100"];
                }
            }];
            
            if (model.icon.image) {
                cell.customImageView.image = model.icon.image;
            } else {
                cell.customImageView.image = [UIImage imageNamed:@"Placeholder_100_100"];
                if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
                {
                    [self.viewModel downloadIconImage];
                }
            }
        }
        
        return cell;
    }
    
    if (row == 1) {
        
        static NSString *merchandiseDescriptionCellIdentifier = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            merchandiseDescriptionCellIdentifier = NSStringFromClass([PR_MerchandiseDescriptionCell class]);
        });
        PR_MerchandiseDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:merchandiseDescriptionCellIdentifier forIndexPath:indexPath];
        cell.descriptionLabel.text = model.detailDesc;
        
        return cell;
    }
    
    if (row == 2) {
        
        static NSString *merchandiseImageViewCellIdentifier = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            merchandiseImageViewCellIdentifier = NSStringFromClass([PR_MerchandiseImageViewCell class]);
        });
        PR_MerchandiseImageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:merchandiseImageViewCellIdentifier forIndexPath:indexPath];
        
        cell.images = model.images;
        
        @weakify(self);
        
        cell.collectionViewWillBeginDragging = ^() {
            
            @strongify(self);
            
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        };
        
        return cell;
    }
    
    return [UITableViewCell new];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger heightForRowAtIndexPath = [self.viewModel tableView:tableView heightForRowAtIndexPath:indexPath];
    
    return heightForRowAtIndexPath;
    
}

@end
