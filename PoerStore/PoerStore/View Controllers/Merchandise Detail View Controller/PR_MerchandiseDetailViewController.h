//
//  PR_MerchandiseDetailViewController.h
//  PoerStore
//
//  Created by Creu on 15/5/27.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_TableViewController.h"

@class PR_MerchandiseDetailViewModel;

@interface PR_MerchandiseDetailViewController : PR_TableViewController

@property (strong, nonatomic) PR_MerchandiseDetailViewModel *viewModel;

@end
