//
//  PR_MerchandiseImageCell.h
//  PoerStore
//
//  Created by 伟 晏 on 15/5/28.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PR_MerchandiseImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
