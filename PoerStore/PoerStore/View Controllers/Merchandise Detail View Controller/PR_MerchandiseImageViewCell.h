//
//  PR_MerchandiseImageViewCell.h
//  PoerStore
//
//  Created by 伟 晏 on 15/5/28.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"

@protocol PR_ImageModel;

@interface PR_MerchandiseImageViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) dispatch_block_t collectionViewWillBeginDragging;

@property (strong, nonatomic) NSArray<PR_ImageModel> *images;

@end
