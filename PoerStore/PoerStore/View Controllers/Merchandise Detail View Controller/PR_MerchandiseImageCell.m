//
//  PR_MerchandiseImageCell.m
//  PoerStore
//
//  Created by 伟 晏 on 15/5/28.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseImageCell.h"

@implementation PR_MerchandiseImageCell

- (void)awakeFromNib
{
    self.imageView.layer.borderWidth = 0.5;
    self.imageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.imageView.clipsToBounds = YES;
}

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
