//
//  PR_MerchandiseDetailViewModel.m
//  PoerStore
//
//  Created by Creu on 15/5/27.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseDetailViewModel.h"

#import "PR_MerchandiseRequest.h"

#import "PR_ImageModel.h"
#import "PR_MerchandiseDetailModel.h"

#import <libextobjc/EXTScope.h>
#import "PR_WebImageManager.h"

#import <UIKit/UITableView.h>
#import <UIKit/UIViewController.h>
#import <UITableView+FDTemplateLayoutCell/UITableView+FDTemplateLayoutCell.h>

#import "PR_MerchandiseDescriptionCell.h"

@interface PR_MerchandiseDetailViewModel ()

@property (strong, nonatomic) NSString *merchandiseID;
@property (strong, nonatomic) PR_HTTPRequest *dataTask;
@property (strong, nonatomic) PR_MerchandiseDetailModel *merchandiseDetailModel;

@end

@implementation PR_MerchandiseDetailViewModel

- (instancetype)initWithmerchandiseID:(NSString *)merchandiseID
{
    self = [super init];
    if (self) {
        
        self.merchandiseID = merchandiseID;
    }
    
    return self;
}

- (void)reloadData
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    
    self.dataTask = [PR_MerchandiseRequest requestMerchandiseDetail:self.merchandiseID success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        self.merchandiseDetailModel = responseObject;
        
        if (self.updatedContent) {
            self.updatedContent();
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

- (PR_MerchandiseDetailModel *)merchandiseDetail
{
    return self.merchandiseDetailModel;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row, cellHeight = 0;
    
    switch (row) {
        case 0:
            cellHeight = 121;
            break;
        case 1:
        {
            if (!self.merchandiseDetailModel) {
                cellHeight = 44;
                break;
            }
            
            static NSString *merchandiseDescriptionCellIdentifier = nil;
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                merchandiseDescriptionCellIdentifier = NSStringFromClass([PR_MerchandiseDescriptionCell class]);
            });
            cellHeight = ceilf([tableView fd_heightForCellWithIdentifier:merchandiseDescriptionCellIdentifier cacheByIndexPath:indexPath configuration:^(PR_MerchandiseDescriptionCell *cell) {
                cell.descriptionLabel.text = self.merchandiseDetailModel.detailDesc;
            }]);
        }
            break;
        case 2:
        {
            cellHeight = CGRectGetHeight(self.viewController.view.bounds) - CGRectGetHeight([UIApplication sharedApplication].statusBarFrame) - CGRectGetHeight(self.viewController.navigationController.navigationBar.frame) - CGRectGetHeight(self.viewController.tabBarController.tabBar.frame);
        }
        default:
            break;
    }
    
    return cellHeight;
}

#pragma mark - image

- (void)downloadIconImage
{
    NSURL *imageURL = self.merchandiseDetailModel.icon.url;
    
    @weakify(self);
    [PR_WebImageManager downloadMerchandiseBigIconWithURL:imageURL completed:^(UIImage *image) {
        @strongify(self);
        
        self.merchandiseDetailModel.icon.image = image;
    }];
}

@end
