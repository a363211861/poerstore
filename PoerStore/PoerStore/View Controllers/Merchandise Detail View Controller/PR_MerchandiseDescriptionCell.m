//
//  PR_MerchandiseDescriptionCell.m
//  PoerStore
//
//  Created by 伟 晏 on 15/9/22.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseDescriptionCell.h"


@implementation PR_MerchandiseDescriptionCell

- (void)commonInit
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
