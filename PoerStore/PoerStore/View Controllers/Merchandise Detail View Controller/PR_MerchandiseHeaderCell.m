//
//  PR_MerchandiseHeaderCell.m
//  PoerStore
//
//  Created by Creu on 15/5/27.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseHeaderCell.h"

#import "PR_PriceButton.h"

@implementation PR_MerchandiseHeaderCell

- (void)commonInit
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.priceAndBuyButton.widthConstraint = self.priceButtonWidthConstraint;
}

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
