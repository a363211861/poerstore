//
//  PR_ShoppingCartMerchandiseCell.h
//  PoerStore
//
//  Created by 伟 晏 on 15/10/2.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"

extern NSInteger const kPR_ShoppingCartMerchandiseCellHeight;

@interface PR_ShoppingCartMerchandiseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *customImageView;
@property (weak, nonatomic) IBOutlet UILabel *customTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *customDetailTextLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *addAndMinControl;

- (void)setMerchandiseCount:(NSInteger)count;

@property (copy, nonatomic) dispatch_block_t increaseBlock;
@property (copy, nonatomic) dispatch_block_t minusBlock;

@end
