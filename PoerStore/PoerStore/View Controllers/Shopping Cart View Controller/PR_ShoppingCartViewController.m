//
//  PR_ShoppingCartViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 15/9/24.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_ShoppingCartViewController.h"

#import <KVOController/KVOController.h>

#import "PR_ShoppingCartViewModel.h"

#import "PR_ImageModel.h"
#import "PR_ShoppingCartModel.h"
#import "PR_ShoppingCartMerchandiseModel.h"
#import "PR_MerchandiseModel.h"

#import "PR_ShoppingCartMerchandiseCell.h"


static NSInteger const kPR_ShoppingCartDeleteAlertTag = 100;


@interface PR_ShoppingCartViewController ()

@property (strong, nonatomic) PR_ShoppingCartViewModel *viewModel;

@property (weak, nonatomic) PR_ShoppingCartMerchandiseModel *willDeleteShoppingCartModel;

@end

@implementation PR_ShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"购物车";
    
    self.viewModel = [[PR_ShoppingCartViewModel alloc] init];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"结算" style:UIBarButtonItemStylePlain target:self action:@selector(buyAction:)];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    self.tableView.rowHeight = kPR_ShoppingCartMerchandiseCellHeight;
    self.tableView.estimatedRowHeight = kPR_ShoppingCartMerchandiseCellHeight;
    [PR_ShoppingCartMerchandiseCell registerNIBForTableView:self.tableView];
    
    @weakify(self);
    self.viewModel.updatedContent = ^() {
        @strongify(self);
        
        [self.tableView reloadData];
    };
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.refresh = ^() {
        @strongify(self);
        [self refresh];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.viewModel.active = NO;
}

#pragma mark - action

- (void)buyAction:(UIBarButtonItem *)barButtonItem
{
}

#pragma mark - refresh

- (void)refresh
{
    [self.viewModel reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellReuseIdentifier = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellReuseIdentifier = NSStringFromClass([PR_ShoppingCartMerchandiseCell class]);
    });
    PR_ShoppingCartMerchandiseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        PR_ShoppingCartMerchandiseModel *merchandiseShoppingCartModel = [self.viewModel merchandiseShoppingCartModelForIndexPath:indexPath];
        PR_MerchandiseModel *model = merchandiseShoppingCartModel.merchandise;
        NSString *message = [NSString stringWithFormat:@"确定要将%@从购物车删除吗？", model.name];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.tag = kPR_ShoppingCartDeleteAlertTag;
        [alertView show];
        self.willDeleteShoppingCartModel = merchandiseShoppingCartModel;
    }
}

#pragma mark - UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPat
{
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self.viewModel loadImagesForIndexPaths:[self.tableView indexPathsForVisibleRows]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self.viewModel loadImagesForIndexPaths:[self.tableView indexPathsForVisibleRows]];
}

#pragma mark - Private Methods

- (void)configureCell:(PR_ShoppingCartMerchandiseCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    PR_ShoppingCartMerchandiseModel *merchandiseShoppingCartModel = [self.viewModel merchandiseShoppingCartModelForIndexPath:indexPath];
    PR_MerchandiseModel *model = merchandiseShoppingCartModel.merchandise;
    
    [cell.KVOController unobserveAll];
    
    @weakify(self);
    cell.increaseBlock = ^(){
        @strongify(self);
        [self.viewModel increaseMerchandise:merchandiseShoppingCartModel];
    };
    
    cell.minusBlock = ^(){
        @strongify(self);
        [self.viewModel minusMerchandise:merchandiseShoppingCartModel];
    };
    
    @weakify(cell);
    
    /**
     *  设置name
     */
    {
        cell.customTextLabel.text = model.name;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(name)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.customTextLabel.text = model.name;
        }];
    }
    
    /**
     *  设置detail
     */
    {
        cell.customDetailTextLabel.text = model.desc;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(desc)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.customDetailTextLabel.text = model.desc;
        }];
    }
    
    /**
     *  设置数量
     */
    {
        [cell setMerchandiseCount:merchandiseShoppingCartModel.currentBuyCount];
        [cell.KVOController observe:merchandiseShoppingCartModel keyPath:NSStringFromSelector(@selector(currentBuyCount)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            [cell setMerchandiseCount:merchandiseShoppingCartModel.currentBuyCount];
        }];
    }
    
    /**
     *  设置图片
     */
    {
        [cell.KVOController observe:model.icon keyPath:NSStringFromSelector(@selector(image)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            if (model.icon.image) {
                cell.customImageView.image = model.icon.image;
            } else {
                cell.customImageView.image = [UIImage imageNamed:@"Placeholder_64_64"];
            }
        }];
        
        if (model.icon.image) {
            cell.customImageView.image = model.icon.image;
        } else {
            cell.customImageView.image = [UIImage imageNamed:@"Placeholder_64_64"];
            if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
            {
                [self.viewModel startIconDownloadForIndexPath:indexPath];
            }
        }
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kPR_ShoppingCartDeleteAlertTag) {
        
        if (buttonIndex == 1) {
            [self.viewModel deleteMerchandise:self.willDeleteShoppingCartModel];
        } else {
            [self.tableView setEditing:NO animated:YES];
        }
        self.willDeleteShoppingCartModel = nil;
    }
}

@end
