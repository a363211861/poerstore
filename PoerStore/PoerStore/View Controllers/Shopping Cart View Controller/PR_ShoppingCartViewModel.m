//
//  PR_ShoppingCartViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 15/9/24.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_ShoppingCartViewModel.h"

#import <KVOController/KVOController.h>
#import <libextobjc/EXTScope.h>
#import <UIKit/UITableView.h>

#import "NSError+PR_Category.h"

#import "PR_WebImageManager.h"

#import "PR_ImageModel.h"

#import "PR_ShoppingCartService.h"

#import "PR_ShoppingCartModel.h"
#import "PR_ShoppingCartMerchandiseModel.h"
#import "PR_MerchandiseModel.h"
#import "PR_ShoppingCartAmountModel.h"


@interface PR_ShoppingCartViewModel ()

@property (weak, nonatomic) PR_ShoppingCartService *service;
@property (strong, nonatomic) PR_ShoppingCartModel *shoppingCartModel;

@end

@implementation PR_ShoppingCartViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.service = [PR_ShoppingCartService service];
    }
    return self;
}

- (void)reloadData
{
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    [self.service requestShoppingCartMerchandisesWithSuccess:^{
        @strongify(self);
        
        self.shoppingCartModel = self.service.shoppingCartModel;
        if (self.updatedContent) {
            self.updatedContent();
        }
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
    } failure:^(NSError *error) {
        @strongify(self);
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

- (void)increaseMerchandise:(PR_ShoppingCartMerchandiseModel *)merchandise
{
    NSUInteger targetCount = merchandise.currentBuyCount + 1;
    if (targetCount > merchandise.merchandise.buyMaxCount) {
        NSError *error = [NSError errorWithDomain:PR_ShoppingCartErrorDomain code:PR_ShoppingCartIncreaseErrorCode localizedDescription:[NSString stringWithFormat:@"该商品最多只能购买%@件", @(merchandise.merchandise.buyMaxCount)]];
        if (self.error) {
            self.error(error);
        }
        return;
    }
    
    [self changeMerchandise:merchandise.merchandise count:targetCount];
}

- (void)minusMerchandise:(PR_ShoppingCartMerchandiseModel *)merchandise
{
    NSUInteger targetCount = merchandise.currentBuyCount - 1;
    if (targetCount < merchandise.merchandise.buyMinCount) {
        NSError *error = [NSError errorWithDomain:PR_ShoppingCartErrorDomain code:PR_ShoppingCartMinusErrorCode localizedDescription:[NSString stringWithFormat:@"该商品必须至少购买%@件", @(merchandise.merchandise.buyMinCount)]];
        if (self.error) {
            self.error(error);
        }
        return;
    }
    
    [self changeMerchandise:merchandise.merchandise count:targetCount];
}

- (void)changeMerchandise:(PR_MerchandiseModel *)merchandise count:(NSUInteger)count
{
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    [self.service changeMerchandise:merchandise count:count success:^{
        @strongify(self);
        [self reloadData];
        
    } failure:^(NSError *error) {
        @strongify(self);
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

- (void)deleteMerchandise:(PR_ShoppingCartMerchandiseModel *)merchandise
{
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    [self.service deleteMerchandise:merchandise.merchandise success:^{
        @strongify(self);
        [self reloadData];
        
    } failure:^(NSError *error) {
        @strongify(self);
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

#pragma mark - public funcs

- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    if (section == 0) {
        numberOfRows = self.shoppingCartModel.merchandises.count;
    }
    
    return numberOfRows;
}

- (PR_ShoppingCartMerchandiseModel *)merchandiseShoppingCartModelForIndexPath:(NSIndexPath *)indexPath
{
    PR_ShoppingCartMerchandiseModel *merchandiseShoppingCartModel = nil;
    if (indexPath.section == 0) {
        merchandiseShoppingCartModel = self.shoppingCartModel.merchandises[indexPath.row];
    }
    return merchandiseShoppingCartModel;
}

- (NSURL *)avatarImageURLAtIndexPath:(NSIndexPath *)indexPath
{
    PR_ShoppingCartMerchandiseModel *merchandiseShoppingCartModel = [self merchandiseShoppingCartModelForIndexPath:indexPath];
    PR_MerchandiseModel *model = merchandiseShoppingCartModel.merchandise;
    
    return model.icon.url;
}

- (void)loadImagesForIndexPaths:(NSArray *)indexPaths
{
    for (NSIndexPath *indexPath in indexPaths) {
        
        [self startIconDownloadForIndexPath:indexPath];
    }
}

- (void)startIconDownloadForIndexPath:(NSIndexPath *)indexPath
{
    NSURL *iconURL = [self avatarImageURLAtIndexPath:indexPath];
    
    @weakify(self);
    [PR_WebImageManager downloadMerchandiseSmallIconWithURL:iconURL completed:^(UIImage *image) {
        @strongify(self);
        
        [self downloadFinishImage:image forIndexPath:indexPath];
    }];
}

- (void)downloadFinishImage:(UIImage *)image forIndexPath:(NSIndexPath *)indexPath
{
    PR_ShoppingCartMerchandiseModel *merchandiseShoppingCartModel = [self merchandiseShoppingCartModelForIndexPath:indexPath];
    PR_MerchandiseModel *model = merchandiseShoppingCartModel.merchandise;
    model.icon.image = image;
}

@end
