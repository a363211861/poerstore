//
//  PR_ShoppingCartMerchandiseCell.m
//  PoerStore
//
//  Created by 伟 晏 on 15/10/2.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_ShoppingCartMerchandiseCell.h"

NSInteger const kPR_ShoppingCartMerchandiseCellHeight = 81;

@implementation PR_ShoppingCartMerchandiseCell

- (void)commonInit
{
    self.addAndMinControl.selectedSegmentIndex = UISegmentedControlNoSegment;
}


- (IBAction)segmentAction:(UISegmentedControl *)sender {
    
    NSInteger selectedSegmentIndex = sender.selectedSegmentIndex;
    switch (selectedSegmentIndex) {
        case 0:
        {
            if (self.minusBlock) {
                self.minusBlock();
            }
        }
            break;
        case 2:
        {
            if (self.increaseBlock) {
                self.increaseBlock();
            }
        }
            break;
        default:
            break;
    }
    
    sender.userInteractionEnabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        sender.userInteractionEnabled = YES;
        sender.selectedSegmentIndex = UISegmentedControlNoSegment;
    });
}

- (void)setMerchandiseCount:(NSInteger)count
{
    NSString *countString = [NSString stringWithFormat:@"%@", @(count)];
    [self.addAndMinControl setTitle:countString forSegmentAtIndex:1];
}

@end
