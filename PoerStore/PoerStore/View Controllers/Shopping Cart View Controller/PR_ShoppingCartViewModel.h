//
//  PR_ShoppingCartViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 15/9/24.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@class PR_ShoppingCartMerchandiseModel, PR_MerchandiseModel;

@interface PR_ShoppingCartViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t updatedContent;

- (void)reloadData;

- (void)increaseMerchandise:(PR_ShoppingCartMerchandiseModel *)merchandise;
- (void)minusMerchandise:(PR_ShoppingCartMerchandiseModel *)merchandise;
- (void)deleteMerchandise:(PR_ShoppingCartMerchandiseModel *)merchandise;

- (NSInteger)numberOfRowsInSection:(NSInteger)section;
- (PR_ShoppingCartMerchandiseModel *)merchandiseShoppingCartModelForIndexPath:(NSIndexPath *)indexPath;

- (void)loadImagesForIndexPaths:(NSArray *)indexPaths;
- (void)startIconDownloadForIndexPath:(NSIndexPath *)indexPath;

@end
