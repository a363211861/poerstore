//
//  PR_ResetPasswordViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 16/9/4.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ResetPasswordViewController.h"

#import "UIFont+PR_Category.h"
#import "UIButton+PR_Category.h"
#import "NSString+PR_Category.h"
#import "UITextField+PR_Category.h"
#import "UIView+PR_Category.h"

@interface PR_ResetPasswordViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *sureButton;

@end

@implementation PR_ResetPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"重置密码";
    
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
    
    NSInteger textFieldLeftWidth = 60;
    {
        UITextField *textField = self.usernameTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, CGRectGetHeight(textField.frame))];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"手 机";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UITextField *textField = self.verificationCodeTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, CGRectGetHeight(textField.frame))];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"验 证";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(0, 0, 90, CGRectGetHeight(textField.frame));
        [button setNormalTitle:@"获取验证码"];
        textField.rightView = button;
        textField.rightViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UITextField *textField = self.passwordTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, textField.frame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"密 码";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UIButton *button = self.sureButton;
        button.exclusiveTouch = YES;
        [button backgroundWithSystemColor];
    }
}

#pragma mark - action

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (IBAction)sureAction:(UIButton *)button
{
    [self.view endEditing:YES];
    
    NSString *string = self.usernameTextField.text;
    if (string.length == 0 || ![string isValidateEmail]) {
        [self.usernameTextField becomeFirstResponder];
        [self.usernameTextField backgroundForError];
        [self.usernameTextField shake];
        return;
    } else {
        [self.usernameTextField backgroundForNormal];
    }
    
    string = self.verificationCodeTextField.text;
    if (string.length == 0) {
        [self.verificationCodeTextField becomeFirstResponder];
        [self.verificationCodeTextField backgroundForError];
        [self.verificationCodeTextField shake];
        return;
    } else {
        [self.verificationCodeTextField backgroundForNormal];
    }
    
    string = self.passwordTextField.text;
    if (string.length == 0 || ![string isValidatePassword]) {
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField backgroundForError];
        [self.passwordTextField shake];
        return;
    } else {
        [self.passwordTextField backgroundForNormal];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField backgroundForEdit];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField backgroundForNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self sureAction:self.sureButton];
    }
    return YES;
}

@end
