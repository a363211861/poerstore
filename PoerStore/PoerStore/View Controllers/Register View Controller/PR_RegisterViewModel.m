//
//  PR_RegisterViewModel.m
//  PoerStore
//
//  Created by Creu on 15/5/22.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_RegisterViewModel.h"

#import <libextobjc/EXTScope.h>

#import "PR_UserRequest.h"

@interface PR_RegisterViewModel ()

@property (strong, nonatomic) PR_HTTPRequest *dataTask;

@end

@implementation PR_RegisterViewModel

- (void)registerWithUsername:(NSString *)username password:(NSString *)password
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    
    self.dataTask = [PR_UserRequest registerWithUsername:username password:password success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

@end
