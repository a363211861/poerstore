//
//  PR_RegisterViewModel.h
//  PoerStore
//
//  Created by Creu on 15/5/22.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@interface PR_RegisterViewModel : PR_ViewModel

- (void)registerWithUsername:(NSString *)username password:(NSString *)password;

@end
