//
//  PR_RegisterViewController.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_RegisterViewController.h"

#import "UIFont+PR_Category.h"

#import "UIButton+PR_Category.h"
#import "NSString+PR_Category.h"
#import "UITextField+PR_Category.h"
#import "UIView+PR_Category.h"

#import "PR_RegisterViewModel.h"

@interface PR_RegisterViewController ()

@property (strong, nonatomic) PR_RegisterViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *rePasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end

@implementation PR_RegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"注册";
    
    self.viewModel = [[PR_RegisterViewModel alloc] init];
    
    @weakify(self);
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self showMessage:error.localizedDescription];
    };
    
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
    
    NSInteger textFieldLeftWidth = 60;
    {
        UITextField *textField = self.usernameTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, CGRectGetHeight(textField.frame))];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"用户名";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UITextField *textField = self.passwordTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, CGRectGetHeight(textField.frame))];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"密   码";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UITextField *textField = self.rePasswordTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, textField.frame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"验   证";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UIButton *button = self.registerButton;
        button.exclusiveTouch = YES;
        [button backgroundWithSystemColor];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.viewModel.active = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
}

#pragma mark - action

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (IBAction)registerAction:(UIButton *)button
{
    [self.view endEditing:YES];
    
    NSString *username = self.usernameTextField.text;
    if (username.length == 0 || ![username isValidateUsername]) {
        [self.usernameTextField becomeFirstResponder];
        [self.usernameTextField backgroundForError];
        [self.usernameTextField shake];
        return;
    } else {
        [self.usernameTextField backgroundForNormal];
    }
    
    NSString *password = self.passwordTextField.text;
    if (password.length == 0 || ![password isValidatePassword]) {
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField backgroundForError];
        [self.passwordTextField shake];
        return;
    } else {
        [self.passwordTextField backgroundForNormal];
    }
    
    NSString *rePassword = self.rePasswordTextField.text;
    if (![password isEqualToString:rePassword]) {
        [self.rePasswordTextField becomeFirstResponder];
        [self.rePasswordTextField backgroundForError];
        [self.rePasswordTextField shake];
        return;
    } else {
        [self.rePasswordTextField backgroundForNormal];
    }
    
    NSString *username_md5 = [username md5], *password_md5 = [password md5];
    [self.viewModel registerWithUsername:username_md5 password:password_md5];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField backgroundForEdit];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField backgroundForNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self registerAction:self.registerButton];
    }
    return YES;
}

@end
