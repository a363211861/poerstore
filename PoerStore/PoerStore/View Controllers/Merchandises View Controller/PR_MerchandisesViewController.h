//
//  PR_MerchandisesViewController.h
//  PoerStore
//
//  Created by 伟 晏 on 15/11/25.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_TableViewController.h"

#import "PR_DataStruct.h"

@interface PR_MerchandisesViewController : PR_TableViewController

@property (assign, nonatomic) PR_MerchandisesStyle style;

@end


@interface PR_MerchandisesViewController (Helper)

+ (NSString *)titleForStyle:(PR_MerchandisesStyle)style;
+ (UIImage *)tabBarItemImageForStyle:(PR_MerchandisesStyle)style;

@end