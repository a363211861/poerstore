//
//  PR_MerchandisesViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 15/11/25.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_MerchandisesViewModel.h"

#import <libextobjc/EXTScope.h>
#import "PR_WebImageManager.h"

#import "UIImage+PR_Category.h"

#import "PR_MerchandiseRequest.h"

#import "PR_MerchandiseModel.h"
#import "PR_ImageModel.h"
#import "PR_MerchandiseDetailViewModel.h"


@interface PR_MerchandisesViewModel ()

@property (assign, nonatomic) PR_MerchandisesStyle style;

@property (strong, nonatomic) PR_HTTPRequest *dataTask;
@property (strong, nonatomic) NSMutableArray *merchandises;

@end


@implementation PR_MerchandisesViewModel

- (instancetype)initWithStyle:(PR_MerchandisesStyle)style
{
    self = [super init];
    if (self == nil) return nil;
    
    self.style = style;
    self.merchandises = [NSMutableArray array];
    
    return self;
}


- (void)reloadData
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    
    self.dataTask = [PR_MerchandiseRequest requestMerchandisesForStyle:self.style success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        [self.merchandises setArray:responseObject];
        
        if (self.updatedContent) {
            self.updatedContent();
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

- (void)loadNextPage
{
}


#pragma mark - Public Methods


- (PR_MerchandiseModel *)merchandiseModelForIndexPath:(NSIndexPath *)indexPath
{
    PR_MerchandiseModel *merchandise = nil;
    if (indexPath && indexPath.section == 0) {
        merchandise = self.merchandises[indexPath.row];
    }
    return merchandise;
}


- (PR_MerchandiseDetailViewModel *)merchandiseDetailViewModelForIndexPath:(NSIndexPath *)indexPath
{
    PR_MerchandiseModel *model = [self merchandiseModelForIndexPath:indexPath];
    PR_MerchandiseDetailViewModel *viewModel = [[PR_MerchandiseDetailViewModel alloc] initWithmerchandiseID:model.merchandiseID];
    
    return viewModel;
}


- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    if (section == 0) {
        numberOfRows = self.merchandises.count;
    }
    
    return numberOfRows;
}


- (NSURL *)avatarImageURLAtIndexPath:(NSIndexPath *)indexPath
{
    PR_MerchandiseModel *model = [self merchandiseModelForIndexPath:indexPath];
    
    return model.icon.url;
}

- (void)loadImagesForIndexPaths:(NSArray *)indexPaths
{
    for (NSIndexPath *indexPath in indexPaths) {
        
        [self startIconDownloadForIndexPath:indexPath];
    }
}

- (void)startIconDownloadForIndexPath:(NSIndexPath *)indexPath
{
    NSURL *iconURL = [self avatarImageURLAtIndexPath:indexPath];
    
    @weakify(self);
    [PR_WebImageManager downloadMerchandiseSmallIconWithURL:iconURL completed:^(UIImage *image) {
        @strongify(self);
        
        [self downloadFinishImage:image forIndexPath:indexPath];
    }];
}

- (void)downloadFinishImage:(UIImage *)image forIndexPath:(NSIndexPath *)indexPath
{
    PR_MerchandiseModel *model = [self merchandiseModelForIndexPath:indexPath];
    model.icon.image = image;
}

@end
