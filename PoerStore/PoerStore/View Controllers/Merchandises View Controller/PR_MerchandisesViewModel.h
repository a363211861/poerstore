//
//  PR_MerchandisesViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 15/11/25.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

#import "PR_DataStruct.h"

@class UIImage;
@class PR_MerchandiseModel;
@class PR_MerchandiseDetailViewModel;
@class PR_MerchandiseIconModel;

@interface PR_MerchandisesViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t updatedContent;

- (instancetype)initWithStyle:(PR_MerchandisesStyle)style;

- (void)reloadData;
- (void)loadNextPage;

- (PR_MerchandiseDetailViewModel *)merchandiseDetailViewModelForIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)numberOfRowsInSection:(NSInteger)section;
- (PR_MerchandiseModel *)merchandiseModelForIndexPath:(NSIndexPath *)indexPath;

- (void)loadImagesForIndexPaths:(NSArray *)indexPaths;
- (void)startIconDownloadForIndexPath:(NSIndexPath *)indexPath;

@end
