//
//  PR_MerchandisesViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 15/11/25.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_MerchandisesViewController.h"

#import <KVOController/KVOController.h>

#import "UIStoryboard+PR_Category.h"

#import "PR_MerchandisesViewModel.h"
#import "PR_ImageModel.h"
#import "PR_MerchandiseModel.h"
#import "PR_ShoppingCartService.h"

#import "PR_MerchandiseCell.h"
#import "PR_PriceButton.h"

#import "PR_MerchandiseDetailViewController.h"

@interface PR_MerchandisesViewController ()

@property (strong, nonatomic) PR_MerchandisesViewModel *viewModel;

@end

@implementation PR_MerchandisesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [[self class] titleForStyle:self.style];
    
    self.tableView.rowHeight = kPR_MerchandiseCellHeight;
    self.tableView.estimatedRowHeight = kPR_MerchandiseCellHeight;
    [PR_MerchandiseCell registerNIBForTableView:self.tableView];
    
    self.viewModel = [[PR_MerchandisesViewModel alloc] initWithStyle:self.style];
    
    @weakify(self);
    self.viewModel.updatedContent = ^() {
        @strongify(self);
        [self.tableView reloadData];
    };
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.refresh = ^() {
        @strongify(self);
        [self refresh];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.viewModel.active = NO;
}

#pragma mark - refresh

- (void)refresh
{
    [self.viewModel reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellReuseIdentifier = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellReuseIdentifier = NSStringFromClass([PR_MerchandiseCell class]);
    });
    PR_MerchandiseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PR_MerchandiseDetailViewController *viewController = [[UIStoryboard merchandiseStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_MerchandiseDetailViewController class])];
    viewController.viewModel = [self.viewModel merchandiseDetailViewModelForIndexPath:indexPath];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        [self.viewModel loadImagesForIndexPaths:[self.tableView indexPathsForVisibleRows]];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self.viewModel loadImagesForIndexPaths:[self.tableView indexPathsForVisibleRows]];
}


#pragma mark - Private Methods

- (void)configureCell:(PR_MerchandiseCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    PR_MerchandiseModel *model = [self.viewModel merchandiseModelForIndexPath:indexPath];
    
    [cell.KVOController unobserveAll];
    
    
    @weakify(cell);
    
    /**
     *  设置name
     */
    {
        cell.customTextLabel.text = model.name;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(name)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.customTextLabel.text = model.name;
        }];
    }
    
    /**
     *  设置detail
     */
    {
        cell.customDetailTextLabel.text = model.desc;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(desc)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.customDetailTextLabel.text = model.desc;
        }];
    }
    
    /**
     *  设置price
     */
    {
        @weakify(model);
        
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(style)) options:NSKeyValueObservingOptionInitial block:^(id observer, id object, NSDictionary *change) {
            
            @strongify(model);
            @strongify(cell);
            
            switch (model.style) {
                case PR_PriceButtonStylePrice:
                {
                    if (model.discountPrice) {
                        [cell.priceAndBuyButton setPrice:[NSString stringWithFormat:@"¥%.2f", model.discountPrice]];
                    } else {
                        [cell.priceAndBuyButton setPrice:@"购买"];
                    }
                    cell.priceAndBuyButton.style = PR_PriceButtonStylePrice;
                }
                    break;
                case PR_PriceButtonStylePurchase:
                {
                    cell.priceAndBuyButton.style = PR_PriceButtonStylePurchase;
                }
                    break;
                case PR_PriceButtonStyleActivity:
                {
                    cell.priceAndBuyButton.style = PR_PriceButtonStyleActivity;
                }
                    break;
                case PR_PriceButtonStyleShoppingCart:
                {
                    cell.priceAndBuyButton.style = PR_PriceButtonStyleShoppingCart;
                }
                    break;
                default:
                    break;
            }
        }];
        
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(discountPrice)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            
            @strongify(cell);
            
            if (model.discountPrice > 0) {
                [cell.priceAndBuyButton setPrice:[NSString stringWithFormat:@"¥%.2f", model.discountPrice]];
            } else {
                [cell.priceAndBuyButton setPrice:@"购买"];
            }
        }];
        
        @weakify(self);
        
        cell.priceAndBuyButton.buttonAction = ^(PR_PriceButtonOption option){
            
            @strongify(model);
            
            switch (option) {
                case PR_PriceButtonOptionPreparePurchase:
                {
                    model.style = PR_PriceButtonStylePurchase;
                }
                    break;
                case PR_PriceButtonOptionPurchase:
                {
                    model.style = PR_PriceButtonStyleActivity;
                    
                    [[PR_ShoppingCartService service] addMerchandise:model success:nil failure:^(NSError *error) {
                        [self showMessage:error.localizedDescription];
                    }];
                    
                    model.style = PR_PriceButtonStyleShoppingCart;
                }
                    break;
                case PR_PriceButtonOptionCancel:
                {
                    model.style = PR_PriceButtonStylePrice;
                }
                    break;
                case PR_PriceButtonOptionShoppingCart:
                {
                    @strongify(self);
                    [self.tabBarController setSelectedIndex:3];
                }
                    break;
                default:
                    break;
            }
        };
    }
    
    /**
     *  设置图片
     */
    {
        [cell.KVOController observe:model.icon keyPath:NSStringFromSelector(@selector(image)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            if (model.icon.image) {
                cell.customImageView.image = model.icon.image;
            } else {
                cell.customImageView.image = [UIImage imageNamed:@"Placeholder_64_64"];
            }
        }];
        
        if (model.icon.image) {
            cell.customImageView.image = model.icon.image;
        } else {
            cell.customImageView.image = [UIImage imageNamed:@"Placeholder_64_64"];
            if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
            {
                [self.viewModel startIconDownloadForIndexPath:indexPath];
            }
        }
    }
}

@end


@implementation PR_MerchandisesViewController (Helper)

+ (NSString *)titleForStyle:(PR_MerchandisesStyle)style
{
    NSString *title = @"";
    switch (style) {
        case PR_MerchandisesStyleFeatured:
            title = @"精品";
            break;
        case PR_MerchandisesStyleAll:
            title = @"全部";
            break;
        case PR_MerchandisesStyleHistory:
            title = @"历史";
            break;
        default:
            break;
    }
    return title;
}

+ (UIImage *)tabBarItemImageForStyle:(PR_MerchandisesStyle)style
{
    NSString *imageName = @"";
    switch (style) {
        case PR_MerchandisesStyleFeatured:
            imageName = @"star";
            break;
        case PR_MerchandisesStyleAll:
            imageName = @"globe";
            break;
        case PR_MerchandisesStyleHistory:
            imageName = @"clock";
            break;
        default:
            break;
    }
    UIImage *image = nil;
    if (imageName.length > 0) {
        image = [UIImage imageNamed:imageName];
    }
    return image;
}

@end
