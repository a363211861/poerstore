//
//  PR_PriceButton.m
//  PR_PriceButton
//
//  Created by admin on 15/8/24.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_PriceButton.h"

#import "PR_ActivityIndicatorView.h"
#import "PR_PurchaseCoverView.h"

#import <PureLayout/PureLayout.h>
#import <libextobjc/EXTScope.h>

#import "UIImage+PR_Category.h"
#import "UIColor+PR_Category.h"


static NSString * const kPR_PriceButtonStyleKey = @"style";
NSInteger const kPR_PriceButtonCornerRadius = 5;
CGFloat const kPR_PriceButtonBorderWidth = 1;
NSInteger const kPR_PriceButtonFontSize = 15;

@interface PR_PriceButton ()

@property (assign, nonatomic) BOOL hasInit;

@property (weak, nonatomic) UIButton *priceButton;
@property (weak, nonatomic) UIButton *purchaseButton;
@property (weak, nonatomic) PR_ActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) UIButton *shoppingCartButton;

@property (strong, nonatomic) PR_PurchaseCoverView *PurchaseCoverView;

@end

@implementation PR_PriceButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    if (self.hasInit) {
        return;
    }
    self.hasInit = YES;
    self.style = PR_PriceButtonStylePrice;
    
    {
        UIButton *button = [[UIButton alloc] initForAutoLayout];
        button.layer.cornerRadius = kPR_PriceButtonCornerRadius;
        button.layer.borderColor = button.tintColor.CGColor;
        button.layer.borderWidth = kPR_PriceButtonBorderWidth;
        button.titleLabel.font = [UIFont systemFontOfSize:kPR_PriceButtonFontSize];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitleColor:button.tintColor forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        UIImage *image = [UIImage imageWithColor:button.tintColor cornerRadius:kPR_PriceButtonCornerRadius];
        UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius)];
        [button setBackgroundImage:resizableImage forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(priceAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.priceButton = button;
        
        [button autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [button autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        [button autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [button autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
    }
    
    {
        UIColor *tintColor = [UIColor colorWithR:68 G:159 B:27 A:1];
        UIButton *button = [[UIButton alloc] initForAutoLayout];
        button.layer.cornerRadius = kPR_PriceButtonCornerRadius;
        button.layer.borderColor = tintColor.CGColor;
        button.layer.borderWidth = kPR_PriceButtonBorderWidth;
        button.titleLabel.font = [UIFont systemFontOfSize:kPR_PriceButtonFontSize];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitleColor:tintColor forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        UIImage *image = [UIImage imageWithColor:tintColor cornerRadius:kPR_PriceButtonCornerRadius];
        UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius)];
        [button setBackgroundImage:resizableImage forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(purchaseAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.purchaseButton = button;
        
        [button autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [button autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        [button autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [button autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
        
        [button setTitle:@"购买" forState:UIControlStateNormal];
    }
    
    {
        PR_ActivityIndicatorView *activityIndicatorView = [[PR_ActivityIndicatorView alloc] initForAutoLayout];
        activityIndicatorView.hidden = YES;
        activityIndicatorView.hidesWhenStopped = YES;
        activityIndicatorView.userInteractionEnabled = NO;
        activityIndicatorView.tintColor = self.priceButton.tintColor;
        [self addSubview:activityIndicatorView];
        self.activityIndicatorView = activityIndicatorView;
        
        [activityIndicatorView autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [activityIndicatorView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        [activityIndicatorView autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [activityIndicatorView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionHeight ofView:activityIndicatorView];
    }
    
    {
        UIButton *button = [[UIButton alloc] initForAutoLayout];
        button.layer.cornerRadius = kPR_PriceButtonCornerRadius;
        button.layer.borderColor = [UIColor blueColor].CGColor;
        button.layer.borderWidth = kPR_PriceButtonBorderWidth;
        button.titleLabel.font = [UIFont systemFontOfSize:kPR_PriceButtonFontSize];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        UIImage *image = [UIImage imageWithColor:button.tintColor cornerRadius:kPR_PriceButtonCornerRadius];
        UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius, kPR_PriceButtonCornerRadius)];
        [button setBackgroundImage:resizableImage forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(shoppingCartAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.shoppingCartButton = button;
        
        [button autoPinEdgeToSuperviewEdge:ALEdgeTop];
        [button autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        [button autoPinEdgeToSuperviewEdge:ALEdgeRight];
        [button autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
        
        [button setTitle:@"购物车" forState:UIControlStateNormal];
    }
    
    [self addObserver:self forKeyPath:kPR_PriceButtonStyleKey options:NSKeyValueObservingOptionInitial context:nil];
}

- (void)showPurchaseCoverView
{
    [PR_PurchaseCoverView shareCoverView].respondView = self.purchaseButton;
    
    @weakify(self);
    
    [[PR_PurchaseCoverView shareCoverView] showWithCancel:^{
        
        @strongify(self);
        
        if (self.buttonAction) {
            self.buttonAction(PR_PriceButtonOptionCancel);
        }
    }];
}

- (void)hiddenPurchaseCoverView
{
    [[PR_PurchaseCoverView shareCoverView] dismiss];
    [PR_PurchaseCoverView shareCoverView].respondView = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self && [keyPath isEqualToString:kPR_PriceButtonStyleKey]) {
        
        switch (self.style) {
            case PR_PriceButtonStylePrice:
            {
                self.priceButton.hidden = NO;
                self.purchaseButton.hidden = YES;
                self.shoppingCartButton.hidden = YES;
                [self.activityIndicatorView stopAnimating];
                
                [self hiddenPurchaseCoverView];
            }
                break;
            case PR_PriceButtonStylePurchase:
            {
                self.priceButton.hidden = YES;
                self.purchaseButton.hidden = NO;
                self.shoppingCartButton.hidden = YES;
                [self.activityIndicatorView stopAnimating];
                
                [self showPurchaseCoverView];
            }
                break;
            case PR_PriceButtonStyleActivity:
            {
                self.priceButton.hidden = YES;
                self.purchaseButton.hidden = YES;
                self.shoppingCartButton.hidden = YES;
                [self.activityIndicatorView startAnimating];
                
                [self hiddenPurchaseCoverView];
            }
                break;
            case PR_PriceButtonStyleShoppingCart:
            {
                self.priceButton.hidden = YES;
                self.purchaseButton.hidden = YES;
                self.shoppingCartButton.hidden = NO;
                [self.activityIndicatorView stopAnimating];
            }
            default:
                break;
        }
        
    } else {
        
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:kPR_PriceButtonStyleKey];
    
#if DEBUG
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
#endif
}

- (void)priceAction:(UIButton *)button
{
    if (self.buttonAction) {
        self.buttonAction(PR_PriceButtonOptionPreparePurchase);
    }
}

- (void)purchaseAction:(UIButton *)button
{
    if (self.buttonAction) {
        self.buttonAction(PR_PriceButtonOptionPurchase);
    }
}

- (void)shoppingCartAction:(UIButton *)button
{
    if (self.buttonAction) {
        self.buttonAction(PR_PriceButtonOptionShoppingCart);
    }
}

- (void)setPrice:(NSString *)price
{
    if (!price) {
        price = @"";
    }
    [self.priceButton setTitle:price forState:UIControlStateNormal];
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:self.priceButton.titleLabel.font forKey:NSFontAttributeName];
    CGSize size = [price sizeWithAttributes:attributes];
    
    self.widthConstraint.constant = ceilf(size.width) + 20;
}

@end
