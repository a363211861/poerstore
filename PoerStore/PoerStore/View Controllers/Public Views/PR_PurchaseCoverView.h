//
//  PR_PurchaseCoverView.h
//  PoerStore
//
//  Created by 伟 晏 on 15/11/11.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PR_PurchaseCoverView : UIView

+ (instancetype)shareCoverView;

@property (assign, nonatomic) UIView *respondView;

- (void)showWithCancel:(dispatch_block_t)cancel;

- (void)dismiss;

@end
