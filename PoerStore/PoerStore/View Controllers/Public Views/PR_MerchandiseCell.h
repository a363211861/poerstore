//
//  PR_MerchandiseCell.h
//  PoerStore
//
//  Created by 伟 晏 on 15/5/23.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"

extern NSInteger const kPR_MerchandiseCellHeight;

@class PR_PriceButton;

@interface PR_MerchandiseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *customImageView;
@property (weak, nonatomic) IBOutlet UILabel *customTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *customDetailTextLabel;
@property (weak, nonatomic) IBOutlet PR_PriceButton *priceAndBuyButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceButtonWidthConstraint;

@end
