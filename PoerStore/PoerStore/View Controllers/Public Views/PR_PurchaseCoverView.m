//
//  PR_PurchaseCoverView.m
//  PoerStore
//
//  Created by 伟 晏 on 15/11/11.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_PurchaseCoverView.h"

#import <PureLayout/PureLayout.h>

@interface PR_PurchaseCoverView ()

@property (copy, nonatomic) dispatch_block_t cancel;

@end


@implementation PR_PurchaseCoverView

+ (instancetype)shareCoverView
{
    static PR_PurchaseCoverView *_shareCoverView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareCoverView = [[PR_PurchaseCoverView alloc] init];
    });
    return _shareCoverView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        UIView *rootView = [self rootView];
        self.frame = rootView.bounds;
        [rootView addSubview:self];
        [self addSelfConstraints];
        
        self.userInteractionEnabled = YES;
        self.hidden = YES;
    }
    return self;
}

- (UIView *)rootView
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    UIView *rootView = window.rootViewController.view;
    if (!rootView) {
        rootView = window;
    }
    
    return rootView;
}

- (void)showWithCancel:(dispatch_block_t)cancel
{
    self.cancel = cancel;
    self.hidden = NO;
}

- (void)dismiss
{
    self.hidden = YES;
}

- (void)addSelfConstraints
{
    [self autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [self autoPinEdgeToSuperviewEdge:ALEdgeBottom];
    [self autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self autoPinEdgeToSuperviewEdge:ALEdgeTrailing];
}

- (void)removeSelfAllConstraints
{
    [self removeConstraints:self.constraints];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *targetView = nil;
    
    if (self.respondView) {
        
        CGRect respondFrame = [[self rootView] convertRect:self.respondView.frame fromView:self.respondView.superview];
        CGRect targetFrame = [self convertRect:respondFrame fromView:[self rootView]];
        
        if(CGRectContainsPoint(targetFrame, point)) {
            
            targetView = self.respondView;
        }
    }
    
    if (!targetView) {
        
        targetView = [super hitTest:point withEvent:event];
    }
    
    return targetView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.cancel) {
        self.cancel();
    }
}

@end
