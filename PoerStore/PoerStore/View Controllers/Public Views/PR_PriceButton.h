//
//  PR_PriceButton.h
//  PR_PriceButton
//
//  Created by admin on 15/8/24.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PR_DataStruct.h"

@interface PR_PriceButton : UIView

@property (assign, nonatomic) PR_PriceButtonStyle style;
@property (weak, nonatomic) NSLayoutConstraint *widthConstraint;

- (void)setPrice:(NSString *)price;

@property (copy, nonatomic) void(^buttonAction)(PR_PriceButtonOption option);

@property (copy, nonatomic) dispatch_block_t cancelPurchase;

@end
