//
//  PR_MerchandiseCell.m
//  PoerStore
//
//  Created by 伟 晏 on 15/5/23.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseCell.h"

#import "UIImage+PR_Category.h"

#import "PR_PriceButton.h"

NSInteger const kPR_MerchandiseCellHeight = 81;

@interface PR_MerchandiseCell ()

@end

@implementation PR_MerchandiseCell

- (void)commonInit
{
    self.priceAndBuyButton.widthConstraint = self.priceButtonWidthConstraint;
}

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
