//
//  PR_ActivityIndicatorView.m
//  PR_PriceButton
//
//  Created by admin on 15/8/24.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_ActivityIndicatorView.h"

extern NSString * const UIViewControllerViewWillAppear;

NSString *const MRActivityIndicatorViewSpinAnimationKey = @"MRActivityIndicatorViewSpinAnimationKey";

@interface PR_ActivityIndicatorView ()

@property (strong, nonatomic) UIViewController *viewController;

@end

@implementation PR_ActivityIndicatorView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)dealloc
{
#if DEBUG
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (Class)layerClass {
    return CAShapeLayer.class;
}

- (CAShapeLayer *)shapeLayer {
    return (CAShapeLayer *)self.layer;
}

- (void)commonInit {
    self.hidesWhenStopped = YES;
    
    self.layer.borderWidth = 0;
    self.shapeLayer.lineWidth = 1.0f;
    self.shapeLayer.fillColor = UIColor.clearColor.CGColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewControllerWillAppear:) name:UIViewControllerViewWillAppear object:nil];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.frame;
    if (frame.size.width != frame.size.height) {
        // Ensure that we have a square frame
        CGFloat s = MAX(frame.size.width, frame.size.height);
        frame.size.width = s;
        frame.size.height = s;
        self.frame = frame;
    }
    
    self.shapeLayer.path = [self layoutPath].CGPath;
}

- (UIBezierPath *)layoutPath {
    const double TWO_M_PI = 2.0*M_PI;
    double startAngle = 0.75 * TWO_M_PI;
    double endAngle = startAngle + TWO_M_PI * 0.9;
    
    CGFloat width = self.bounds.size.width;
    return [UIBezierPath bezierPathWithArcCenter:CGPointMake(width/2.0f, width/2.0f)
                                          radius:width/2.0f
                                      startAngle:startAngle
                                        endAngle:endAngle
                                       clockwise:YES];
}


#pragma mark - Hook tintColor

- (void)tintColorDidChange  {
    [super tintColorDidChange];
    self.shapeLayer.strokeColor = self.tintColor.CGColor;
}

#pragma mark - View Controller

- (UIViewController *)viewController
{
    if (_viewController) {
        
        return _viewController;
    }
    
    UIResponder *responder = self;
    while ((responder = [responder nextResponder])) {
        
        if ([responder isKindOfClass: [UIViewController class]]) {
            
            _viewController = (UIViewController *)responder;
            break;
        }
    }
    
    return _viewController;
}

#pragma mark - Action

- (void)viewControllerWillAppear:(NSNotification *)notification
{
    UIViewController *viewController = [notification object];
    if (_animating && self.viewController == viewController) {
        
        [self startAnimating];
    }
}


#pragma mark - Control animation

- (void)startAnimating {
    _animating = YES;
    
    [self.layer removeAnimationForKey:MRActivityIndicatorViewSpinAnimationKey];
    
    CABasicAnimation *spinAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spinAnimation.toValue = @(1*2*M_PI);
    spinAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    spinAnimation.duration = 1.0;
    spinAnimation.repeatCount = INFINITY;
    [self.layer addAnimation:spinAnimation forKey:MRActivityIndicatorViewSpinAnimationKey];
    
    if (self.hidesWhenStopped) {
        self.hidden = NO;
    }
}

- (void)stopAnimating {
    _animating = NO;
    
    [self.layer removeAnimationForKey:MRActivityIndicatorViewSpinAnimationKey];
    
    if (self.hidesWhenStopped) {
        self.hidden = YES;
    }
}

- (BOOL)isAnimating {
    return _animating;
}

@end
