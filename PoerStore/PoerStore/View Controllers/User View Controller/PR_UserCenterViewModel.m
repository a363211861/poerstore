//
//  PR_UserCenterViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/13.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_UserCenterViewModel.h"

#import <libextobjc/EXTScope.h>

#import "PR_UserRequest.h"

#import "PR_UserModel.h"
#import "PR_UserInfoModel.h"

@interface PR_UserCenterViewModel ()

@property (strong, nonatomic) PR_HTTPRequest *dataTask;
@property (strong, nonatomic) PR_UserInfoModel *userInfo;

@end

@implementation PR_UserCenterViewModel

- (void)reloadData
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    
    self.dataTask = [PR_UserRequest requestUserInfoWithSuccess:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        self.userInfo = responseObject;
        
        if (self.updatedContent) {
            self.updatedContent();
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

- (NSString *)username
{
    PR_UserModel *user = [PR_User user].userModel;
    
    NSString *username = user.username;
    
    return username;
}

- (NSString *)waitPayOrderCountString
{
    NSString *waitPayOrderCountString = [NSString stringWithFormat:@"%@", self.userInfo.waitPayOrderCount];
    
    return waitPayOrderCountString;
}

- (NSString *)transportOrderCountString
{
    NSString *transportOrderCountString = [NSString stringWithFormat:@"%@", self.userInfo.transportOrderCount];
    
    return transportOrderCountString;
}

- (NSString *)finsihOrderCountString
{
    NSString *finsihOrderCountString = [NSString stringWithFormat:@"%@", self.userInfo.finsihOrderCount];
    
    return finsihOrderCountString;
}

- (NSString *)addressCountString
{
    NSString *addressCountString = [NSString stringWithFormat:@"%@", self.userInfo.addressCount];
    
    return addressCountString;
}

@end
