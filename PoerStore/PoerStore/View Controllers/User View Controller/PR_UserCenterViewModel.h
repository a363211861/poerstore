//
//  PR_UserCenterViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/13.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@interface PR_UserCenterViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t updatedContent;

- (void)reloadData;

- (NSString *)username;
- (NSString *)waitPayOrderCountString;
- (NSString *)transportOrderCountString;
- (NSString *)finsihOrderCountString;
- (NSString *)addressCountString;

@end
