//
//  PR_UserCenterViewController.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_UserCenterViewController.h"

#import <libextobjc/EXTScope.h>

#import "PR_UserCenterViewModel.h"


#import "UIStoryboard+PR_Category.h"

#import "PR_OrdersViewController.h"
#import "PR_OrderViewController.h"

#import "PR_AddressesViewController.h"
#import "PR_AddressViewController.h"

#import "PR_ResetPasswordViewController.h"

#import "PR_FeedbackViewController.h"


@interface PR_UserCenterViewController () <UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *waitPayOrderCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *transportOrderCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishOrderCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressCountLabel;

@property (strong, nonatomic) PR_UserCenterViewModel *viewModel;

@end

@implementation PR_UserCenterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"我的";
    
    self.viewModel = [[PR_UserCenterViewModel alloc] init];
    
    @weakify(self);
    self.viewModel.updatedContent = ^() {
        @strongify(self);
        [self updatedUserInfoShow];
    };
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.refresh = ^() {
        @strongify(self);
        [self refresh];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.viewModel.active = NO;
}

#pragma mark - refresh

- (void)refresh
{
    [self.viewModel reloadData];
}

- (void)updatedUserInfoShow
{
    self.usernameLabel.text = [self.viewModel username];
    self.waitPayOrderCountLabel.text = [self.viewModel waitPayOrderCountString];
    self.transportOrderCountLabel.text = [self.viewModel transportOrderCountString];
    self.finishOrderCountLabel.text = [self.viewModel finsihOrderCountString];
    self.addressCountLabel.text = [self.viewModel addressCountString];
}

#pragma mark - go where

/**
 *  跳转到待支付订单列表
 */
- (void)goWaitPayOrderViewController
{
    PR_OrdersViewController *viewController = [[UIStoryboard orderStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_OrdersViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

/**
 *  跳转到待收货订单列表
 */
- (void)goTransportOrderViewController
{
    PR_OrdersViewController *viewController = [[UIStoryboard orderStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_OrdersViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

/**
 *  跳转到全部订单列表
 */
- (void)goAllOrderViewController
{
    PR_OrdersViewController *viewController = [[UIStoryboard orderStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_OrdersViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

/**
 *  跳转到收货地址列表
 */
- (void)goAddressesViewController
{
    PR_FeedbackViewController *viewController = [[UIStoryboard addressStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_AddressesViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

/**
 *  重置密码
 */
- (void)goChangePasswordViewController
{
    PR_ResetPasswordViewController *viewController = [[UIStoryboard authorizeStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_ResetPasswordViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

/**
 *  用户反馈
 */
- (void)goFeedBackViewController
{
    PR_FeedbackViewController *viewController = [[UIStoryboard userStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_FeedbackViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

/**
 *  注销
 */
- (void)signOut
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"确定要退出当前账号吗？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"注销" otherButtonTitles:nil];
    [actionSheet showInView:self.view];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section, row = indexPath.row;
    
    if (section == 1) {
        
        if (row == 0) {
            
            [self goWaitPayOrderViewController];
            
        } else if (row == 1) {
            
            [self goTransportOrderViewController];
            
        } else if (row == 2) {
            
            [self goAllOrderViewController];
        }
        
    } else if (section == 2) {
        
        if (row == 0) {
            
            [self goAddressesViewController];
        }
        
    } else if (section == 3) {
        
        if (row == 0) {
            
            [self goChangePasswordViewController];
        }
        
    } else if (section == 4) {
        
        if (row == 0) {
            
            [self goFeedBackViewController];
        }
        
    } else if (section == 5) {
        
        if (row == 0) {
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            [self signOut];
        }
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSLog(@"注销");
    }
}

@end
