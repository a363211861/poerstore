//
//  PR_FeedbackViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_FeedbackViewController.h"

#import "UIImage+PR_Category.h"

#import "PR_FeedbackViewModel.h"


static NSString * const PR_FeedbackTextViewPlaceholder = @"您的每一个建议，都是我们前进的动力。";

@interface PR_FeedbackViewController ()

@property (strong, nonatomic) PR_FeedbackViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UIImageView *textViewBackgroundImageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation PR_FeedbackViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"反馈";
    
    self.viewModel = [[PR_FeedbackViewModel alloc] init];
    
    @weakify(self);
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.feedBackSuccess = ^() {
        @strongify(self);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"谢谢您的反馈，我们会珍惜您写给我们的每一个字。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleDone target:self action:@selector(sendAction:)];
        self.navigationItem.rightBarButtonItem = barButtonItem;
    }
    
    {
        self.textView.textColor = [UIColor lightGrayColor];
        self.textView.text = PR_FeedbackTextViewPlaceholder;
    }
    
    {
        self.textViewBackgroundImageView.image = [self textViewBackgroundImageForColor:[UIColor colorWithWhite:0.8 alpha:1]];
    }
}

#pragma mark - funcs

- (UIImage *)textViewBackgroundImageForColor:(UIColor *)color
{
    UIImage *image = [UIImage buttonImageWithColor:[UIColor whiteColor] cornerRadius:5 shadowColor:color  shadowInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
    UIImage *resizableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) resizingMode:UIImageResizingModeStretch];
    
    return resizableImage;
}

#pragma mark - action

- (void)sendAction:(UIBarButtonItem *)barButtonItem
{
    NSString *text = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *message = nil;
    do {
        
        if (text.length == 0) {
            
            message = @"亲，要写下您的建议，我们才会知道哟。";
            
            break;
        }
        
        if (![text isEqualToString:PR_FeedbackTextViewPlaceholder]) {
            
            message = @"亲，要写下您的建议，我们才会知道哟。";
            
            break;
        }
        
        [self.textView resignFirstResponder];
        self.textView.editable = NO;
        
        [self.viewModel sendFeedBack:text];
        
        return;
        
    } while (0);
    
    if (message.length > 0) {
        
        [self showMessage:message];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.textColor = [UIColor blackColor];
    NSString *text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([text isEqualToString:PR_FeedbackTextViewPlaceholder]) {
        
        textView.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSString *text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([text isEqualToString:PR_FeedbackTextViewPlaceholder]) {
    
        textView.textColor = [UIColor lightGrayColor];
    
    } else if (text.length == 0) {
        
        textView.text = PR_FeedbackTextViewPlaceholder;
        textView.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
