//
//  PR_FeedbackViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@interface PR_FeedbackViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t feedBackSuccess;

- (void)sendFeedBack:(NSString *)context;

@end
