//
//  PR_AddressViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressViewController.h"

#import "PR_AddressViewModel.h"
#import "PR_UserAddressModel.h"

#import <KVOController/KVOController.h>

#import "UITextField+PR_Category.h"

#import "UIStoryboard+PR_Category.h"
#import "PR_AddressAreaPickerViewController.h"
#import "PR_AddressNeighbourhoodPickerViewController.h"

@interface PR_AddressViewController ()

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *areaCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *streetCell;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UITextField *detailTextField;
@property (weak, nonatomic) IBOutlet UITextField *postcodeTextField;

@end

@implementation PR_AddressViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"地址";
    
    [self customInit];
    
    [self bindViewModel];
    
    self.viewModel.viewTypeDidChanged();
}

#pragma mark - funcs

- (void)bindViewModel
{
    @weakify(self);
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
    
    self.viewModel.viewTypeDidChanged = ^() {
        @strongify(self);
        
        switch (self.viewModel.viewType) {
            case PR_AddressViewNew:
            {
                [self showSaveButton];
                [self changeViewForEdit];
            }
                break;
            case PR_AddressViewRead:
            {
                [self showEditAndDeleteButtonWithAnimate:YES];
                [self changeViewForNotEdit];
            }
                break;
            case PR_AddressViewEdit:
            {
                [self showSaveAndCancelButtonWithAnimate:YES];
                [self changeViewForEdit];
            }
                break;
            default:
                break;
        }
    };
    
    [self.KVOController observe:self.viewModel keyPath:NSStringFromSelector(@selector(viewType)) options:NSKeyValueObservingOptionInitial block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        if (self.viewModel.viewType == PR_AddressViewNew || self.viewModel.viewType == PR_AddressViewEdit) {
            [self bindModel];
        } else {
            [self.viewModel.address.KVOController unobserveAll];
        }
    }];
}

- (void)bindModel
{
    [self.viewModel.address.KVOController unobserveAll];
    
    @weakify(self);
    
    [self.viewModel.address.KVOController observe:self.userNameTextField keyPath:NSStringFromSelector(@selector(text)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        self.viewModel.address.addressee = self.userNameTextField.text;
    }];
    
    [self.viewModel.address.KVOController observe:self.phoneNumberTextField keyPath:NSStringFromSelector(@selector(text)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        self.viewModel.address.phoneNumber = self.phoneNumberTextField.text;
    }];
    
    [self.viewModel.address.KVOController observe:self.detailTextField keyPath:NSStringFromSelector(@selector(text)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        self.viewModel.address.detail = self.detailTextField.text;
    }];
    
    [self.viewModel.address.KVOController observe:self.postcodeTextField keyPath:NSStringFromSelector(@selector(text)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        self.viewModel.address.postcode = self.postcodeTextField.text;
    }];
    
    [self.KVOController observe:self.viewModel.address keyPaths:@[NSStringFromSelector(@selector(province)), NSStringFromSelector(@selector(city)), NSStringFromSelector(@selector(district))] options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        NSMutableString *addressString = [NSMutableString string];
        if (self.viewModel.address.province.length > 0) {
            [addressString appendString:self.viewModel.address.province];
        }
        if (self.viewModel.address.city.length > 0) {
            [addressString appendString:self.viewModel.address.city];
        }
        if (self.viewModel.address.district.length > 0) {
            [addressString appendString:self.viewModel.address.district];
        }
        self.areaLabel.text = addressString;
    }];
    
    [self.KVOController observe:self.viewModel.address keyPath:NSStringFromSelector(@selector(neighbourhood)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        self.streetLabel.text = self.viewModel.address.neighbourhood;
    }];
}

#pragma mark - UI

- (void)customInit
{
    [self.userNameTextField disableAllAutoFunction];
    [self.phoneNumberTextField disableAllAutoFunction];
    [self.detailTextField disableAllAutoFunction];
    [self.postcodeTextField disableAllAutoFunction];
    
    if (self.viewModel.address) {
        
        self.userNameTextField.text = self.viewModel.address.addressee;
        self.phoneNumberTextField.text = self.viewModel.address.phoneNumber;
        {
            NSMutableString *addressString = [NSMutableString string];
            if (self.viewModel.address.province.length > 0) {
                [addressString appendString:self.viewModel.address.province];
            }
            if (self.viewModel.address.city.length > 0) {
                [addressString appendString:self.viewModel.address.city];
            }
            if (self.viewModel.address.district.length > 0) {
                [addressString appendString:self.viewModel.address.district];
            }
            self.areaLabel.text = addressString;
        }
        self.streetLabel.text = self.viewModel.address.neighbourhood;
        self.postcodeTextField.text = self.viewModel.address.postcode;
        self.detailTextField.text = self.viewModel.address.detail;
    }
}

- (void)changeViewForEdit
{
    self.userNameTextField.enabled = YES;
    self.phoneNumberTextField.enabled = YES;
    self.detailTextField.enabled = YES;
    self.postcodeTextField.enabled = YES;
    
    self.tableView.allowsSelection = YES;
    self.areaCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.streetCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

- (void)changeViewForNotEdit
{
    self.userNameTextField.enabled = NO;
    self.phoneNumberTextField.enabled = NO;
    self.detailTextField.enabled = NO;
    self.postcodeTextField.enabled = NO;
    
    self.tableView.allowsSelection = NO;
    self.areaCell.accessoryType = UITableViewCellAccessoryNone;
    self.streetCell.accessoryType = UITableViewCellAccessoryNone;
}

- (void)showSaveButton
{
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(addAction:)];
    self.navigationItem.rightBarButtonItem = barButtonItem;
}

- (void)showEditAndDeleteButtonWithAnimate:(BOOL)animate
{
    NSMutableArray *barButtonItems = [NSMutableArray array];
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"删除" style:UIBarButtonItemStylePlain target:self action:@selector(deleteAction:)];
        [barButtonItems addObject:barButtonItem];
    }
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(editAction:)];
        [barButtonItems addObject:barButtonItem];
    }
    
    [self.navigationItem setRightBarButtonItems:barButtonItems animated:animate];
}

- (void)showSaveAndCancelButtonWithAnimate:(BOOL)animate
{
    NSMutableArray *barButtonItems = [NSMutableArray array];
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction:)];
        [barButtonItems addObject:barButtonItem];
    }
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction:)];
        [barButtonItems addObject:barButtonItem];
    }
    
    [self.navigationItem setRightBarButtonItems:barButtonItems animated:animate];
}

#pragma mark - action

- (void)addAction:(UIBarButtonItem *)barButtonItem
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveAction:(UIBarButtonItem *)barButtonItem
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelAction:(UIBarButtonItem *)barButtonItem
{
    self.viewModel.viewType = PR_AddressViewRead;
}

- (void)deleteAction:(UIBarButtonItem *)barButtonItem
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)editAction:(UIBarButtonItem *)barButtonItem
{
    self.viewModel.viewType = PR_AddressViewEdit;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.viewModel.viewType == PR_AddressViewRead) {
        return;
    }
    
    NSInteger row = indexPath.row;
    
    switch (row) {
        case 2:
        {
            PR_AddressAreaPickerViewController *viewController = [[UIStoryboard addressStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_AddressAreaPickerViewController class])];
            viewController.selectedProvinceCode = self.viewModel.address.provinceCode;
            viewController.selectedCityCode = self.viewModel.address.cityCode;
            viewController.selectedDistrictCode = self.viewModel.address.districtCode;
            
            @weakify(self);
            
            viewController.didSelectAddress = ^(PR_ProvinceModel *province, PR_CityModel *city, PR_DistrictModel *district) {
                
                @strongify(self);
                
                [self.viewModel selectProvince:province];
                [self.viewModel selectCity:city];
                [self.viewModel selectDistrict:district];
            };
            
            viewController.dismiss = ^(){
                
                @strongify(self);
                [self dismissViewControllerAnimated:YES completion:NULL];
            };
            
            viewController.modalPresentationStyle = UIModalPresentationCustom;
            [self presentViewController:viewController animated:YES completion:NULL];
        }
            break;
        case 3:
        {
            PR_DistrictModel *district = [self.viewModel selectedDistrictModel];
            if (!district) {
                [self alertMessage:@"请选择所在城市"]; return;
            }
            PR_AddressNeighbourhoodPickerViewController *viewController = [[UIStoryboard addressStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_AddressNeighbourhoodPickerViewController class])];
            viewController.selectedNeighbourhoodCode = self.viewModel.address.neighbourhoodCode;
            viewController.districtModel = district;
            
            @weakify(self);
            
            viewController.didSelectNeighbourhood = ^(PR_NeighbourhoodModel *neighbourhood) {
                
                @strongify(self);
                
                [self.viewModel selectNeighbourhood:neighbourhood];
            };
            
            viewController.dismiss = ^(){
                
                @strongify(self);
                [self dismissViewControllerAnimated:YES completion:NULL];
            };
            
            viewController.modalPresentationStyle = UIModalPresentationCustom;
            [self presentViewController:viewController animated:YES completion:NULL];
        }
            break;
        default:
            break;
    }
}


@end
