//
//  PR_AddressAreaPickerViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@class PR_ProvinceModel;
@class PR_CityModel;
@class PR_DistrictModel;

@interface PR_AddressAreaPickerViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t updateContent;

@property (strong, nonatomic, readonly) PR_ProvinceModel *selectedProvince;
@property (strong, nonatomic, readonly) PR_CityModel *selectedCity;
@property (strong, nonatomic, readonly) PR_DistrictModel *selectedDistrict;

@property (copy, nonatomic) NSString *selectedProvinceCode;
@property (copy, nonatomic) NSString *selectedCityCode;
@property (copy, nonatomic) NSString *selectedDistrictCode;

@property (copy, nonatomic) void(^selectRow)(NSInteger row, NSInteger component);

- (void)reloadData;

- (void)autoSelectCity;
- (void)autoSelectDistrict;

- (NSInteger)numberOfComponents;
- (NSInteger)numberOfRowsInComponent:(NSInteger)component;
- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component;

- (void)didSelectRow:(NSInteger)row inComponent:(NSInteger)component;

@end
