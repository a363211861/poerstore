//
//  PR_AddressesViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressesViewModel.h"

#import <libextobjc/EXTScope.h>
#import <UIKit/UITableView.h>

#import "PR_UserAddressModel.h"

#import "PR_AddressViewModel.h"

#import "PR_AddressRequest.h"

@interface PR_AddressesViewModel ()

@property (strong, nonatomic) PR_HTTPRequest *dataTask;
@property (strong, nonatomic) NSMutableArray *addresses;

@end

@implementation PR_AddressesViewModel

- (instancetype)init
{
    self = [super init];
    if (self == nil) return nil;
    
    self.addresses = [NSMutableArray array];
    
    return self;
}

- (void)reloadData
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    
    self.dataTask = [PR_AddressRequest requestAddressesWithSuccess:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        [self.addresses setArray:responseObject];
        
        if (self.updatedContent) {
            self.updatedContent();
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}



- (void)loadNextPage
{
}


#pragma mark - Public Methods


- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    if (section == 0) {
        numberOfRows = self.addresses.count;
    }
    
    return numberOfRows;
}

- (PR_UserAddressModel *)addressModelForIndexPath:(NSIndexPath *)indexPath
{
    PR_UserAddressModel *address = nil;
    if (indexPath && indexPath.section == 0) {
        address = self.addresses[indexPath.row];
    }
    return address;
}

- (PR_AddressViewModel *)addressViewModelForIndexPath:(NSIndexPath *)indexPath
{
    PR_UserAddressModel *address = [self addressModelForIndexPath:indexPath];
    
    PR_AddressViewModel *viewModel = [[PR_AddressViewModel alloc] initWithAddress:address];
    viewModel.viewType = PR_AddressViewRead;
    
    return viewModel;
}

- (PR_AddressViewModel *)addressViewModelForNew
{
    PR_UserAddressModel *address = [[PR_UserAddressModel alloc] init];
    
    PR_AddressViewModel *viewModel = [[PR_AddressViewModel alloc] initWithAddress:address];
    viewModel.viewType = PR_AddressViewNew;
    
    return viewModel;
}

@end
