//
//  PR_AddressesViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@class PR_UserAddressModel;
@class PR_AddressViewModel;

@interface PR_AddressesViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t updatedContent;

- (void)reloadData;
- (void)loadNextPage;

- (NSInteger)numberOfRowsInSection:(NSInteger)section;
- (PR_UserAddressModel *)addressModelForIndexPath:(NSIndexPath *)indexPath;

- (PR_AddressViewModel *)addressViewModelForIndexPath:(NSIndexPath *)indexPath;
- (PR_AddressViewModel *)addressViewModelForNew;

@end
