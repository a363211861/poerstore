//
//  PR_AddressNeighbourhoodPickerViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/9/1.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

@class PR_DistrictModel, PR_NeighbourhoodModel;

@interface PR_AddressNeighbourhoodPickerViewModel : PR_ViewModel

- (instancetype)initWithDistrictModel:(PR_DistrictModel *)districtModel;

@property (copy, nonatomic) dispatch_block_t updateContent;

@property (strong, nonatomic, readonly) PR_NeighbourhoodModel *selectedNeighbourhood;

@property (copy, nonatomic) NSString *selectedNeighbourhoodCode;

@property (copy, nonatomic) void(^selectRow)(NSInteger row, NSInteger component);

- (NSInteger)numberOfComponents;
- (NSInteger)numberOfRowsInComponent:(NSInteger)component;
- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component;

- (void)didSelectRow:(NSInteger)row inComponent:(NSInteger)component;

@end
