//
//  PR_AddressViewController.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_TableViewController.h"

@class PR_AddressViewModel;

@interface PR_AddressViewController : PR_TableViewController

@property (strong, nonatomic) PR_AddressViewModel *viewModel;

@end
