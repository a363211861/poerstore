//
//  PR_AddressAreaPickerViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressAreaPickerViewModel.h"

#import "PR_AddressConfigModel.h"

@interface PR_AddressAreaPickerViewModel ()

@property (strong, nonatomic) NSArray<PR_ProvinceModel *> *provinceList;

@property (strong, nonatomic, readwrite) PR_ProvinceModel *selectedProvince;
@property (strong, nonatomic, readwrite) PR_CityModel *selectedCity;
@property (strong, nonatomic, readwrite) PR_DistrictModel *selectedDistrict;

@end


@implementation PR_AddressAreaPickerViewModel

- (void)reloadData
{
    [[PR_AddressListModel addressList] requestAddressListWithComplete:^(NSArray<PR_ProvinceModel *> *provinceList, NSError *error) {
        
        if (!error) {
            
            self.provinceList = [NSArray arrayWithArray:provinceList];
            if (self.updateContent) {
                self.updateContent();
            }
            
            for (PR_ProvinceModel *province in provinceList) {
                if ([province.provinceCode integerValue] == [self.selectedProvinceCode integerValue]) {
                    self.selectedProvince = province;
                    break;
                }
            }
            
            for (PR_CityModel *city in self.selectedProvince.citys) {
                if ([city.cityCode integerValue] == [self.selectedCityCode integerValue]) {
                    self.selectedCity = city;
                    break;
                }
            }
            
            for (PR_DistrictModel *district in self.selectedCity.districts) {
                if ([district.districtCode integerValue] == [self.selectedDistrictCode integerValue]) {
                    self.selectedDistrict = district;
                    break;
                }
            }
            
            if (!self.selectedProvince) {
                [self autoSelectProvince];
            }
            
        } else {
            
            if (self.error) {
                self.error(error);
            }
        }
    }];
}

- (void)autoSelectProvince
{
    [self autoSelectFirstRowForComponent:0];
    if (self.selectRow) {
        self.selectRow(0,0);
    }
}

- (void)autoSelectCity
{
    [self autoSelectFirstRowForComponent:1];
    if (self.selectRow) {
        self.selectRow(0,1);
    }
}

- (void)autoSelectDistrict
{
    [self autoSelectFirstRowForComponent:2];
    if (self.selectRow) {
        self.selectRow(0,2);
    }
}

- (void)autoSelectFirstRowForComponent:(NSInteger)component
{
    NSInteger numberOfComponents = [self numberOfComponents];
    if (numberOfComponents > component) {
        NSInteger numberOfRows = [self numberOfRowsInComponent:component];
        if (numberOfRows > 0) {
            [self didSelectRow:0 inComponent:component];
        }
    }
}

- (NSInteger)numberOfComponents
{
    return 3;
}

- (NSInteger)numberOfRowsInComponent:(NSInteger)component
{
    NSInteger numberOfRows = 0;
    
    switch (component) {
        case 0:
        {
            numberOfRows = self.provinceList.count;
        }
            break;
        case 1:
        {
            numberOfRows = self.selectedProvince.citys.count;
        }
            break;
        case 2:
        {
            numberOfRows = self.selectedCity.districts.count;
        }
            break;
        default:
            break;
    }
    
    return numberOfRows;
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = nil;
    
    switch (component) {
        case 0:
        {
            PR_ProvinceModel *province = self.provinceList[row];
            title = province.province;
        }
            break;
        case 1:
        {
            PR_CityModel *city = self.selectedProvince.citys[row];
            title = city.city;
        }
            break;
        case 2:
        {
            PR_DistrictModel *district = self.selectedCity.districts[row];
            title = district.district;
        }
            break;
        default:
            break;
    }
    
    return title;
}

- (void)didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch (component) {
        case 0:
        {
            self.selectedProvince = self.provinceList[row];
        }
            break;
        case 1:
        {
            self.selectedCity = self.selectedProvince.citys[row];
        }
            break;
        case 2:
        {
            self.selectedDistrict = self.selectedCity.districts[row];
        }
            break;
        default:
            break;
    }
}

@end
