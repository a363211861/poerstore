//
//  PR_AddressAreaPickerViewController.h
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewController.h"

@class PR_ProvinceModel;
@class PR_CityModel;
@class PR_DistrictModel;

@interface PR_AddressAreaPickerViewController : PR_ViewController

@property (copy, nonatomic) NSString *selectedProvinceCode;
@property (copy, nonatomic) NSString *selectedCityCode;
@property (copy, nonatomic) NSString *selectedDistrictCode;

@property (copy, nonatomic) dispatch_block_t dismiss;
@property (copy, nonatomic) void(^didSelectAddress)(PR_ProvinceModel *province, PR_CityModel *city, PR_DistrictModel *district);

@end
