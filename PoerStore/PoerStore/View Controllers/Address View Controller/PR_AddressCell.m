//
//  PR_AddressCell.m
//  PoerStore
//
//  Created by 伟 晏 on 16/2/15.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressCell.h"

NSInteger const kPR_AddressCellHeight = 80;

@interface PR_AddressCell ()

@end

@implementation PR_AddressCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
