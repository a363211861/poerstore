//
//  PR_AddressesViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressesViewController.h"

#import <KVOController/KVOController.h>

#import "PR_UserAddressModel.h"

#import "PR_AddressCell.h"

#import "PR_AddressesViewModel.h"

#import "UIStoryboard+PR_Category.h"
#import "PR_AddressViewController.h"

@interface PR_AddressesViewController ()

@property (strong, nonatomic) PR_AddressesViewModel *viewModel;

@end

@implementation PR_AddressesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"收货地址";
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStylePlain target:self action:@selector(addAction:)];
        self.navigationItem.rightBarButtonItem = barButtonItem;
    }
    
    self.tableView.rowHeight = kPR_AddressCellHeight;
    self.tableView.estimatedRowHeight = kPR_AddressCellHeight;
    [PR_AddressCell registerNIBForTableView:self.tableView];
    
    self.viewModel = [[PR_AddressesViewModel alloc] init];
    
    @weakify(self);
    self.viewModel.updatedContent = ^() {
        @strongify(self);
        [self.tableView reloadData];
    };
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.refresh = ^() {
        @strongify(self);
        [self refresh];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.viewModel.active = NO;
}

#pragma mark - action

- (void)addAction:(UIBarButtonItem *)barButtonItem
{
    PR_AddressViewController *viewController = [[UIStoryboard addressStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_AddressViewController class])];
    viewController.viewModel = [self.viewModel addressViewModelForNew];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - refresh

- (void)refresh
{
    [self.viewModel reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellIdentifier = NSStringFromClass([PR_AddressCell class]);
    });
    PR_AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PR_AddressViewController *viewController = [[UIStoryboard addressStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_AddressViewController class])];
    viewController.viewModel = [self.viewModel addressViewModelForIndexPath:indexPath];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Private Methods

- (void)configureCell:(PR_AddressCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    PR_UserAddressModel *model = [self.viewModel addressModelForIndexPath:indexPath];
    
    [cell.KVOController unobserveAll];
    
    
    @weakify(cell);
    
    /**
     *  设置name
     */
    {
        cell.addresseeLabel.text = model.addressee;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(addressee)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.addresseeLabel.text = model.addressee;
        }];
    }
    
    /**
     *  设置电话号码
     */
    {
        cell.phoneNumberLabel.text = model.phoneNumber;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(phoneNumber)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.phoneNumberLabel.text = model.phoneNumber;
        }];
    }
    
    /**
     *  设置detail
     */
    {
        cell.addressLabel.text = model.detail;
        [cell.KVOController observe:model keyPath:NSStringFromSelector(@selector(detail)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
            @strongify(cell);
            
            cell.addressLabel.text = model.detail;
        }];
    }
}

@end
