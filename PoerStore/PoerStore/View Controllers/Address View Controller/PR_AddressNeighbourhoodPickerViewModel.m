//
//  PR_AddressNeighbourhoodPickerViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/9/1.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressNeighbourhoodPickerViewModel.h"

#import "PR_AddressConfigModel.h"


@interface PR_AddressNeighbourhoodPickerViewModel ()

@property (strong, nonatomic) PR_DistrictModel *districtModel;

@property (strong, nonatomic, readwrite) PR_NeighbourhoodModel *selectedNeighbourhood;

@end


@implementation PR_AddressNeighbourhoodPickerViewModel

- (instancetype)initWithDistrictModel:(PR_DistrictModel *)districtModel
{
    self = [super init];
    if (self) {
        self.districtModel = districtModel;
    }
    return self;
}

- (void)setSelectedNeighbourhoodCode:(NSString *)selectedNeighbourhoodCode
{
    _selectedNeighbourhoodCode = [selectedNeighbourhoodCode copy];
    
    for (PR_NeighbourhoodModel *neighbourhood in self.districtModel.neighbourhoods) {
        
        if ([neighbourhood.neighbourhoodCode integerValue] == [selectedNeighbourhoodCode integerValue]) {
            
            self.selectedNeighbourhood = neighbourhood;
            
            if (self.selectRow) {
                self.selectRow([self.districtModel.neighbourhoods indexOfObject:neighbourhood], 0);
            }
            
            break;
        }
    }
}

- (NSInteger)numberOfComponents
{
    return 1;
}

- (NSInteger)numberOfRowsInComponent:(NSInteger)component
{
    NSInteger numberOfRows = self.districtModel.neighbourhoods.count;
    return numberOfRows;
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    PR_NeighbourhoodModel *neighbourhoodModel = self.districtModel.neighbourhoods[row];
    NSString *neighbourhood = neighbourhoodModel.neighbourhood;
    return neighbourhood;
}

- (void)didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedNeighbourhood = self.districtModel.neighbourhoods[row];
}

@end
