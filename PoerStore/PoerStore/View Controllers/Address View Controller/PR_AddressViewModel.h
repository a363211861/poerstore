//
//  PR_AddressViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

typedef NS_ENUM(NSInteger, PR_AddressViewType) {
    
    PR_AddressViewNew = 0,
    PR_AddressViewRead,
    PR_AddressViewEdit
};

@class PR_UserAddressModel;

@class PR_ProvinceModel, PR_CityModel, PR_DistrictModel, PR_NeighbourhoodModel;

@interface PR_AddressViewModel : PR_ViewModel

@property (copy, nonatomic) dispatch_block_t viewTypeDidChanged;

@property (assign, nonatomic) PR_AddressViewType viewType;

@property (strong, nonatomic, readonly) PR_UserAddressModel *address;

- (instancetype)initWithAddress:(PR_UserAddressModel *)address;

- (void)selectProvince:(PR_ProvinceModel *)province;
- (void)selectCity:(PR_CityModel *)city;
- (void)selectDistrict:(PR_DistrictModel *)district;
- (void)selectNeighbourhood:(PR_NeighbourhoodModel *)neighbourhood;

- (PR_DistrictModel *)selectedDistrictModel;

@end
