//
//  PR_AddressViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/1/18.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressViewModel.h"

#import "PR_UserAddressModel.h"

#import "PR_AddressConfigModel.h"

@interface PR_AddressViewModel ()

@property (strong, nonatomic, readwrite) PR_UserAddressModel *address;

@property (strong, nonatomic) PR_ProvinceModel *didSelectedProvince;
@property (strong, nonatomic) PR_CityModel *didSelectedCity;
@property (strong, nonatomic) PR_DistrictModel *didSelectedDistrict;
@property (strong, nonatomic) PR_NeighbourhoodModel *didSelectedNeighbourhoodModel;

@end

@implementation PR_AddressViewModel

- (instancetype)initWithAddress:(PR_UserAddressModel *)address
{
    self = [super init];
    if (self) {
        
        self.address = address;
        
        [self addObserver:self forKeyPath:NSStringFromSelector(@selector(viewType)) options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(viewType))];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (object == self && [keyPath isEqualToString:NSStringFromSelector(@selector(viewType))]) {
        
        if (self.viewTypeDidChanged) {
            self.viewTypeDidChanged();
        }
        
    } else {
        
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (PR_DistrictModel *)selectedDistrictModel
{
    return self.didSelectedDistrict;
}

- (void)selectProvince:(PR_ProvinceModel *)province
{
    self.didSelectedProvince = province;
    
    self.address.province = province.province;
    self.address.provinceCode = province.provinceCode;
}

- (void)selectCity:(PR_CityModel *)city
{
    self.didSelectedCity = city;
    
    self.address.city = city.city;
    self.address.cityCode = city.cityCode;
}

- (void)selectDistrict:(PR_DistrictModel *)district
{
    self.didSelectedDistrict = district;
    
    self.address.district = district.district;
    self.address.districtCode = district.districtCode;
}

- (void)selectNeighbourhood:(PR_NeighbourhoodModel *)neighbourhood
{
    self.didSelectedNeighbourhoodModel = neighbourhood;
    
    self.address.neighbourhood = neighbourhood.neighbourhood;
    self.address.neighbourhoodCode = neighbourhood.neighbourhoodCode;
}

@end
