//
//  PR_AddressAreaPickerViewController.m
//  PoerStore
//
//  Created by 伟 晏 on 16/4/16.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressAreaPickerViewController.h"

#import <libextobjc/EXTScope.h>
#import <KVOController/KVOController.h>

#import "PR_AddressConfigModel.h"

#import "PR_AddressAreaPickerViewModel.h"


@interface PR_AddressAreaPickerViewController () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) PR_AddressAreaPickerViewModel *viewModel;

@end

@implementation PR_AddressAreaPickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    tapGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    self.viewModel = [[PR_AddressAreaPickerViewModel alloc] init];
    self.viewModel.selectedProvinceCode = self.selectedProvinceCode;
    self.viewModel.selectedCityCode = self.selectedCityCode;
    self.viewModel.selectedDistrictCode = self.selectedDistrictCode;
    
    @weakify(self);
    
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    self.viewModel.error = ^(NSError *error) {
        @strongify(self);
        [self alertMessage:[error localizedDescription]];
    };
    
    self.viewModel.updateContent = ^() {
        @strongify(self);
        [self.pickerView reloadAllComponents];
    };
    
    self.viewModel.selectRow = ^(NSInteger row, NSInteger component) {
        @strongify(self);
        [self.pickerView reloadComponent:component];
        [self.pickerView selectRow:row inComponent:component animated:YES];
    };
    
    self.viewModel.refresh = ^() {
        @strongify(self);
        [self.viewModel reloadData];
    };
    
    [self.KVOController observe:self.viewModel keyPath:NSStringFromSelector(@selector(selectedProvince)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        [self.viewModel autoSelectCity];
    }];
    
    [self.KVOController observe:self.viewModel keyPath:NSStringFromSelector(@selector(selectedCity)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        [self.viewModel autoSelectDistrict];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.viewModel.active = NO;
}

#pragma mark - action

- (void)tapGestureAction:(UITapGestureRecognizer *)tapGesture
{
    if (self.dismiss) {
        
        self.dismiss();
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self.view];
    if (CGRectContainsPoint(self.pickerView.frame, point)) {
        return NO;
    }
    return YES;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return [self.viewModel numberOfComponents];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger numberOfRowsInComponent = [self.viewModel numberOfRowsInComponent:component];
    
    return numberOfRowsInComponent;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = [self.viewModel titleForRow:row forComponent:component];
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self.viewModel didSelectRow:row inComponent:component];
    
    if (self.didSelectAddress) {
        self.didSelectAddress(self.viewModel.selectedProvince, self.viewModel.selectedCity, self.viewModel.selectedDistrict);
    }
}

@end
