//
//  PR_AddressNeighbourhoodPickerViewController.h
//  PoerStore
//
//  Created by 伟 晏 on 16/9/1.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_ViewController.h"

@class PR_DistrictModel, PR_NeighbourhoodModel;

@interface PR_AddressNeighbourhoodPickerViewController : PR_ViewController

@property (strong, nonatomic) PR_DistrictModel *districtModel;

@property (copy, nonatomic) NSString *selectedNeighbourhoodCode;

@property (copy, nonatomic) dispatch_block_t dismiss;
@property (copy, nonatomic) void(^didSelectNeighbourhood)(PR_NeighbourhoodModel *neighbourhood);

@end
