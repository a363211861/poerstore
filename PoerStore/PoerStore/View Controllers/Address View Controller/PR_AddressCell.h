//
//  PR_AddressCell.h
//  PoerStore
//
//  Created by 伟 晏 on 16/2/15.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "UITableViewCell+PR_Category.h"

extern NSInteger const kPR_AddressCellHeight;

@interface PR_AddressCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *addresseeLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
