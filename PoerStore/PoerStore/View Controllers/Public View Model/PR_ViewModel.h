//
//  PR_ViewModel.h
//  PoerStore
//
//  Created by 伟 晏 on 15/8/25.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PR_DataStruct.h"

@interface PR_ViewModel : NSObject

@property (assign, nonatomic) BOOL active;

@property (copy, nonatomic) dispatch_block_t didBecomeActive;
@property (copy, nonatomic) dispatch_block_t didBecomeInactive;

/**
 *  HUD 回调
 */
@property (copy, nonatomic) dispatch_block_t showHUD;
@property (copy, nonatomic) dispatch_block_t hiddenHUD;

/**
 *  Progress 回调
 */
@property (copy, nonatomic) void(^showProgress)(double progress);
@property (strong, nonatomic) NSProgress *progress;
- (void)observerProgress:(NSProgress *)progress;
- (void)removeObserverProgress:(NSProgress *)progress;

/**
 *  刷新的方法 每隔一段时间调用一次
 */
@property (copy, nonatomic) dispatch_block_t refresh;

/**
 *  统一的错误处理
 */
@property (copy, nonatomic) PR_ErrorCallback error;

@end
