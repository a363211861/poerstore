//
//  PR_ViewModel.m
//  PoerStore
//
//  Created by 伟 晏 on 15/8/25.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_ViewModel.h"

#import <objc/runtime.h>
#import <KVOController/KVOController.h>
#import <libextobjc/EXTScope.h>

#import "PR_Macro.h"

static NSTimeInterval const kPR_ViewModel_AutoRefreshTimeInterval = 300;

@interface PR_ViewModel ()

@property (strong, atomic) NSDate *refreshDate;

@end

@implementation PR_ViewModel

- (void)setActive:(BOOL)active
{
    _active = active;
    
    if (active) {
        
        if (!self.refreshDate || fabs(self.refreshDate.timeIntervalSinceNow) - kPR_ViewModel_AutoRefreshTimeInterval > 0.01) {
            self.refreshDate = [NSDate date];
            if (self.refresh) {
                self.refresh();
            }
        }
        
        if (self.didBecomeActive) {
            self.didBecomeActive();
        }
        
        return;
    }
    
    if (!active) {
        
        if (self.didBecomeInactive) {
            self.didBecomeInactive();
        }
        
        return;
    }
}

- (void)observerProgress:(NSProgress *)progress
{
    @weakify(self);
    [self.KVOController observe:progress keyPath:NSStringFromSelector(@selector(fractionCompleted)) options:NSKeyValueObservingOptionInitial block:^(id  _Nullable observer, id  _Nonnull object, NSDictionary<NSString *,id> * _Nonnull change) {
        @strongify(self);
        
        if (self.showProgress) {
            self.showProgress(progress.fractionCompleted);
        }
    }];
}

- (void)removeObserverProgress:(NSProgress *)progress
{
    [self.KVOController unobserve:progress];
}



#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
