//
//  PR_TableViewController.m
//  PoerStore
//
//  Created by YanW on 15/4/23.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_TableViewController.h"

@interface PR_TableViewController ()

@end

@implementation PR_TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = YES;
    self.tableView.tableFooterView = [[UIView alloc] init];
}

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
