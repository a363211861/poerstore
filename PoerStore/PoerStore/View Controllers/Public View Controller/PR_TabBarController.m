//
//  PR_TabBarController.m
//  PoerStore
//
//  Created by YanW on 15/4/23.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_TabBarController.h"

#import "UIStoryboard+PR_Category.h"
#import "CQMFloatingController.h"
#import "PR_NavigationController.h"
#import "PR_MerchandisesViewController.h"
#import "PR_ShoppingCartViewController.h"
#import "PR_UserCenterViewController.h"
#import "PR_LoginViewController.h"

#import "UIColor+PR_Category.h"

#import <libextobjc/EXTScope.h>
#import <KVOController/KVOController.h>

#import "PR_UserModel.h"

#import "PR_DataStruct.h"

@interface PR_TabBarController () <UITabBarControllerDelegate>

@property (assign, nonatomic) NSInteger willSelectIndex;
@property (weak, nonatomic) CQMFloatingController *floatingController;

@end

@implementation PR_TabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
    
    NSMutableArray *viewControllers = [NSMutableArray array];
    
    {
        PR_MerchandisesViewController *viewController = [[UIStoryboard merchandiseStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_MerchandisesViewController class])];
        viewController.style = PR_MerchandisesStyleFeatured;
        
        PR_NavigationController *navigationController = [[PR_NavigationController alloc] initWithRootViewController:viewController];
        navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:[PR_MerchandisesViewController titleForStyle:viewController.style] image:[PR_MerchandisesViewController tabBarItemImageForStyle:viewController.style] tag:0];
        [viewControllers addObject:navigationController];
    }
    
    {
        PR_MerchandisesViewController *viewController = [[UIStoryboard merchandiseStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_MerchandisesViewController class])];
        viewController.style = PR_MerchandisesStyleAll;
        
        PR_NavigationController *navigationController = [[PR_NavigationController alloc] initWithRootViewController:viewController];
        navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:[PR_MerchandisesViewController titleForStyle:viewController.style] image:[PR_MerchandisesViewController tabBarItemImageForStyle:viewController.style] tag:1];
        [viewControllers addObject:navigationController];
    }
    
    {
        PR_MerchandisesViewController *viewController = [[UIStoryboard merchandiseStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_MerchandisesViewController class])];
        viewController.style = PR_MerchandisesStyleHistory;
        
        PR_NavigationController *navigationController = [[PR_NavigationController alloc] initWithRootViewController:viewController];
        navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:[PR_MerchandisesViewController titleForStyle:viewController.style] image:[PR_MerchandisesViewController tabBarItemImageForStyle:viewController.style] tag:2];
        [viewControllers addObject:navigationController];
    }
    
    {
        PR_ShoppingCartViewController *viewController = [[UIStoryboard shoppingCartStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_ShoppingCartViewController class])];
        
        PR_NavigationController *navigationController = [[PR_NavigationController alloc] initWithRootViewController:viewController];
        navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"购物车" image:[UIImage imageNamed:@"shoppingCart"] tag:3];
        [viewControllers addObject:navigationController];
    }
    
    {
        PR_UserCenterViewController *viewController = [[UIStoryboard userStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_UserCenterViewController class])];
        
        PR_NavigationController *navigationController = [[PR_NavigationController alloc] initWithRootViewController:viewController];
        navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[UIImage imageNamed:@"user"] tag:4];
        [viewControllers addObject:navigationController];
    }
    
    self.viewControllers = viewControllers;
}

#pragma mark - login control

- (void)showLoginView
{
    CQMFloatingController *floatingController = [CQMFloatingController sharedFloatingController];
    floatingController.portraitFrameSize = CGSizeMake(CGRectGetWidth(self.view.bounds) - 80, 300);
    floatingController.frameColor = [UIColor whiteColor];
    
    PR_LoginViewController *viewController = [[UIStoryboard authorizeStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_LoginViewController class])];
    
    [floatingController showInView:self.view withContentViewController:viewController animated:YES];
    
    self.floatingController = floatingController;
}

- (void)addObserverForUser
{
    [self.KVOController unobserve:[PR_User user]];
    
    @weakify(self);
    [self.KVOController observe:[PR_User user] keyPath:NSStringFromSelector(@selector(userModel)) options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        
        self.selectedIndex = self.willSelectIndex;
        [self.floatingController dismissAnimated:YES];
        [self removeObserverForUser];
    }];
}

- (void)removeObserverForUser
{
    [self.KVOController unobserve:[PR_User user] keyPath:NSStringFromSelector(@selector(userModel))];
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSInteger willSelectIndex = [tabBarController.viewControllers indexOfObject:viewController];
    self.willSelectIndex = willSelectIndex;
    if (willSelectIndex == 4) {
        
        if (![PR_User user].userModel) {
            [self showLoginView];
            [self addObserverForUser];
            return NO;
        }
    }
    return YES;
}

@end
