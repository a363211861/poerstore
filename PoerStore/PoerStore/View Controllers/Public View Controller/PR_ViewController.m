//
//  PR_ViewController.m
//  PoerStore
//
//  Created by YanW on 15/4/23.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_ViewController.h"

@interface PR_ViewController ()

@end

@implementation PR_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#if DEBUG

- (void)dealloc
{
    NSLog(@"%@ dealloc", NSStringFromClass([self class]));
}

#endif

@end
