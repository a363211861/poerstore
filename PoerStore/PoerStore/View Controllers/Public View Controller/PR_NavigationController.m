//
//  PR_NavigationController.m
//  PoerStore
//
//  Created by YanW on 15/4/23.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_NavigationController.h"

#import <objc/runtime.h>

@interface PR_NavigationController () <UIGestureRecognizerDelegate>

@end

@implementation PR_NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self.interactivePopGestureRecognizer isKindOfClass:[UIGestureRecognizer class]]) {
        object_setClass(self.interactivePopGestureRecognizer, [UIPanGestureRecognizer class]);
    }
    self.interactivePopGestureRecognizer.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
