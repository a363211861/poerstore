//
//  PR_LoginViewModel.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_ViewModel.h"

@interface PR_LoginViewModel : PR_ViewModel

- (void)loginForUsername:(NSString *)username password:(NSString *)password;

@end
