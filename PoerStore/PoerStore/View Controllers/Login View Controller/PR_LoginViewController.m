//
//  PR_LoginViewController.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_LoginViewController.h"

#import <libextobjc/EXTScope.h>

#import "PR_LoginViewModel.h"

#import "UIFont+PR_Category.h"
#import "UIButton+PR_Category.h"
#import "NSString+PR_Category.h"
#import "UITextField+PR_Category.h"
#import "UIView+PR_Category.h"

#import "UIStoryboard+PR_Category.h"
#import "PR_RegisterViewController.h"
#import "PR_ResetPasswordViewController.h"

@interface PR_LoginViewController ()

@property (strong, nonatomic) PR_LoginViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgetPasswordButton;

@end

@implementation PR_LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"登录";
    
    self.viewModel = [[PR_LoginViewModel alloc] init];
    
    @weakify(self);
    self.viewModel.showHUD = ^() {
        @strongify(self);
        [self showHUD];
    };
    
    self.viewModel.hiddenHUD = ^() {
        @strongify(self);
        [self hideHUD];
    };
    
    {
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"注册" style:UIBarButtonItemStyleDone target:self action:@selector(registerAction:)];
        self.navigationItem.rightBarButtonItem = barButtonItem;
    }
    
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
    
    NSInteger textFieldLeftWidth = 50;
    {
        UITextField *textField = self.usernameTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, CGRectGetHeight(textField.frame))];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"用户名";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UITextField *textField = self.passwordTextField;
        [textField backgroundForNormal];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textFieldLeftWidth, textField.frame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont customFontFour];
        label.text = @"密   码";
        textField.leftView = label;
        textField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    {
        UIButton *button = self.loginButton;
        button.exclusiveTouch = YES;
        [button backgroundWithSystemColor];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.viewModel.active = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.viewModel.active = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.view endEditing:YES];
}

#pragma mark - action

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (void)registerAction:(UIBarButtonItem *)barButtonItem
{
    PR_RegisterViewController *viewController = [[UIStoryboard authorizeStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_RegisterViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)loginAction:(UIButton *)button
{
    [self.view endEditing:YES];
    
    NSString *username = [self.usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (username.length == 0 || ![username isValidateUsername]) {
        [self.usernameTextField becomeFirstResponder];
        [self.usernameTextField backgroundForError];
        [self.usernameTextField shake];
        return;
    } else {
        [self.usernameTextField backgroundForNormal];
    }
    
    NSString *password = self.passwordTextField.text;
    if (password.length == 0 || ![password isValidatePassword]) {
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField backgroundForError];
        [self.passwordTextField shake];
        return;
    } else {
        [self.passwordTextField backgroundForNormal];
    }
    
    [self.viewModel loginForUsername:username password:password];
}

- (IBAction)forgetPasswordAction:(UIButton *)sender {
    
    PR_ResetPasswordViewController *viewController = [[UIStoryboard authorizeStoryboard] instantiateViewControllerWithIdentifier:NSStringFromClass([PR_ResetPasswordViewController class])];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField backgroundForEdit];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField backgroundForNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self loginAction:self.loginButton];
    }
    return YES;
}

@end
