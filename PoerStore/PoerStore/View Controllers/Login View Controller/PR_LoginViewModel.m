//
//  PR_LoginViewModel.m
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_LoginViewModel.h"

#import <libextobjc/EXTScope.h>

#import "PR_UserRequest.h"

#import "PR_UserModel.h"

@interface PR_LoginViewModel ()

@property (strong, nonatomic) PR_HTTPRequest *dataTask;

@end

@implementation PR_LoginViewModel

- (void)loginForUsername:(NSString *)username password:(NSString *)password
{
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (self.showHUD) {
        self.showHUD();
    }
    
    @weakify(self);
    
    self.dataTask = [PR_UserRequest loginWithUsername:username password:password success:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        [PR_User user].userModel = responseObject;
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if (self.hiddenHUD) {
            self.hiddenHUD();
        }
        
        if (self.error) {
            self.error(error);
        }
    }];
}

@end
