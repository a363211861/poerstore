//
//  PR_UserModel.m
//  PoerStore
//
//  Created by 伟 晏 on 15/10/3.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_UserModel.h"


@implementation PR_UserModel

@end


@implementation PR_User

+ (instancetype)user
{
    static PR_User *_shareUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareUser = [[PR_User alloc] init];
    });
    return _shareUser;
}

@end
