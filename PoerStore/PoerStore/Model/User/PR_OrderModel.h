//
//  PR_OrderModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/10.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@class PR_MerchandiseModel, PR_UserAddressModel;

@interface PR_OrderModel : PR_Model

@property (strong, nonatomic) PR_MerchandiseModel *merchandise;
@property (strong, nonatomic) NSNumber *count;
@property (strong, nonatomic) NSNumber *price;

@property (strong, nonatomic) PR_UserAddressModel *address;

@end
