//
//  PR_UserModel.h
//  PoerStore
//
//  Created by 伟 晏 on 15/10/3.
//  Copyright © 2015年 Creu. All rights reserved.
//

#import "PR_Model.h"


@interface PR_UserModel : PR_Model

@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *token;

@end


@interface PR_User : NSObject

@property (strong, nonatomic) PR_UserModel *userModel;

+ (instancetype)user;

@end