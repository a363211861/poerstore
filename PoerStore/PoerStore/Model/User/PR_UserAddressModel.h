//
//  PR_UserAddressModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/10.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@interface PR_UserAddressModel : PR_Model

@property (copy, nonatomic) NSString *addressID;

@property (copy, nonatomic) NSString *addressee; //收件人
@property (copy, nonatomic) NSString *phoneNumber;

@property (copy, nonatomic) NSString *province; //省
@property (copy, nonatomic) NSString *provinceCode;
@property (copy, nonatomic) NSString *city; //市
@property (copy, nonatomic) NSString *cityCode;
@property (copy, nonatomic) NSString *district; //区
@property (copy, nonatomic) NSString *districtCode;
@property (copy, nonatomic) NSString *neighbourhood; //街道
@property (copy, nonatomic) NSString *neighbourhoodCode;
@property (copy, nonatomic) NSString *detail; //详细地址
@property (copy, nonatomic) NSString *postcode; //邮政编码

@end
