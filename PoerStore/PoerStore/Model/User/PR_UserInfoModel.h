//
//  PR_UserInfoModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/10.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@interface PR_UserInfoModel : PR_Model

@property (strong, nonatomic) NSNumber *waitPayOrderCount;
@property (strong, nonatomic) NSNumber *transportOrderCount;
@property (strong, nonatomic) NSNumber *finsihOrderCount;

@property (strong, nonatomic) NSNumber *addressCount;

@end
