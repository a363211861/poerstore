//
//  PR_OrdersModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/1/10.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@protocol PR_OrderModel;

@interface PR_OrdersModel : PR_Model

@property (strong, nonatomic) NSMutableArray<PR_OrderModel> *waitPayOrders;
@property (strong, nonatomic) NSMutableArray<PR_OrderModel> *transportOrders;
@property (strong, nonatomic) NSMutableArray<PR_OrderModel> *finishOrders;

@end
