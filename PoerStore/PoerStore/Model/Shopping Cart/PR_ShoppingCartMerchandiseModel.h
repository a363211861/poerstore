//
//  PR_MerchandiseShoppingCartModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@class PR_MerchandiseModel;

@interface PR_ShoppingCartMerchandiseModel : PR_Model

@property (strong, nonatomic) PR_MerchandiseModel *merchandise;
@property (assign, nonatomic) NSUInteger currentBuyCount; //购买数量 default merchandise buyMinCount

@property (assign, nonatomic) BOOL canBuy; //是否可以购买
@property (copy, nonatomic) NSString *message; //不可以购买的原因（三个字）

@end
