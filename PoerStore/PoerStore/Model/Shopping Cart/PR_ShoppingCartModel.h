//
//  PR_ShoppingCartModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@protocol PR_ShoppingCartMerchandiseModel;
@class PR_ShoppingCartAmountModel;

@interface PR_ShoppingCartModel : PR_Model

@property (strong, nonatomic) NSArray<PR_ShoppingCartMerchandiseModel> *merchandises;
@property (strong, nonatomic) PR_ShoppingCartAmountModel *amount;

@end
