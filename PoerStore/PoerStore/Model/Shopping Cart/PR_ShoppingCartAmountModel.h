//
//  PR_ShoppingCartAmountModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/10/14.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@interface PR_ShoppingCartAmountModel : PR_Model

@property (assign, nonatomic) float payableMoney; //应付
@property (assign, nonatomic) float saveMoney; //节省
@property (assign, nonatomic) float totalMoney; //原价

@end
