//
//  PR_ImageModel.m
//  PoerStore
//
//  Created by 伟 晏 on 15/9/13.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_ImageModel.h"

@implementation PR_ImageModel

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    if (self) {
        NSString *urlString = dict[@"url"];
        self.url = [NSURL URLWithString:urlString];
    }
    return self;
}

@end
