//
//  PR_ImageModel.h
//  PoerStore
//
//  Created by 伟 晏 on 15/9/13.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_Model.h"

@class UIImage;

@interface PR_ImageModel : PR_Model

//Ignore
@property (strong, nonatomic) NSURL<Ignore> *url;
@property (strong, nonatomic) UIImage<Ignore> *image;

@end
