//
//  PR_AddressConfigModel.m
//  PoerStore
//
//  Created by 伟 晏 on 16/3/19.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_AddressConfigModel.h"

@implementation PR_AddressVersionModel

- (BOOL)isEqualToVersion:(PR_AddressVersionModel *)version
{
    NSComparisonResult result = [self.version compare:version.version options:NSCaseInsensitiveSearch];
    
    return result;
}

@end


@implementation PR_ProvinceModel

+(JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  NSStringFromSelector(@selector(province)): @"name",
                                                                  NSStringFromSelector(@selector(citys)): @"city"
                                                                  }];
}

@end


@implementation PR_CityModel

+(JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  NSStringFromSelector(@selector(city)): @"name"
                                                                  }];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    if (self) {
        
        NSMutableArray *districtArray = [NSMutableArray array];
        
        NSArray *area = dict[@"area"];
        
        for (NSString *areaStr in area) {
            
            PR_DistrictModel *district = [[PR_DistrictModel alloc] init];
            district.district = areaStr;
            
            [districtArray addObject:district];
        }
        
        self.districts = (NSArray<PR_DistrictModel> *)[NSArray arrayWithArray:districtArray];
    }
    return self;
}

@end


@implementation PR_DistrictModel

+(JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  NSStringFromSelector(@selector(district)): @"name"
                                                                  }];
}

@end



@implementation PR_NeighbourhoodModel

@end



#import <libextobjc/EXTScope.h>

#import "PR_AddressRequest.h"
#import "PR_AddressCacheManager.h"

@interface PR_AddressListModel ()

@property (strong) NSMutableArray *callbackBlocs;

@property (assign) BOOL isLoading;
@property (nonatomic, strong) PR_HTTPRequest *versionRequest;
@property (nonatomic, strong) PR_HTTPRequest *addressRequest;

@end


@implementation PR_AddressListModel

+ (instancetype)addressList
{
    static PR_AddressListModel *_addressList = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _addressList = [[PR_AddressListModel alloc] init];
    });
    return _addressList;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.callbackBlocs = [NSMutableArray array];
    }
    return self;
}


- (void)requestAddressListWithComplete:(PR_RequestAddressListComplete)complete
{
    if (complete) {
        
        if ([PR_AddressCacheManager cacheManager].cache.provinces.count > 0) {
            
            complete([PR_AddressCacheManager cacheManager].cache.provinces, nil); return;
        }
        
        [self.callbackBlocs addObject:complete];
    }
    
    if (self.isLoading) {
        
        return;
    }
    
    @weakify(self);
    
    PR_HTTPRequest *request = [PR_AddressRequest requestAddressTreeVersionWithSuccess:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        if ([[PR_AddressCacheManager cacheManager].cache.version isEqualToVersion:responseObject]) {
            
            [self completeWithSucces];
            
        } else {
            
            [self requestAddressListWithVersion:responseObject];
        }
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        [self completeWithError:error];
    }];
    self.versionRequest = request;
}


- (void)requestAddressListWithVersion:(PR_AddressVersionModel *)version
{
    @weakify(self);
    
    PR_HTTPRequest *request = [PR_AddressRequest requestAddressTreeWithSuccess:^(PR_HTTPRequest * _Nonnull request, id  _Nullable responseObject) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        
        [PR_AddressCacheManager cacheManager].cache.version = version;
        [PR_AddressCacheManager cacheManager].cache.provinces = responseObject;
        
        [self completeWithSucces];
        
    } progress:^(NSProgress * _Nonnull progress) {
        
        @strongify(self);
        self.progress = progress;
        [self observerProgress:progress];
        
    } failure:^(PR_HTTPRequest * _Nonnull request, NSError * _Nonnull error) {
        
        @strongify(self);
        [self removeObserverProgress:self.progress];
        [self completeWithError:error];
    }];
    self.addressRequest = request;
}


- (void)completeWithSucces
{
    for (PR_RequestAddressListComplete complete in self.callbackBlocs) {
        
        complete([PR_AddressCacheManager cacheManager].cache.provinces, nil);
    }
}


- (void)completeWithError:(NSError *)error
{
    for (PR_RequestAddressListComplete complete in self.callbackBlocs) {
        
        complete(nil, error);
    }
}

@end
