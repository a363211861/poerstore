//
//  PR_AddressConfigModel.h
//  PoerStore
//
//  Created by 伟 晏 on 16/3/19.
//  Copyright © 2016年 Creu. All rights reserved.
//

#import "PR_Model.h"

@interface PR_AddressVersionModel : PR_Model

@property (copy, nonatomic) NSString *version;

- (BOOL)isEqualToVersion:(PR_AddressVersionModel *)version;

@end


@protocol PR_CityModel;

@interface PR_ProvinceModel : PR_Model

@property (copy, nonatomic) NSString *province; //省
@property (copy, nonatomic) NSString *provinceCode;

@property (copy, nonatomic) NSArray<PR_CityModel, Optional> *citys;

@end



@protocol PR_DistrictModel;

@interface PR_CityModel : PR_Model

@property (copy, nonatomic) NSString *city; //市
@property (copy, nonatomic) NSString *cityCode;

@property (copy, nonatomic) NSArray<PR_DistrictModel, Optional> *districts;

@end


@protocol PR_NeighbourhoodModel;

@interface PR_DistrictModel : PR_Model

@property (copy, nonatomic) NSString *district; //区
@property (copy, nonatomic) NSString *districtCode;

@property (copy, nonatomic) NSArray<PR_NeighbourhoodModel, Optional> *neighbourhoods;

@end


@interface PR_NeighbourhoodModel : PR_Model

@property (copy, nonatomic) NSString *neighbourhood; //街道
@property (copy, nonatomic) NSString *neighbourhoodCode;

@end


#import "PR_ViewModel.h"

typedef void(^PR_RequestAddressListComplete)(NSArray<PR_ProvinceModel *> *provinceList, NSError *error);

@interface PR_AddressListModel : PR_ViewModel

+ (instancetype)addressList;

- (void)requestAddressListWithComplete:(PR_RequestAddressListComplete)complete;

@end
