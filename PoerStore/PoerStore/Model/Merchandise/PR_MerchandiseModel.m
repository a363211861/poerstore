//
//  PR_MerchandiseModel.m
//  PoerStore
//
//  Created by Creu on 15/5/22.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseModel.h"

#import "PR_ImageModel.h"

@implementation PR_MerchandiseModel

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    if (self) {
        
        NSString *iconURLString = dict[@"icon"];
        if (iconURLString) {
            self.icon = [[PR_ImageModel alloc] initWithDictionary:@{@"url": iconURLString} error:err];
        } else {
            self.icon = [[PR_ImageModel alloc] init];
        }
    }
    return self;
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

+ (BOOL)propertyIsIgnored:(NSString*)propertyName
{
    BOOL result = [super propertyIsIgnored:propertyName];
    if ([propertyName isEqualToString:NSStringFromSelector(@selector(style))] ||
        [propertyName isEqualToString:NSStringFromSelector(@selector(icon))]) {
        
        result = YES;
    }
    return result;
}

@end
