//
//  PR_MerchandiseModel.h
//  PoerStore
//
//  Created by Creu on 15/5/22.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_Model.h"

#import "PR_DataStruct.h"

@class PR_ImageModel;

@interface PR_MerchandiseModel : PR_Model

@property (strong, nonatomic) NSString *merchandiseID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *desc;
@property (assign, nonatomic) float normalPrice;
@property (assign, nonatomic) float discountPrice;
@property (strong, nonatomic) PR_ImageModel *icon;

@property (assign, nonatomic) NSUInteger buyMinCount;
@property (assign, nonatomic) NSUInteger buyMaxCount;

@property (assign, nonatomic) PR_PriceButtonStyle style;

@end
