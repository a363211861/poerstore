//
//  PR_MerchandiseDetailModel.m
//  PoerStore
//
//  Created by 伟 晏 on 15/8/21.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_MerchandiseDetailModel.h"

#import "PR_ImageModel.h"


@implementation PR_MerchandiseDetailModel

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    if (self) {
        
        self.merchandiseID = dict[@"id"];
        NSString *iconURLString = dict[@"icon"];
        if (iconURLString) {
            self.icon = [[PR_ImageModel alloc] initWithDictionary:@{@"url": iconURLString} error:err];
        } else {
            self.icon = [[PR_ImageModel alloc] init];
        }
        
        self.detailDesc = @"这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试这是一个测试";
        
        NSMutableArray *tempArray = [NSMutableArray array];
        NSArray *images = dict[@"images"];
        for (NSString *url in images) {
            
            PR_ImageModel *model = [[PR_ImageModel alloc] initWithDictionary:@{@"url": url} error:err];
            [tempArray addObject:model];
        }
        self.images = (NSArray<PR_ImageModel> *)[NSArray arrayWithArray:tempArray];
    }
    return self;
}

@end
