//
//  PR_MerchandiseDetailModel.h
//  PoerStore
//
//  Created by 伟 晏 on 15/8/21.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_Model.h"

@protocol PR_ImageModel;

@class PR_ImageModel;

@interface PR_MerchandiseDetailModel : PR_Model

@property (strong, nonatomic) NSString *merchandiseID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSString *detailDesc;
@property (strong, nonatomic) NSNumber *price;
@property (strong, nonatomic) PR_ImageModel<Ignore> *icon;
@property (strong, nonatomic) NSArray<Ignore, PR_ImageModel> *images;

@end

