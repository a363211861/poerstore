//
//  PR_WebImageManager.m
//  PoerStore
//
//  Created by Creu on 15/5/26.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import "PR_WebImageManager.h"

#import <SDWebImage/SDWebImageCompat.h>
#import <SDWebImage/SDWebImageManager.h>

#import <libextobjc/EXTScope.h>

#import "UIImage+PR_Category.h"

@interface PR_WebImageManager ()

@property (strong, nonatomic) NSMutableDictionary *imageDownloadsInProgress;

@end

@implementation PR_WebImageManager

+ (void)load
{
    [PR_WebImageManager shareWebImageManager];
}

+ (instancetype)shareWebImageManager
{
    static PR_WebImageManager *_shareWebImageManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareWebImageManager = [[PR_WebImageManager alloc] init];
    });
    return _shareWebImageManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [SDWebImageManager sharedManager].imageCache.maxCacheSize = 50 * 1024 * 1024;
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
        
        self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [[SDWebImageManager sharedManager].imageCache clearMemory];
}

- (void)downloadImageWithURL:(NSURL *)URL completed:(void(^)(UIImage *image))completed
{
    if (!URL) {
        if (completed) {
            completed(nil);
        }
        return;
    }
    
    NSString *cacheKey = [[SDWebImageManager sharedManager] cacheKeyForURL:URL];
    
    id<SDWebImageOperation> operation = self.imageDownloadsInProgress[cacheKey];
    if (operation) {
        return;
    }
    
    @weakify(self);
    
    operation = [[SDWebImageManager sharedManager] downloadImageWithURL:URL options:SDWebImageLowPriority progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
        if (image && !error) {
            
            @strongify(self);
            
            if (completed) {
                
                dispatch_main_sync_safe(^(){
                    
                    completed(image);
                });
            }
            
            [self.imageDownloadsInProgress removeObjectForKey:cacheKey];
            
        } else {
            
            @strongify(self);
            [self.imageDownloadsInProgress removeObjectForKey:cacheKey];
        }
    }];
    
    self.imageDownloadsInProgress[cacheKey] = operation;
}

+ (void)downloadImageWithURL:(NSURL *)URL completed:(void(^)(UIImage *image))completed
{
    [[PR_WebImageManager shareWebImageManager] downloadImageWithURL:URL completed:completed];
}

@end



@implementation PR_WebImageManager (resizeAndMask)

+ (void)queryImageForKey:(NSString *)key completed:(void(^)(UIImage *image))completed
{
    if (completed) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            UIImage *image = [[SDWebImageManager sharedManager].imageCache imageFromDiskCacheForKey:key];
            
            dispatch_main_sync_safe(^(){
                
                completed(image);
            });
        });
    }
}

- (void)downloadImageWithURL:(NSURL *)URL forSize:(CGSize)size completed:(void(^)(UIImage *image))completed
{
    if (!URL) {
        if (completed) {
            completed(nil);
        }
        return;
    }
    
    assert(!CGSizeEqualToSize(size, CGSizeZero));
    
    size.width = ceilf(size.width);
    size.height = ceilf(size.height);
    
    NSString *resizeURLString = [[URL absoluteString] stringByAppendingFormat:@"_%@_%@", @(size.width), @(size.height)];
    NSURL *resizeURL = [NSURL URLWithString:resizeURLString];
    
    NSString *resizeKey = [[SDWebImageManager sharedManager] cacheKeyForURL:resizeURL];
    
    [[SDWebImageManager sharedManager] cachedImageExistsForURL:resizeURL completion:^(BOOL isInCache) {
        
        if (isInCache) {
            
            [[self class] queryImageForKey:resizeKey completed:completed];
            
        } else {
            
            [self downloadImageWithURL:URL completed:^(UIImage *image) {
                
                if (image) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        UIImage *targetResizeImage = [image scaleToSize:size];
                        
                        [[SDWebImageManager sharedManager].imageCache storeImage:targetResizeImage forKey:resizeKey toDisk:YES];
                        
                        if (completed) {
                            
                            dispatch_main_sync_safe(^(){
                                
                                completed(targetResizeImage);
                            });
                        }
                    });
                    
                } else {
                    
                    if (completed) {
                        
                        dispatch_main_sync_safe(^(){
                            
                            completed(nil);
                        });
                    }
                }
            }];
        }
    }];
}


- (void)downloadImageWithURL:(NSURL *)URL forSize:(CGSize)size maskImage:(UIImage *)maskImage completed:(void(^)(UIImage *image))completed
{
    if (!URL) {
        if (completed) {
            completed(nil);
        }
        return;
    }
    
    assert(maskImage);
    
    NSString *resizeMaskURLString = [[URL absoluteString] stringByAppendingFormat:@"_%@_%@_mask", @(size.width), @(size.height)];
    NSURL *resizeMaskURL = [NSURL URLWithString:resizeMaskURLString];
    
    NSString *resizeMaskKey = [[SDWebImageManager sharedManager] cacheKeyForURL:resizeMaskURL];
    
    [[SDWebImageManager sharedManager] cachedImageExistsForURL:resizeMaskURL completion:^(BOOL isInCache) {
        
        if (isInCache) {
            
            [[self class] queryImageForKey:resizeMaskKey completed:completed];
            
        } else {
            
            [self downloadImageWithURL:URL forSize:size completed:^(UIImage *image) {
                
                if (image) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        UIImage *targetMaskImage = [image imageWithMask:maskImage];
                        
                        [[SDWebImageManager sharedManager].imageCache storeImage:targetMaskImage forKey:resizeMaskKey toDisk:YES];
                        
                        if (completed) {
                            
                            dispatch_main_sync_safe(^(){
                                
                                completed(targetMaskImage);
                            });
                        }
                    });
                    
                } else {
                    
                    if (completed) {
                        
                        dispatch_main_sync_safe(^(){
                            
                            completed(nil);
                        });
                    }
                }
            }];
        }
    }];
}


+ (void)downloadImageWithURL:(NSURL *)URL forSize:(CGSize)size completed:(void (^)(UIImage *))completed
{
    [[PR_WebImageManager shareWebImageManager] downloadImageWithURL:URL forSize:size completed:completed];
}

+ (void)downloadImageWithURL:(NSURL *)URL forSize:(CGSize)size maskImage:(UIImage *)maskImage completed:(void (^)(UIImage *))completed
{
    [[PR_WebImageManager shareWebImageManager] downloadImageWithURL:URL forSize:size maskImage:maskImage completed:completed];
}

@end




static CGSize const kMerchandiseIconSmallSize = {64, 64};

@implementation PR_WebImageManager (Merchandise)

+ (void)downloadMerchandiseSmallIconWithURL:(NSURL *)URL completed:(void (^)(UIImage *))completed
{
    static UIImage *maskImage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        maskImage = [UIImage imageNamed:@"Mask_64_64"];
    });
    [[PR_WebImageManager shareWebImageManager] downloadImageWithURL:URL forSize:kMerchandiseIconSmallSize maskImage:maskImage completed:completed];
}

+ (void)downloadMerchandiseBigIconWithURL:(NSURL *)URL completed:(void (^)(UIImage *))completed
{
    static UIImage *maskImage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        maskImage = [UIImage imageNamed:@"Mask_100_100"];
    });
    [[PR_WebImageManager shareWebImageManager] downloadImageWithURL:URL forSize:kMerchandiseIconSmallSize maskImage:maskImage completed:completed];
}

@end
