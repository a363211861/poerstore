//
//  PR_WebImageManager.h
//  PoerStore
//
//  Created by Creu on 15/5/26.
//  Copyright (c) 2015年 Creu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>

@class UIImage;

@interface PR_WebImageManager : NSObject

+ (void)downloadImageWithURL:(NSURL *)URL completed:(void(^)(UIImage *image))completed;

@end


@interface PR_WebImageManager (resizeAndMask)

+ (void)downloadImageWithURL:(NSURL *)URL forSize:(CGSize)size completed:(void(^)(UIImage *image))completed;

+ (void)downloadImageWithURL:(NSURL *)URL forSize:(CGSize)size maskImage:(UIImage *)maskImage completed:(void(^)(UIImage *image))completed;

@end


@interface PR_WebImageManager (Merchandise)

+ (void)downloadMerchandiseSmallIconWithURL:(NSURL *)URL completed:(void(^)(UIImage *image))completed;
+ (void)downloadMerchandiseBigIconWithURL:(NSURL *)URL completed:(void(^)(UIImage *image))completed;

@end