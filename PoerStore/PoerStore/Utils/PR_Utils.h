//
//  PR_Utils.h
//  PoerStore
//
//  Created by YanW on 15/5/9.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PR_Utils : NSObject

#if DEBUG

+ (void)printJsonObject:(id)jsonObject;

#endif

@end
