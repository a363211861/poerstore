//
//  PR_Functions.h
//  PoerStore
//
//  Created by 伟 晏 on 16/9/7.
//  Copyright © 2016年 Creu. All rights reserved.
//

#ifndef PR_Functions_h
#define PR_Functions_h

static inline CGFloat pr_fabs(CGFloat number)
{
#if defined(__LP64__) && __LP64__
    return fabs(number);
#else
    return fabsf(number);
#endif
}

#endif /* PR_Functions_h */
