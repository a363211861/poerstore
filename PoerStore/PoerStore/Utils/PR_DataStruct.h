//
//  PR_DataStruct.h
//  PoerStore
//
//  Created by 伟 晏 on 15/10/25.
//  Copyright © 2015年 Creu. All rights reserved.
//

#ifndef PR_DataStruct_h
#define PR_DataStruct_h

typedef NS_ENUM(NSUInteger, PR_MerchandisesStyle) {
    PR_MerchandisesStyleFeatured = 0,
    PR_MerchandisesStyleAll,
    PR_MerchandisesStyleHistory
};

typedef NS_ENUM(NSUInteger, PR_PriceButtonStyle) {
    PR_PriceButtonStylePrice = 0,//显示price
    PR_PriceButtonStylePurchase, //显示购买
    PR_PriceButtonStyleActivity, //菊花
    PR_PriceButtonStyleShoppingCart //已经放入购物车
};

typedef NS_ENUM(NSUInteger, PR_PriceButtonOption) {
    PR_PriceButtonOptionPreparePurchase = 0, //准备购买
    PR_PriceButtonOptionPurchase, //购买
    PR_PriceButtonOptionCancel, //取消准备购买
    PR_PriceButtonOptionShoppingCart //去购物车
};

typedef NS_ENUM(NSUInteger, PR_UserOrderStatus) {
    PR_UserOrderWaitPay = 0, //待支付
    PR_UserOrderTransport, //待收货，配送中
    PR_UserOrderHistory //已完成，历史单
};

typedef void(^PR_ErrorCallback)(NSError *error);

#endif /* PR_DataStruct_h */
