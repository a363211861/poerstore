//
//  PR_AlertCenter.m
//  PoerStore
//
//  Created by YanW on 15/5/22.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import "PR_AlertCenter.h"

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

#import <MBProgressHUD/MBProgressHUD.h>

static NSString * const kUIAlertView_Key = @"UIAlertView_Key";

@interface UIAlertView (PR_Private)

@property (strong, nonatomic) id<NSCopying> key;

@end


@implementation UIAlertView (PR_Private)

@dynamic key;

- (void)setKey:(id<NSCopying>)key
{
    objc_setAssociatedObject(self, (__bridge const void *)(kUIAlertView_Key), key, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id<NSCopying>)key
{
    return objc_getAssociatedObject(self, (__bridge const void *)(kUIAlertView_Key));
}

@end






@interface PR_AlertCenter ()

@property (strong, atomic) NSMutableDictionary *alertViewCache;

@end

@implementation PR_AlertCenter

+ (instancetype)shareCenter
{
    static PR_AlertCenter *_shareCenter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareCenter = [[PR_AlertCenter alloc] init];
    });
    return _shareCenter;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.alertViewCache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)alertMessage:(NSString *)message
{
    [self alertMessage:message forKey:message];
}

- (void)alertMessage:(NSString *)message forKey:(id<NSCopying>)key
{
    if (message.length == 0) {
        return;
    }
    
    UIAlertView *alertView = self.alertViewCache[key];
    if (alertView) {
        return;
    }
    
    alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil];
    alertView.key = key;
    [alertView show];
    
    self.alertViewCache[key] = alertView;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.key) {
        
        [self.alertViewCache removeObjectForKey:alertView.key];
    }
}



- (void)showMessage:(NSString *)message
{
    [self showMessage:message minShowTime:2];
}

- (void)showMessage:(NSString *)message minShowTime:(NSTimeInterval)minShowTime
{
    [self showMessage:message forKey:message minShowTime:minShowTime];
}

- (void)showMessage:(NSString *)message forKey:(id<NSCopying>)key minShowTime:(NSTimeInterval)minShowTime
{
    if (message.length == 0) {
        return;
    }
    
    if (!key) {
        key = message;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        MBProgressHUD *hud = self.alertViewCache[key];
        if (hud) {
            return;
        }
        
        hud = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] delegate] window] animated:YES];
        hud.minShowTime = minShowTime;
        hud.mode = MBProgressHUDModeText;
        hud.label.text = message;
        [hud showAnimated:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(minShowTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [hud hideAnimated:YES];
            [self.alertViewCache removeObjectForKey:key];
        });
        
        self.alertViewCache[key] = hud;
    });
}

@end
