//
//  PR_AlertCenter.h
//  PoerStore
//
//  Created by YanW on 15/5/22.
//  Copyright (c) 2015年 YanW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PR_AlertCenter : NSObject

+ (instancetype)shareCenter;

/**
 *  系统的弹框提示alertView
 *
 *  @param message 提示内容
 */
- (void)alertMessage:(NSString *)message;


/**
 *  系统的弹框提示
 *
 *  @param message 提示内容
 *  @param key     提示的key，避免重复弹框提示
 */
- (void)alertMessage:(NSString *)message forKey:(id<NSCopying>)key;


/**
 *  HUD 弹出提示，2秒自动消失
 *
 *  @param message 提示内容
 */
- (void)showMessage:(NSString *)message;


/**
 *  HUD 弹出提示，最短显示minShowTime秒
 *
 *  @param message     提示内容
 *  @param minShowTime 最短显示时间
 */
- (void)showMessage:(NSString *)message minShowTime:(NSTimeInterval)minShowTime;


/**
 *  HUD 弹出提示，最短显示minShowTime秒
 *
 *  @param message     提示内容
 *  @param key         最短的Key，避免同一时间重复提示
 *  @param minShowTime 最短显示时间
 */
- (void)showMessage:(NSString *)message forKey:(id<NSCopying>)key minShowTime:(NSTimeInterval)minShowTime;

@end
